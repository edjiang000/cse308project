public class Touch implements Comparable<Touch> {
    int precinct1;
    int precinct2;

    Touch(int precinct1, int precinct2) {
        this.precinct1 = precinct1;
        this.precinct2 = precinct2;
    }

    @Override
    public int compareTo(Touch o) {
        if (precinct1 == o.precinct1 && precinct2 == o.precinct2) {
            return 0;
        } else if (precinct1 > o.precinct1) {
            return 1;
        } else if (precinct1 == o.precinct1){
            if (precinct2 > o.precinct2) {
                return 1;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return String.format("(%d, %d)", precinct1, precinct2);
    }
}

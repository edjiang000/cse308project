//
//
//import com.google.gson.JsonArray;
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import org.locationtech.jts.geom.*;
//import org.locationtech.jts.geom.impl.PackedCoordinateSequence;
//
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.*;
//
//public class JsonTool {
//
//    private static final String FILEPATH = "src\\maine2.geojson";
//
//    public static void main (String[] args) throws Exception {
//        GeometryFactory gf = new GeometryFactory();
//
//        JsonParser parser = new JsonParser();
//        String json = new String(Files.readAllBytes(Paths.get(FILEPATH)));
//        JsonElement jsonTree = parser.parse(json);
//
//        TreeMap<Integer, Geometry> polygonMap = new TreeMap<>();
//        TreeMap<Coordinate, ArrayList<Integer>> coordinateToPrecinctMap = new TreeMap<>();
//
//        if(jsonTree.isJsonObject()){
//            JsonObject jsonObject = jsonTree.getAsJsonObject();
//            JsonElement mainElement = jsonObject.get("features");
//            JsonArray mainArray = mainElement.getAsJsonArray();
//
//            int precinctId = 0;
//
//            for (JsonElement e : mainArray) {
//                precinctId++;
//                JsonObject geoObject = e.getAsJsonObject().get("geometry").getAsJsonObject();
//                String geoType = geoObject.get("type").getAsString();
//                JsonArray geoArray = geoObject.get("coordinates").getAsJsonArray();
//
//                if (geoType.equals("Polygon")) {
//                    // First list main boundary
//                    Polygon polygon = generatePolygon(precinctId, geoArray, coordinateToPrecinctMap, gf);
//                    polygonMap.put(precinctId, polygon);
//                } else {
//                    Polygon[] polygons = new Polygon[geoArray.size()];
//                    for (int p = 0; p < polygons.length; p++) {
//                        JsonArray pointArray = geoArray.get(p).getAsJsonArray();
//                        Polygon polygon = generatePolygon(precinctId, pointArray, coordinateToPrecinctMap, gf);
//                        polygons[p] = polygon;
//                    }
//                    polygonMap.put(precinctId, new MultiPolygon(polygons, gf));
//                }
//            }
//        }
//        State state = new State();
//        generateEdges(state, coordinateToPrecinctMap, polygonMap);
//        System.out.println("Totally: " + polygonMap.keySet().size());
//        int pc = 0;
//        int mpc = 0;
//        for (Geometry g : polygonMap.values()) {
//            if (g instanceof Polygon) pc++;
//            if (g instanceof MultiPolygon) mpc++;
//        }
//        System.out.println("Polygon: " + pc);
//        System.out.println("Multipolygon: " + mpc);
//
//        explainResult(coordinateToPrecinctMap, 20);
//    }
//
//    private static ArrayList<Edge> generateEdges (State state, TreeMap<Coordinate, ArrayList<Integer>> coordinateToPrecinctMap, TreeMap<Integer, Geometry> polygonMap) {
//        Iterator<Integer> precinctIterator = polygonMap.keySet().iterator();
//        while (precinctIterator.hasNext()) {
//            int precinctId = precinctIterator.next();
//            Geometry geometry = polygonMap.get(precinctId);
//
//            if (geometry instanceof Polygon) {
//                Polygon polygon = (Polygon)geometry;
//                Coordinate[] c = polygon.getExteriorRing().getCoordinates();
//
//                Edge prev = new Edge();
//                for (int i = 1; i < c.length; i++) {
//                    double dis = c[i].distance(c[i-1]);
//                    ArrayList<Integer> listS = coordinateToPrecinctMap.get(c[i]);
//                    ArrayList<Integer> listE = coordinateToPrecinctMap.get(c[i-1]);
//                    int precinctId2;
//                    if (listE.size() == 1) {
//                        precinctId2 = 0;
//                    } else if (listE.size() == 2) {
//                        if (listE.get(0) == precinctId) {
//                            precinctId2 = listE.get(1);
//                        } else {
//                            precinctId2 = listE.get(0);
//                        }
//                    } else {
//                        precinctId2 = findSuccessor(listS, listE, c[i], c[i-1], precinctId);
//                    }
//                    if (precinctId < precinctId2 || precinctId2 == 0) {
//                        if (prev.precinct1 != precinctId || prev.precinct2 != precinctId2) {
//                            if (findEdgeFromState(state, precinctId, precinctId2) == null) {
//                                prev.ends.add(c[i-1]);
//                                Edge current = new Edge(precinctId, precinctId2);
//                                current.starts.add(c[i-1]);
//                                current.length.add(dis);
//                                current.pointLength.add(1);
//                                current.totalLength += dis;
//                                state.edges.add(current);
//                                prev = current;
//                            }
//                        } else {
//                            if (listE.size() <= 2) {
//                                prev.length.set(prev.length.size() - 1, prev.length.get(prev.length.size() - 1) + dis);
//                                prev.pointLength.set(prev.pointLength.size() - 1, prev.pointLength.get(prev.pointLength.size() - 1) + 1);
//                                prev.totalLength += dis;
//                            } else {
//                                prev.starts.add(c[i-1]);
//                                prev.length.add(dis);
//                                prev.pointLength.add(1);
//                                prev.totalLength += dis;
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        return null;
//    }
//
//    private static int findSuccessor (ArrayList<Integer> listS, ArrayList<Integer> listE, Coordinate c1, Coordinate c2, int id) {
//        // TODO:
//        return 0;
//    }
//
//    private static Edge findEdgeFromState (State state, int precinctId, int precinctId2) {
//        for (Edge e : state.edges) {
//            if (e.precinct1 == precinctId) {
//                if (e.precinct2 == precinctId2) {
//                    return e;
//                }
//            } else if (e.precinct2 == precinctId) {
//                if (e.precinct1 == precinctId2) {
//                    return e;
//                }
//            }
//        }
//        return null;
//    }
//
//    private static void tryPut(int precinctId, Coordinate c, TreeMap<Coordinate, ArrayList<Integer>> map) {
//        ArrayList<Integer> l = map.get(c);
//        if (l == null) {
//            l = new ArrayList<>();
//            l.add(precinctId);
//            map.put(c, l);
//        } else {
//            if (!l.contains(precinctId))
//                l.add(precinctId);
//        }
//    }
//
//    private static Polygon generatePolygon (int precinctId, JsonArray geoArray, TreeMap<Coordinate, ArrayList<Integer>> map, GeometryFactory gf) {
//        LinearRing mainRing = null;
//        LinearRing[] internalRing = new LinearRing[geoArray.size()-1];
//        for (int i = 0; i < geoArray.size(); i++) {
//            ArrayList<Coordinate> mainCoordinateList = new ArrayList<>();
//            JsonArray allCoordinates = geoArray.get(i).getAsJsonArray();
//
//            // Each array in "allCoordinates is a coordinate with 2 elements"
//            for (int k = 0; k < allCoordinates.size(); k++) {
//                JsonArray pair = allCoordinates.get(k).getAsJsonArray();
//                Coordinate c = new Coordinate(pair.get(0).getAsDouble(), pair.get(1).getAsDouble());
//                if (k < allCoordinates.size()-1)
//                    tryPut(precinctId, c, map);
//                mainCoordinateList.add(c);
//            }
//            Coordinate[] tempArr = new Coordinate[mainCoordinateList.size()];
//            LinearRing ring = new LinearRing(new PackedCoordinateSequence.Double(mainCoordinateList.toArray(tempArr)), gf);
//
//            // First ring is main ring, all other rings are internal
//            if (i == 0) {
//                mainRing = ring;
//            } else {
//                internalRing[i-1] = ring;
//            }
//        }
//        return new Polygon(mainRing, internalRing, gf);
//    }
//
//    private static void explainResult(TreeMap<Coordinate, ArrayList<Integer>> map, int threshold) {
//        Iterator<ArrayList<Integer>> it = map.values().iterator();
//        TreeMap<Touch, Integer> touchCounts = new TreeMap<>();
//        while (it.hasNext()) {
//            ArrayList<Integer> l = it.next();
//            if (l.size() == 1) {
//                Touch touch = new Touch(0, l.get(0));
//                Integer touchNum;
//                if ((touchNum = touchCounts.get(touch)) == null) {
//                    touchCounts.put(touch, 1);
//                } else {
//                    touchCounts.put(touch, touchNum+1);
//                }
//            } else {
//                for (int i = 0; i < l.size(); i++) {
//                    for (int j = i+1; j < l.size(); j++) {
//                        int pid1 = l.get(i);
//                        int pid2 = l.get(j);
////                        if (pid1 >= pid2) continue;
//                        Touch touch = new Touch(pid1, pid2);
//                        Integer touchNum;
//                        if ((touchNum = touchCounts.get(touch)) == null) {
//                            touchCounts.put(touch, 1);
//                        } else {
//                            touchCounts.put(touch, touchNum+1);
//                        }
//                    }
//                }
//            }
//        }
//
//        Iterator<Touch> it2 = touchCounts.keySet().iterator();
//        ArrayList<Touch> touches = new ArrayList<>();
//        while (it2.hasNext()) {
//            Touch touch = it2.next();
//            int i = touchCounts.get(touch);
//            if (i > threshold) {
//                touches.add(touch);
//            }
//        }
//
////        for (Touch touch : touches) {
////            if (touch.precinct1 == 27 || touch.precinct2 == 27) {
////                System.out.println(touch);
////            }
////        }
//        System.out.println("Totally " + touches.size() + " touches touches two precincts.");
//    }
//}

import geometry.GraphNode;

import java.util.ArrayList;
import java.util.List;

public class Edge {
    double totalLength;
    int precinct1;
    int precinct2;
    List<GraphNode> starts;
    List<Integer> pointLength;
    List<Double> length;
    List<GraphNode> ends;

    public Edge (int precinct1, int precinct2) {
        this.precinct1 = precinct1;
        this.precinct2 = precinct2;
        starts = new ArrayList<>();
        pointLength = new ArrayList<>();
        length = new ArrayList<>();
        ends = new ArrayList<>();
        totalLength = 0;
    }

    public Edge () {
        this(0, 0);
    }

    @Override
    public boolean equals (Object o) {
        if (o instanceof Edge) {
            Edge e = (Edge) o;
            if (e.precinct1 == this.precinct1 && e.precinct2 == this.precinct2) return true;
        }
        return false;
    }
}

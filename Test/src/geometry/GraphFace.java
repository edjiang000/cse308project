package geometry;

import java.util.ArrayList;

public class GraphFace implements Comparable<GraphFace> {
    private Graph parent;

    private int precinctId;
    private int faceId;
    private GraphNode startNode;
    private GraphEdge startEdge;
    private int size;

    public static GraphFace initGraphFace(int precinctId, int faceId, ArrayList<GraphNode> nodes, Graph parent) {
        GraphFace face = new GraphFace();
        face.parent = parent;
        face.precinctId = precinctId;
        face.faceId = faceId;
        face.startNode = nodes.get(0);
        face.size = nodes.size()-1;
        for (int i = 1; i < face.size; i++) {
            GraphNode from = nodes.get(i-1);
            GraphNode to = nodes.get(i);
            GraphNode next = nodes.get(i+1);
            from.outGoes(GraphEdge.initGraphEdge(from, to, face, next, parent));
        }
        nodes.get(face.size-1).outGoes(GraphEdge.initGraphEdge(nodes.get(face.size-1), nodes.get(0), face, nodes.get(1), parent));
        face.startEdge = parent.getEdge(nodes.get(0), nodes.get(1));
        return parent.insertFace(face);
    }

    public int getPrecinctId() {
        return precinctId;
    }

    public int getFaceId() {
        return faceId;
    }

    public GraphNode getStartNode() {
        return startNode;
    }

    public GraphEdge getStartEdge() {
        return startEdge;
    }

    public int getSize() {
        return size;
    }

    @Override
    public int compareTo(GraphFace o) {
        if (precinctId == o.precinctId) return faceId - o.faceId;
        else return precinctId - o.precinctId;
    }
}

package geometry;

import java.util.ArrayList;

public class GraphNode implements Comparable<GraphNode> {
    private Graph parent;

    private double x;
    private double y;

    private ArrayList<GraphEdge> outGoings;

    public static GraphNode initGraphNode(double x, double y, Graph parent) {
        GraphNode n = new GraphNode();
        n.parent = parent;
        n.x = x;
        n.y = y;
        n.outGoings = new ArrayList<>();
        return parent.insertNode(n);
    }

    public int outGoes (GraphEdge e) {
        outGoings.add(e);
        return outGoings.size();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double findDistance(GraphNode p) {
        return Math.sqrt(Math.pow(p.x - this.x, 2) + Math.pow(p.y - this.y, 2));
    }

    @Override
    public int compareTo(GraphNode o) {
        if (x == o.x && y == o.y) {
            return 0;
        } else if (x > o.x) {
            return 1;
        } else if (x == o.x){
            if (y > o.y) {
                return 1;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return String.format("(%2.5f, %2.5f)", x, y);
    }
}
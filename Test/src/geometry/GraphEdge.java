package geometry;

public class GraphEdge implements Comparable<GraphEdge> {
    private Graph parent;

    private GraphNode from;
    private GraphNode to;
    private GraphFace left;
    private GraphEdge reverse;
    private GraphNode next;

    public static GraphEdge initGraphEdge (GraphNode from, GraphNode to, GraphFace left, GraphNode next, Graph parent) {
        GraphEdge edge = new GraphEdge();
        edge.parent = parent;
        edge.from = from;
        edge.to = to;
        edge = parent.insertEdge(edge);
        edge.left = left;
        edge.next = next;
        GraphEdge rev = parent.getEdge(to, from);
        if (rev != null) {
            edge.reverse = rev;
            rev.reverse = edge;
        }
        return edge;
    }

    public void setFrom(GraphNode from) {
        this.from = from;
    }

    public void setTo(GraphNode to) {
        this.to = to;
    }

    public GraphNode getFrom() {
        return from;
    }

    public GraphNode getTo() {
        return to;
    }

    public GraphFace getLeft() {
        return left;
    }

    public GraphFace getRight() {
        if (reverse == null) return null;
        return reverse.left;
    }

    public GraphEdge getReverse() {
        return reverse;
    }

    public GraphEdge next() {
        return parent.getEdge(to, next);
    }

    public double getLength () {
        return from.findDistance(to);
    }

    @Override
    public int compareTo(GraphEdge o) {
        if (from.compareTo(o.from) == 0 && to.compareTo(o.to) == 0) {
            return 0;
        } else if (from.compareTo(o.from) > 0) {
            return 1;
        } else if (from.compareTo(o.from) == 0){
            if (to.compareTo(o.to) > 0) {
                return 1;
            }
        }
        return -1;
    }
}

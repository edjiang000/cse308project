
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

public class TextTool {
    private static final String FILENAME = "src\\maine2.geojson";

    public static void main(String[] args) throws Exception {
        FileReader fr = new FileReader(FILENAME);
        int in;

        TreeMap<Point, ArrayList<Integer>> pointToPrecinctMap = new TreeMap<>();
        TreeMap<Integer, ArrayList<Point>> precinctToPointMap = new TreeMap<>();

        int count = 0;
        int precinctId = 0;
        while (findNextGeo(fr) != -1) {
            precinctId++;
            count++;
            ArrayList<Point> pl = generatePoints(fr);
            precinctToPointMap.put(precinctId, pl);
            System.out.println("found: " + pl.size());
            System.out.println(pl.get(0).compareTo(pl.get(pl.size()-1)) == 0);
            for (Point p : pl) {
                ArrayList<Integer> l = pointToPrecinctMap.get(p);
                if (l == null) {
                    l = new ArrayList<>();
                    l.add(precinctId);
                    pointToPrecinctMap.put(p, l);
                } else {
                    if (!l.contains(precinctId))
                        l.add(precinctId);
                }
            }
        }
        Iterator<ArrayList<Integer>> it = pointToPrecinctMap.values().iterator();
        TreeMap<Touch, Integer> touchCounts = new TreeMap<>();
        int counter = 0;
        while (it.hasNext()) {
            ArrayList<Integer> l = it.next();
            if (l.size() == 1) {
                counter++;
                Touch touch = new Touch(0, l.get(0));
                Integer touchNum;
                if ((touchNum = touchCounts.get(touch)) == null) {
                    touchCounts.put(touch, 1);
                } else {
                    touchCounts.put(touch, touchNum+1);
                }
            } else {
                for (int i = 0; i < l.size(); i++) {
                    for (int j = i+1; j < l.size(); j++) {
                        Touch touch = new Touch(l.get(i), l.get(j));
                        Integer touchNum;
                        if ((touchNum = touchCounts.get(touch)) == null) {
                            touchCounts.put(touch, 1);
                        } else {
                            touchCounts.put(touch, touchNum+1);
                        }
                    }
                }
            }
        }

        System.out.println(counter);

        Iterator<Touch> it2 = touchCounts.keySet().iterator();
        ArrayList<Touch> touches = new ArrayList<>();
        while (it2.hasNext()) {
            Touch touch = it2.next();
            int i = touchCounts.get(touch);
            if (i > 20) {
                touches.add(touch);
            }
        }

        for (Touch touch : touches) {
            if (touch.precinct1 == 27 || touch.precinct2 == 27) {
                System.out.println(touch);
            }
        }
        System.out.println(count);
        System.out.println("Totally " + touches.size() + " touches touches two precincts.");



        fr.close();
    }

    private static void fileTest (FileReader fr) throws Exception {
        ArrayList<Point> pl = generatePoints(fr);
        System.out.println(pl.size());
        double min = pl.get(0).findDistance(pl.get(pl.size()-1));
        int posMin = 0;
        double max = min;
        int posMax = 0;
        double tot = min;
        for (int i = 1; i < pl.size(); i++) {
            Point curr = pl.get(i);
            Point prev = pl.get(i-1);
            double dis = curr.findDistance(prev);
            tot += dis;
            if (dis < min) {
                min = dis;
                posMin = i;
            } else if (dis > max) {
                max = dis;
                posMax = i;
            }
        }
        System.out.println(pl.get(1));
        double avg = tot / (pl.size()-1.0);
        System.out.println("Max: " + max);
        System.out.println("Max Pos: " + posMax);
        System.out.println("Min: " + min);
        System.out.println("Min Pos: " + posMin);
        System.out.println("Tot: " + tot);
        System.out.println("Avg: " + avg);
    }

    private static int findNextGeo (FileReader fr) throws Exception {
        int in;
        while ((in = fr.read()) != -1) {
            char c = (char)in;
            if (c == 'g') {
                c = (char) fr.read();
                if (c == 'e') {
                    c = (char) fr.read();
                    if (c == 'o') {
                        c = (char) fr.read();
                        if (c == 'm') {
                            c = (char) fr.read();
                            if (c == 'e') {
                                c = (char) fr.read();
                                if (c == 't') {
                                    c = (char) fr.read();
                                    if (c == 'r') {
                                        c = (char) fr.read();
                                        if (c == 'y') {
                                            return 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }

    private static int findNextObj (FileReader fr) throws Exception {
        int in;
        while ((in = fr.read()) != -1) {
            char c = (char)in;
            if (c == 'O') {
                c = (char) fr.read();
                if (c == 'B') {
                    c = (char) fr.read();
                    if (c == 'J') {
                        c = (char) fr.read();
                        if (c == 'E') {
                            c = (char) fr.read();
                            if (c == 'C') {
                                c = (char) fr.read();
                                if (c == 'T') {
                                    c = (char) fr.read();
                                    if (c == 'I') {
                                        c = (char) fr.read();
                                        if (c == 'D') {
                                            int value = 0;
                                            while ((c = (char)fr.read()) != ',') {
                                                if (Character.isDigit(c)) {
                                                    value *= 10;
                                                    value += Character.getNumericValue(c);
                                                }
                                            }
                                            findNextGeo(fr);
                                            return value;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }

    private static int countOccur (FileReader fr, String target) throws Exception {
        int count = 0;
        char[] chars = target.toCharArray();
        int length = chars.length;
        int in;
        while ((in = fr.read()) != -1) {
            char c = (char) in;
            boolean match = true;
            if (c == chars[0]) {
                for (int i = 1; i < length; i++) {
                    c = (char)fr.read();
                    if (c != chars[i]) {
                        match = false;
                    }
                }
            } else {
                match = false;
            }
            if (match) count++;
        }
        return count;
    }

    private static ArrayList<Point> generatePoints (FileReader fr) throws Exception {
        /*
            0: not start
            1: [ read
            2: - read
            3: n read
            4: . read
            5: n read
            6: , read
            7: - read
            8: n read
            9: , read
            10: n read
         */
        int state = 0;
        StringBuilder sbX = new StringBuilder();
        StringBuilder sbY = new StringBuilder();
        ArrayList<Point> pl = new ArrayList<>();
        int in;
        while ((in = fr.read()) != -1) {
            char c = (char)in;
            if (Character.isWhitespace(c)) continue;
            if (c == '}') break;
            if (state == 0) {
                if (c == '[') state = 1;
            } else if (state == 1) {
                if (c == '-') {
                    state = 2;
                    sbX.append(c);
                } else if (Character.isDigit(c)) {
                    state = 3;
                    sbX.append(c);
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }else if (state == 2) {
                if (Character.isDigit(c)) {
                    state = 3;
                    sbX.append(c);
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }else if (state == 3) {
                if (Character.isDigit(c)) {
                    state = 3;
                    sbX.append(c);
                } else if (c == '.') {
                    state = 4;
                    sbX.append(c);
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }else if (state == 4) {
                if (Character.isDigit(c)) {
                    state = 5;
                    sbX.append(c);
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }else if (state == 5) {
                if (Character.isDigit(c)) {
                    state = 5;
                    sbX.append(c);
                } else if (c == ',') {
                    state = 6;
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }else if (state == 6) {
                if (c == '-') {
                    state = 7;
                    sbY.append(c);
                } else if (Character.isDigit(c)) {
                    state = 8;
                    sbY.append(c);
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }else if (state == 7) {
                if (Character.isDigit(c)) {
                    state = 8;
                    sbY.append(c);
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }else if (state == 8) {
                if (Character.isDigit(c)) {
                    state = 8;
                    sbY.append(c);
                } else if (c == '.') {
                    state = 9;
                    sbY.append(c);
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }else if (state == 9) {
                if (Character.isDigit(c)) {
                    state = 10;
                    sbY.append(c);
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            } else {
                if (Character.isDigit(c)) {
                    state = 10;
                    sbY.append(c);
                } else if (c == ']') {
                    double x = Double.parseDouble(sbX.toString());
                    double y = Double.parseDouble(sbY.toString());
                    Point p = new Point(x, y);
                    pl.add(p);
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                } else {
                    state = 0;
                    sbX = new StringBuilder();
                    sbY = new StringBuilder();
                }
            }
        }
        return pl;
    }
}

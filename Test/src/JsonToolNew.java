

import com.google.gson.*;
import geometry.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class JsonToolNew {

    private static final String FILEPATH = "src\\maine2.geojson";

    public static void main (String[] args) throws Exception {
        Graph graph = new Graph();
        JsonParser parser = new JsonParser();
        String json = new String(Files.readAllBytes(Paths.get(FILEPATH)));
        JsonElement jsonTree = parser.parse(json);
        int precinctId = 0;
        TreeMap<Integer, ArrayList<GraphFace>> faceMap = new TreeMap<>();
        TreeMap<Integer, Double> perimeterMap = new TreeMap<>();
        if(jsonTree.isJsonObject()){
            JsonObject jsonObject = jsonTree.getAsJsonObject();
            JsonElement mainElement = jsonObject.get("features");
            JsonArray mainArray = mainElement.getAsJsonArray();
            for (JsonElement e : mainArray) {
                precinctId++;
//                double length = e.getAsJsonObject().get("properties").getAsJsonObject().get("SHAPE_LENG").getAsDouble();
//                perimeterMap.put(precinctId, length);
                JsonObject geoObject = e.getAsJsonObject().get("geometry").getAsJsonObject();
                String geoType = geoObject.get("type").getAsString();
                JsonArray geoArray = geoObject.get("coordinates").getAsJsonArray();

                if (geoType.equals("Polygon")) {
                    ArrayList<GraphFace> faces = generateGraphFace(precinctId, geoArray, graph);
                    faceMap.put(precinctId, faces);
                } else {
                    ArrayList<GraphFace> faces = new ArrayList<>();
                    for (int p = 0; p < geoArray.size(); p++) {
                        JsonArray pointArray = geoArray.get(p).getAsJsonArray();
                        ArrayList<GraphFace> polygons = generateGraphFace(precinctId, pointArray, graph, faces.size());
                        faces.addAll(polygons);
                    }
                    faceMap.put(precinctId, faces);
                }
            }
        }
        Iterator<GraphEdge> edgeIterator = graph.getAllEdges().iterator();
        ArrayList<GraphEdge> borderGraphEdge = new ArrayList<>();
        while (edgeIterator.hasNext()) {
            GraphEdge edge = edgeIterator.next();
            if (edge.getReverse() == null) {
                borderGraphEdge.add(edge);
            }
        }
        int outerFaceId = 0;
        ArrayList<GraphFace> outer = new ArrayList<>();
        while (!borderGraphEdge.isEmpty()) {
//            System.out.println(borderGraphEdge.size());
            ArrayList<GraphNode> graphBorder = tryBorder(borderGraphEdge);
            if (graphBorder != null) {
                graphBorder.add(graphBorder.get(0));
                GraphFace outerFace = GraphFace.initGraphFace(0, outerFaceId, graphBorder, graph);
                outer.add(outerFace);
//                System.out.println("Yes Border.");
            } else {
                System.out.println("No Border.");
                break;
            }
            edgeIterator = graph.getAllEdges().iterator();
            borderGraphEdge = new ArrayList<>();
            while (edgeIterator.hasNext()) {
                GraphEdge edge = edgeIterator.next();
                if (edge.getReverse() == null) {
                    borderGraphEdge.add(edge);
                }
            }
        }
        faceMap.put(0, outer);

        generateBorderJson(outer);

        State state = new State();
        generateEdges(state, faceMap);
//        boolean finalTest = true;
//        for (Edge e : state.edges) {
//            finalTest &= e.ends.size() == e.starts.size();
//            finalTest &= e.length.size() == e.pointLength.size();
//            finalTest &= e.length.size() == e.ends.size();
//            System.out.println(e.totalLength);
//        }
//        TreeMap<Integer, Double> calculatedPerimeter = new TreeMap<>();

//        for (int i = 1; i <= precinctId; i++) {
            double tot = 0;
            for (Edge e : state.edges) {
                if (e.precinct1 == 4 || e.precinct2 == 4) {
//                    System.out.println(state.edges.indexOf(e));
                    tot += e.totalLength;
                }
            }
//            double readPeri = perimeterMap.get(i);
//            System.out.println("" + i + ": " + (readPeri + 0.000005 > tot && readPeri - 0.000005 < tot));
//        }
System.out.println(tot);
//        explainResult(nodeToPrecinctMap, 20);
    }

    private static void generateBorderJson (ArrayList<GraphFace> faces) throws Exception {
        GraphFace face = faces.get(0);
        JsonObject object = new JsonObject();
        object.addProperty("type", "Polygon");
        JsonArray outer = new JsonArray();
        JsonArray mainBorder = new JsonArray();
        GraphNode startNode = face.getStartNode();
        GraphEdge currentEdge = face.getStartEdge();
        JsonArray coordinate = new JsonArray();
        coordinate.add(currentEdge.getFrom().getX());
        coordinate.add(currentEdge.getFrom().getY());
        mainBorder.add(coordinate);
        while (currentEdge.getTo() != startNode) {
            coordinate = new JsonArray();
            coordinate.add(currentEdge.getTo().getX());
            coordinate.add(currentEdge.getTo().getY());
            mainBorder.add(coordinate);
            currentEdge = currentEdge.next();
        }
        coordinate = new JsonArray();
        coordinate.add(currentEdge.getTo().getX());
        coordinate.add(currentEdge.getTo().getY());
        mainBorder.add(coordinate);
        outer.add(mainBorder);
        object.add("coordinates", outer);
        Gson gson = new Gson();
        String json = gson.toJson(object);
        BufferedWriter writer = new BufferedWriter(new FileWriter("src\\maineStateBoundary.geojson"));
        writer.write(json);

        writer.close();
    }

    private static ArrayList<GraphNode> tryBorder (ArrayList<GraphEdge> candidates) {
        ArrayList<GraphNode> res = new ArrayList<>();
        GraphEdge currentEdge = candidates.get(0);
        GraphNode start = currentEdge.getTo();
        res.add(start);
        while (res.size() != candidates.size()) {
            GraphNode nextNode = currentEdge.getFrom();
            GraphEdge nextEdge = null;
            for (GraphEdge edge : candidates) {
                if (edge.getTo().compareTo(nextNode) == 0) {
                    nextEdge = edge;
                    break;
                }
            }
            if (nextEdge == null) {
                return null;
            } else {
                if (nextEdge.getTo() == start) return res;
                res.add(nextEdge.getTo());
                currentEdge = nextEdge;
            }
        }
        return res;
    }

    private static void generateEdges (State state, TreeMap<Integer, ArrayList<GraphFace>> faceMap) {
        for (Integer precinctId : faceMap.keySet()) {
            ArrayList<GraphFace> faces = faceMap.get(precinctId);
            for (GraphFace face : faces) {
                int prevPrecinctId2 = -1;
                int prevFaceId = -1;
                Edge currentEdge = new Edge();
                GraphEdge graphEdge = face.getStartEdge();
                for (int i = 0; i < face.getSize(); i++) {
                    int precinctId2 = graphEdge.getRight().getPrecinctId();
                    int faceId = graphEdge.getRight().getFaceId();
                    if (precinctId2 != prevPrecinctId2 || faceId != prevFaceId) {
                        prevPrecinctId2 = precinctId2;
                        prevFaceId = faceId;
                        currentEdge.ends.add(graphEdge.getFrom());
                        if (precinctId < precinctId2) {
                            currentEdge = findEdgeFromState(state, precinctId, precinctId2);
                            currentEdge.starts.add(graphEdge.getFrom());
                            currentEdge.length.add(graphEdge.getLength());
                            currentEdge.pointLength.add(1);
                            currentEdge.totalLength += graphEdge.getLength();
                        } else {
                            currentEdge = new Edge();
                        }
                    } else {
                        if (precinctId < precinctId2) {
                            currentEdge.length.set(currentEdge.length.size() - 1, currentEdge.length.get(currentEdge.length.size() - 1) + graphEdge.getLength());
                            currentEdge.pointLength.set(currentEdge.pointLength.size() - 1, currentEdge.pointLength.get(currentEdge.pointLength.size() - 1) + 1);
                            currentEdge.totalLength += graphEdge.getLength();
                        } else {
                            currentEdge = new Edge();
                        }
                    }
                    graphEdge = graphEdge.next();
                }
                currentEdge.ends.add(graphEdge.getFrom());
            }
        }
    }

    private static Edge findEdgeFromState (State state, int precinctId1, int precinctId2) {
        for (Edge e : state.edges) {
            if (e.precinct1 == precinctId1) {
                if (e.precinct2 == precinctId2) {
                    return e;
                }
            } else if (e.precinct2 == precinctId1) {
                if (e.precinct1 == precinctId2) {
                    return e;
                }
            }
        }
        Edge e =  new Edge(precinctId1, precinctId2);
        state.edges.add(e);
        return e;
    }

    private static ArrayList<GraphFace> generateGraphFace
            (int precinctId, JsonArray geoArray, Graph graph) {
        int offset = 0;
        return generateGraphFace(precinctId, geoArray, graph, offset);
    }

    private static ArrayList<GraphFace> generateGraphFace
            (int precinctId, JsonArray geoArray, Graph graph, int faceOffset) {
        ArrayList<GraphFace> res = new ArrayList<>();
        for (int i = 0; i < geoArray.size(); i++) {
            ArrayList<GraphNode> nodeList = new ArrayList<>();
            JsonArray allNodes = geoArray.get(i).getAsJsonArray();

            // Each array in "allNodes" is a coordinate with 2 elements
            for (int k = 0; k < allNodes.size(); k++) {
                JsonArray pair = allNodes.get(k).getAsJsonArray();
                GraphNode c = GraphNode.initGraphNode(pair.get(0).getAsDouble(), pair.get(1).getAsDouble(), graph);
                nodeList.add(c);
            }
            int faceId = i + faceOffset;
            res.add(GraphFace.initGraphFace(precinctId, faceId, nodeList, graph));
        }
        return res;
    }
//
//    private static void explainResult(TreeMap<GraphNode, ArrayList<Integer>> map, int threshold) {
//        Iterator<ArrayList<Integer>> it = map.values().iterator();
//        TreeMap<Touch, Integer> touchCounts = new TreeMap<>();
//        while (it.hasNext()) {
//            ArrayList<Integer> l = it.next();
//            if (l.size() == 1) {
//                Touch touch = new Touch(0, l.get(0));
//                Integer touchNum;
//                if ((touchNum = touchCounts.get(touch)) == null) {
//                    touchCounts.put(touch, 1);
//                } else {
//                    touchCounts.put(touch, touchNum+1);
//                }
//            } else {
//                for (int i = 0; i < l.size(); i++) {
//                    for (int j = i+1; j < l.size(); j++) {
//                        int pid1 = l.get(i);
//                        int pid2 = l.get(j);
////                        if (pid1 >= pid2) continue;
//                        Touch touch = new Touch(pid1, pid2);
//                        Integer touchNum;
//                        if ((touchNum = touchCounts.get(touch)) == null) {
//                            touchCounts.put(touch, 1);
//                        } else {
//                            touchCounts.put(touch, touchNum+1);
//                        }
//                    }
//                }
//            }
//        }
//
//        Iterator<Touch> it2 = touchCounts.keySet().iterator();
//        ArrayList<Touch> touches = new ArrayList<>();
//        while (it2.hasNext()) {
//            Touch touch = it2.next();
//            int i = touchCounts.get(touch);
//            if (i > threshold) {
//                touches.add(touch);
//            }
//        }
//
////        for (Touch touch : touches) {
////            if (touch.precinct1 == 27 || touch.precinct2 == 27) {
////                System.out.println(touch);
////            }
////        }
//        System.out.println("Totally " + touches.size() + " touches touches two precincts.");
//    }
}

import java.util.Comparator;

public class Point implements Comparable<Point> {
    double x;
    double y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    double findDistance(Point p) {
        return Math.sqrt(Math.pow(p.x - this.x, 2) + Math.pow(p.y - this.y, 2));
    }

    @Override
    public int compareTo(Point o) {
        if (x == o.x && y == o.y) {
            return 0;
        } else if (x > o.x) {
            return 1;
        } else if (x == o.x){
            if (y > o.y) {
                return 1;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return String.format("(%2.5f, %2.5f)", x, y);
    }
}

//private static class PointComparator implements Comparator<Point> {
//
//    Point target;
//
//    private PointComparator(Point target) {
//        this.target = target;
//    }
//
//    @Override
//    public int compare(Point o1, Point o2) {
//        double d1 = target.findDistance(o1);
//        double d2 = target.findDistance(o2);
//        if (d1 < d2) return -1;
//        if (d2 > d1) return 1;
//        return 0;
//    }
//}
package com.bluejays.gerrymandering;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@SpringBootApplication
@EntityScan(basePackageClasses = {
		GerrymanderingApplication.class,
		Jsr310JpaConverters.class
})
public class GerrymanderingApplication {

	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("EST"));
	}
	
	public static void main(String[] args) {
		SpringApplication.run(GerrymanderingApplication.class, args);
	}

}

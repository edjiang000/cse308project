package com.bluejays.gerrymandering.geometry;

import java.util.ArrayList;
import java.util.List;

public class GraphNode implements Comparable<GraphNode> {
	private Graph parent;

    private double x;
    private double y;

    private List<GraphEdge> outGoings;

    public static GraphNode initGraphNode(double x, double y, Graph parent) {
        GraphNode n = new GraphNode();
        n.parent = parent;
        n.x = x;
        n.y = y;
        n.outGoings = new ArrayList<>();
        return parent.insertNode(n);
    }

    public int outGoes (GraphEdge e) {
        outGoings.add(e);
        return outGoings.size();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double findDistance(GraphNode p) {
    	// Haversine formula for distance using great-circle radius
        final double R = 6372.8; 
        double lon1 = this.x;
        double lon2 = p.x;
        double lat1 = this.y;
        double lat2 = p.y;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
     
        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double d = R * c;
        return d*1000; // In meters
        //return Math.sqrt(Math.pow(p.x - this.x, 2) + Math.pow(p.y - this.y, 2));
    }

    @Override
    public int compareTo(GraphNode o) {
        if (x == o.x && y == o.y) {
            return 0;
        } else if (x > o.x) {
            return 1;
        } else if (x == o.x){
            if (y > o.y) {
                return 1;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return String.format("(%2.5f, %2.5f)", x, y);
    }
}

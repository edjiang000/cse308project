package com.bluejays.gerrymandering.geometry;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class Graph {
	private Map<GraphEdge, GraphEdge> edges;
    private Map<GraphNode, GraphNode> nodes;
    private Map<GraphFace, GraphFace> faces;

    public Graph () {
        edges = new TreeMap<>();
        nodes = new TreeMap<>();
        faces = new TreeMap<>();
    }

    GraphEdge insertEdge (GraphEdge e) {
        GraphEdge edge = edges.get(e);
        if (edge != null) return edge;
        edges.put(e, e);
        return e;
    }

    public GraphEdge getEdge (GraphNode from, GraphNode to) {
        GraphEdge edge = new GraphEdge();
        edge.setFrom(from);
        edge.setTo(to);
        return edges.get(edge);
    }

    public Collection<GraphEdge> getAllEdges () {
        return edges.values();
    }

    GraphNode insertNode (GraphNode n) {
        GraphNode node = nodes.get(n);
        if (node != null) return node;
        nodes.put(n, n);
        return n;
    }

    GraphFace insertFace (GraphFace f) {
        GraphFace face = faces.get(f);
        if (face != null) return face;
        faces.put(f, f);
        return f;
    }
}

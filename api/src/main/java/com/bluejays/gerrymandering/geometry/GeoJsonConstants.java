package com.bluejays.gerrymandering.geometry;

public class GeoJsonConstants {

    final static String TYPE = "type";
    final static String POLYGON = "Polygon";
    final static String COORDINATES = "coordinates";
    final static String FEATURES = "features";
    final static String GEOMETRY = "geometry";
    final static String PROPERTIES = "properties";
    final static String DISTRICT_ID = "DISTRICT_ID";
    final static String WHITE = "white";
    final static String AFRICAN_AMERICAN = "black";
    final static String ASIAN = "asian";
    final static String NATIVE = "native";
    final static String ISLANDER = "islander";
    final static String OTHER = "other";
    final static String HISPANIC = "hispanic";
    final static String POPULATION = "POP10";
    final static String AREA = "SHAPEarea";
    final static String COUNTY = "COUNTY";
    final static String DEMOCRAT = "democrat";
    final static String REPUBLICAN = "republican";
    final static String OTHERPARTY = "otherparty";


}

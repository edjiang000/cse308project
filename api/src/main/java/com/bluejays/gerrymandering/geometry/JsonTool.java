package com.bluejays.gerrymandering.geometry;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.PartyType;

import static com.bluejays.gerrymandering.geometry.GeoJsonConstants.*;
import com.bluejays.gerrymandering.model.*;
import com.bluejays.gerrymandering.repository.StateRepository;
import com.google.gson.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JsonTool {
    private Map<Integer, List<GraphFace>> faceMap;
    private Collection<Edge> edges;
    private State state;
    private Map<Integer, District> districts;
    private List<Precinct> precincts;
    private Collection<Precinct> movablePrecincts;
    private boolean preprocessed;
    private StateRepository stateRepository;

    @Autowired
    public JsonTool (StateRepository stateRepository) {
        this.stateRepository = stateRepository;
        preprocessed = false;
    }

    public void preprocess(State state, String filePath) throws IOException {
        Graph graph = new Graph();
        this.state = state;
        districts = new TreeMap<>();
        precincts = new ArrayList<>();
        movablePrecincts = new HashSet<>();

        
        Path path = Paths.get(filePath);
        readJson(graph, path);

        generateMovablePrecincts();

        state.setIsDefaultState(true);
        state.setName(path.getName(path.getNameCount()-1).toString().replace(".geojson", ""));
        state.setDistricts(districts.values());
        int disNum;
        if (state.getName().equals("maine")) {
        	disNum = 2;
        } else if (state.getName().equals("arizona")) {
        	disNum = 8;
        } else if (state.getName().equals("pennsylania")) {
        	disNum = 19;
        } else {
        	disNum = 0;
        }
        state.setNumDistricts(disNum);
        state.setPrecincts(precincts);
        state.setEdges(edges);
        state.setMovablePrecincts(movablePrecincts);
        preprocessed = true;
        if (stateRepository != null) {
        	stateRepository.save(state);
        }
    }

    /*
    public void generateBorderJson (List<GraphFace> faces, String destination) throws IOException {
        if (!preprocessed) {
            preprocess(state);
        }
        GraphFace face = faces.get(0);
        JsonObject object = new JsonObject();
        object.addProperty(TYPE, POLYGON);
        JsonArray outer = new JsonArray();
        JsonArray mainBorder = new JsonArray();
        GraphNode startNode = face.getStartNode();
        GraphEdge currentEdge = face.getStartEdge();
        JsonArray coordinate = new JsonArray();
        coordinate.add(currentEdge.getFrom().getX());
        coordinate.add(currentEdge.getFrom().getY());
        mainBorder.add(coordinate);
        while (currentEdge.getTo() != startNode) {
            coordinate = new JsonArray();
            coordinate.add(currentEdge.getTo().getX());
            coordinate.add(currentEdge.getTo().getY());
            mainBorder.add(coordinate);
            currentEdge = currentEdge.getNext();
        }
        coordinate = new JsonArray();
        coordinate.add(currentEdge.getTo().getX());
        coordinate.add(currentEdge.getTo().getY());
        mainBorder.add(coordinate);
        outer.add(mainBorder);
        object.add(COORDINATES, outer);
        Gson gson = new Gson();
        String json = gson.toJson(object);
        BufferedWriter writer = new BufferedWriter(new FileWriter(destination));
        writer.write(json);
        writer.close();
    }
	*/

    private void readJson (Graph graph, Path filePath) throws IOException {
        JsonParser parser = new JsonParser();
        String json = new String(Files.readAllBytes(filePath));
        JsonElement jsonTree = parser.parse(json);
        faceMap = new TreeMap<>();
        if(jsonTree.isJsonObject()){
            JsonObject jsonObject = jsonTree.getAsJsonObject();
            JsonElement mainElement = jsonObject.get("features");
            JsonArray mainArray = mainElement.getAsJsonArray();
            Precinct out = new Precinct();
            out.setId(0);
            out.setState(state);
            precincts.add(out);
            for (int i = 1; i <= mainArray.size(); i++) {
                Precinct precinct = new Precinct();
                precinct.setId(i);
                precinct.setState(state);
                precincts.add(precinct);
            }
            
            for (int i = 0; i < mainArray.size(); i++) {
                JsonElement e = mainArray.get(i);
                Precinct precinct = precincts.get(i+1);
                JsonObject precinctObject = e.getAsJsonObject();
                fetchGeometryModule(precinct, graph, precinctObject);
                fetchDemographicModule(precinct, precinctObject);
                fetchPopulationModule(precinct, precinctObject);
                fetchAreaModule(precinct, precinctObject);
                fetchCountyModule(precinct, precinctObject);
                fetchPartyModule(precinct, precinctObject);
            }
        }
        generateOuterBorder(graph);
        generateEdges();
        if(jsonTree.isJsonObject()){
            JsonObject jsonObject = jsonTree.getAsJsonObject();
            JsonElement mainElement = jsonObject.get("features");
            JsonArray mainArray = mainElement.getAsJsonArray();
            for (int i = 0; i < mainArray.size(); i++) {
                JsonElement e = mainArray.get(i);
                Precinct precinct = precincts.get(i+1);
                JsonObject precinctObject = e.getAsJsonObject();
                fetchDistrictModule(precinct, precinctObject);
            }
        }
    }

	private List<GraphNode> tryBorder (List<GraphEdge> candidates) {
        ArrayList<GraphNode> res = new ArrayList<>();
        GraphEdge currentEdge = candidates.get(0);
        GraphNode start = currentEdge.getTo();
        res.add(start);
        while (res.size() != candidates.size()) {
            GraphNode nextNode = currentEdge.getFrom();
            GraphEdge nextEdge = null;
            for (GraphEdge edge : candidates) {
                if (edge.getTo().compareTo(nextNode) == 0) {
                    nextEdge = edge;
                    break;
                }
            }
            if (nextEdge == null) {
                return null;
            } else {
                if (nextEdge.getTo() == start) return res;
                res.add(nextEdge.getTo());
                currentEdge = nextEdge;
            }
        }
        return res;
    }

    private void fetchGeometryModule (Precinct precinct, Graph graph, JsonObject precinctObject) {
        JsonObject geoObject = precinctObject.get(GEOMETRY).getAsJsonObject();
        String geoType = geoObject.get(TYPE).getAsString();
        JsonArray geoArray = geoObject.get(COORDINATES).getAsJsonArray();
        if (geoType.equals(POLYGON)) {
            List<GraphFace> faces = generateGraphFace(precinct.getId(), geoArray, graph);
            faceMap.put(precinct.getId(), faces);
        } else {
            ArrayList<GraphFace> faces = new ArrayList<>();
            for (int p = 0; p < geoArray.size(); p++) {
                JsonArray pointArray = geoArray.get(p).getAsJsonArray();
                List<GraphFace> polygons = generateGraphFace(precinct.getId(), pointArray, graph, faces.size());
                faces.addAll(polygons);
            }
            faceMap.put(precinct.getId(), faces);
        }
    }
    
    private void fetchPartyModule(Precinct precinct, JsonObject precinctObject) {
    	JsonObject propertyObject = precinctObject.get(PROPERTIES).getAsJsonObject();
    	double demVotes = propertyObject.get(DEMOCRAT).getAsDouble();
    	double repVotes = propertyObject.get(REPUBLICAN).getAsDouble();
    	double othVotes = propertyObject.get(OTHERPARTY).getAsDouble();
    	
    	Map<PartyType, Double> m = new HashMap<>();
    	m.put(PartyType.DEMOCRATIC, demVotes);
    	m.put(PartyType.REPUBLICAN, repVotes);
    	m.put(PartyType.OTHERPARTY, othVotes);

    	precinct.setPartyVotes(m);
    }

    private void fetchDistrictModule (Precinct precinct, JsonObject precinctObject) {
        JsonObject propertyObject = precinctObject.get(PROPERTIES).getAsJsonObject();
        int districtId = propertyObject.get(DISTRICT_ID).getAsInt();
        District district = districts.get(districtId);
        if (district == null) {
            district = new District();
            district.setState(state);
            district.setId(districtId);
            districts.put(districtId, district);
        }
        district.addPrecinctSafe(precinct);
        precinct.setDistrict(district);
    }

    private void fetchDemographicModule (Precinct precinct, JsonObject precinctObject) {
            JsonObject propertiesObject = precinctObject.get(PROPERTIES).getAsJsonObject();
            int white = propertiesObject.get(WHITE).getAsInt();
            int africanAmerican = propertiesObject.get(AFRICAN_AMERICAN).getAsInt();
            int nativeAmerican = propertiesObject.get(NATIVE).getAsInt();
            int asian = propertiesObject.get(ASIAN).getAsInt();
            int islander = propertiesObject.get(ISLANDER).getAsInt();
            int other = propertiesObject.get(OTHER).getAsInt();
            int hispanic = propertiesObject.get(HISPANIC).getAsInt();

            Map<DemographicType, Integer> demographics = new HashMap<>();
            demographics.put(DemographicType.CAUCASIAN, white);
            demographics.put(DemographicType.AFRICAN_AMERICAN, africanAmerican);
            demographics.put(DemographicType.NATIVE_AMERICAN, nativeAmerican);
            demographics.put(DemographicType.ASIAN, asian);
            demographics.put(DemographicType.ISLANDER, islander);
            demographics.put(DemographicType.OTHER, other);
            demographics.put(DemographicType.HISPANIC, hispanic);
            precinct.setDemographics(demographics);
    }

    private void fetchPopulationModule (Precinct precinct, JsonObject precinctObject) {
        JsonObject propertiesObject = precinctObject.get(PROPERTIES).getAsJsonObject();
        int population = propertiesObject.get(POPULATION).getAsInt();
        precinct.setPopulation(population);
    }
    
    private void fetchAreaModule (Precinct precinct, JsonObject precinctObject) {
        JsonObject propertiesObject = precinctObject.get(PROPERTIES).getAsJsonObject();
        double area = propertiesObject.get(AREA).getAsDouble();
        precinct.setArea(area);
    }
    
    private void fetchCountyModule(Precinct precinct, JsonObject precinctObject) {
        JsonObject propertiesObject = precinctObject.get(PROPERTIES).getAsJsonObject();
        String county = propertiesObject.get(COUNTY).getAsString();
        precinct.setCounty(county);
	}
    
    private void  generateOuterBorder (Graph graph) {
        Iterator<GraphEdge> edgeIterator = graph.getAllEdges().iterator();
        ArrayList<GraphEdge> borderGraphEdge = new ArrayList<>();
        while (edgeIterator.hasNext()) {
            GraphEdge edge = edgeIterator.next();
            if (edge.getReverse() == null) {
                borderGraphEdge.add(edge);
            }
        }
        int outerFaceId = 0;
        ArrayList<GraphFace> outer = new ArrayList<>();
        while (!borderGraphEdge.isEmpty()) {
            List<GraphNode> graphBorder = tryBorder(borderGraphEdge);
            if (graphBorder != null) {
                graphBorder.add(graphBorder.get(0));
                GraphFace outerFace = GraphFace.initGraphFace(0, outerFaceId, graphBorder, graph);
                outer.add(outerFace);
                outerFaceId++;
            }
            edgeIterator = graph.getAllEdges().iterator();
            borderGraphEdge = new ArrayList<>();
            while (edgeIterator.hasNext()) {
                GraphEdge edge = edgeIterator.next();
                if (edge.getReverse() == null) {
                    borderGraphEdge.add(edge);
                }
            }
        }
        faceMap.put(0, outer);
    }

    private void generateEdges () {
        edges = new HashSet<>();
        for (Integer precinctId : faceMap.keySet()) {
            List<GraphFace> faces = faceMap.get(precinctId);
            for (GraphFace face : faces) {
                int prevPrecinctId2 = -1;
                int prevFaceId = -1;
                Edge currentEdge = new Edge();
                GraphEdge graphEdge = face.getStartEdge();
                for (int i = 0; i < face.getSize(); i++) {
                    int precinctId2 = graphEdge.getRight().getPrecinctId();
                    int faceId = graphEdge.getRight().getFaceId();
                    if (precinctId2 != prevPrecinctId2 || faceId != prevFaceId) {
                        prevPrecinctId2 = precinctId2;
                        prevFaceId = faceId;
                        currentEdge.appendEnds(graphEdge.getFrom());
                        if (precinctId < precinctId2) {
                            currentEdge = findEdge(precincts.get(precinctId), precincts.get(precinctId2));
                            currentEdge.appendStarts(graphEdge.getFrom());
                            currentEdge.appendLength(graphEdge.getLength());
                            currentEdge.appendPointLength(1);
                            currentEdge.increaseTotalLength(graphEdge.getLength());
                        } else {
                            currentEdge = new Edge();
                        }
                    } else {
                        if (precinctId < precinctId2) {
                            currentEdge.lengthGrow(graphEdge.getLength());
                            currentEdge.pointLengthGrow(1);
                            currentEdge.increaseTotalLength(graphEdge.getLength());
                        } else {
                            currentEdge = new Edge();
                        }
                    }
                    graphEdge = graphEdge.getNext();
                }
                currentEdge.appendEnds(graphEdge.getFrom());
            }
        }
    }

    private void generateMovablePrecincts() {
        for (District district : districts.values()) {
            for (Edge edge : district.getBorder()) {
                if (edge.getPrecinct1().getId() != 0 && edge.getPrecinct2().getId() != 0) {
                    movablePrecincts.add(edge.getPrecinct1());
                    movablePrecincts.add(edge.getPrecinct2());
                }
            }
        }
    }

    private Edge findEdge (Precinct precinct1, Precinct precinct2) {
        for (Edge e : edges) {
            if (e.getPrecinct1().getId() == precinct1.getId()) {
                if (e.getPrecinct2().getId()  == precinct2.getId()) {
                    return e;
                }
            } else if (e.getPrecinct2().getId() == precinct1.getId()) {
                if (e.getPrecinct1().getId()  == precinct2.getId()) {
                    return e;
                }
            }
        }
        Edge e =  new Edge(precinct1, precinct2);
        e.setState(state);
        edges.add(e);
        precinct1.addEdge(e);
        precinct2.addEdge(e);
        return e;
    }

    private List<GraphFace> generateGraphFace
            (int precinctId, JsonArray geoArray, Graph graph) {
        int offset = 0;
        return generateGraphFace(precinctId, geoArray, graph, offset);
    }

    private List<GraphFace> generateGraphFace
            (int precinctId, JsonArray geoArray, Graph graph, int faceOffset) {
        ArrayList<GraphFace> res = new ArrayList<>();
        for (int i = 0; i < geoArray.size(); i++) {
            ArrayList<GraphNode> nodeList = new ArrayList<>();
            JsonArray allNodes = geoArray.get(i).getAsJsonArray();

            for (int k = 0; k < allNodes.size(); k++) {
                JsonArray pair = allNodes.get(k).getAsJsonArray();
                GraphNode c = GraphNode.initGraphNode(pair.get(0).getAsDouble(), pair.get(1).getAsDouble(), graph);
                nodeList.add(c);
            }
            int faceId = i + faceOffset;
            res.add(GraphFace.initGraphFace(precinctId, faceId, nodeList, graph));
        }
        return res;
    }

    public boolean isPreprocessed() {
        return preprocessed;
    }
}

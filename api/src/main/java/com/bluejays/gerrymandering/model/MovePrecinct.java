package com.bluejays.gerrymandering.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "moves")
public class MovePrecinct {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int moveId;
	private int precinctId;
	@ManyToOne
	@JoinColumn(name = "state_id")
	private State state;
	private int toDistrictId;
	private int fromDistrictId;
	@Transient
	private boolean join;
	@Transient
	private boolean noMovesRemaining;
	private int desiredNumDistricts;
	@Transient
	private MoveData moveData;
	
	public MovePrecinct() {}
	
	public MovePrecinct(int moveId, int precinctId, State state, int toDistrictId, int fromDistrictId) {
		super();
		this.moveId = moveId;
		this.precinctId = precinctId;
		this.state = state;
		this.toDistrictId = toDistrictId;
		this.fromDistrictId = fromDistrictId;
	}

	public MovePrecinct(int precinctId, int toDistrictId, int fromDistrictId, boolean join) {
		this.precinctId = precinctId;
		this.toDistrictId = toDistrictId;
		this.fromDistrictId = fromDistrictId;
		this.join = join;
		this.noMovesRemaining = false;
	}
	
	public MovePrecinct(boolean noMovesRemaining) {
		this.setNoMovesRemaining(noMovesRemaining);
	}
	
	public int getMoveId() {
		return moveId;
	}
	
	public void setMoveId(int moveId) {
		this.moveId = moveId;
	}
	
	public int getPrecinctId() {
		return precinctId;
	}
	
	public void setPrecinctId(int precinctId) {
		this.precinctId = precinctId;
	}
	
	public Integer getToDistrictId() {
		return toDistrictId;
	}
	
	public void setToDistrictId(Integer toDistrictId) {
		this.toDistrictId = toDistrictId;
	}
	
	public Integer getFromDistrictId() {
		return fromDistrictId;
	}
	
	public void setFromDistrictId(Integer fromDistrictId) {
		this.fromDistrictId = fromDistrictId;
	}

	public Boolean getNoMovesRemaining() {
		return noMovesRemaining;
	}

	public void setNoMovesRemaining(Boolean noMovesRemaining) {
		this.noMovesRemaining = noMovesRemaining;
	}

	public boolean isJoin() {
		return join;
	}

	public void setJoin(boolean join) {
		this.join = join;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public void setToDistrictId(int toDistrictId) {
		this.toDistrictId = toDistrictId;
	}

	public void setFromDistrictId(int fromDistrictId) {
		this.fromDistrictId = fromDistrictId;
	}

	public int getDesiredNumDistricts() {
		return desiredNumDistricts;
	}

	public void setDesiredNumDistricts(int desiredNumDistricts) {
		this.desiredNumDistricts = desiredNumDistricts;
	}

	public MoveData getMoveData() {
		return moveData;
	}

	public void setMoveData(MoveData moveData) {
		this.moveData = moveData;
	}
}

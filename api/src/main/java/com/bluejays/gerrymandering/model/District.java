package com.bluejays.gerrymandering.model;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.PartyType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "district")
public class District {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "district_id")
	private int db_id;
	
    private int id;
    @JsonManagedReference
	@OneToMany(mappedBy = "district", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<Precinct> precincts;
	@Transient
    private Collection<Edge> border;
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "state_id", nullable = false)
	private State state;
	
	private int population;
	private boolean isMajMin;
	private double compactness;
	private double area;
	private double perimeter;
	private boolean isConnected;
	private int minorityPopulation;

	public District() {
        precincts = new HashSet<>();
        border = new HashSet<>();
    }

    public District(int id, Cluster cluster) {
        this.id = id;
        this.state = cluster.getState();
        this.precincts = new HashSet<>();
        this.precincts.addAll(cluster.getPrecincts());
        this.precincts.forEach(p -> {
        	p.setDistrict(this);
        });
        this.border = new HashSet<>();
        for (ClusterEdge clusterEdge : cluster.getBorder()) {
            this.border.addAll(clusterEdge.getEdges());
        }
        updateArea();
        updatePerimeter();
        updateCompactness();
        updatePopulation();
        updateMinority();
        updateMajMinDistrict();
        this.isConnected = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Collection<Precinct> getPrecincts() {
        return Collections.unmodifiableCollection(precincts);
    }

    public Collection<Edge> getBorder() {
        return Collections.unmodifiableCollection(border);
    }
    
    public Collection<Precinct> getMutablePrecincts() {
    	return precincts;
    }
    
    public void setPrecincts(Collection<Precinct> precincts) {
    	this.precincts = new HashSet<>();
    	precincts.forEach(p -> this.addPrecinct(p));
    }
    
    public void setPrecinctsSafe(Collection<Precinct> precincts) {
    	this.precincts = new HashSet<>();
    	precincts.forEach(p -> this.addPrecinctSafe(p));
    }

    public Collection<Edge> getMutableBorder() {
    	return border;
    }
    
    public Collection<Precinct> addPrecinct (Precinct precinct) {
        if (precincts.contains(precinct)) {
            return null;
        } else {
            precincts.add(precinct);
            this.area += precinct.getArea();
        	Collection<Precinct> addedMovables = new HashSet<>();
        	for (Edge edge : precinct.getEdges()) {
        		if (border.contains(edge)) {
        			border.remove(edge);
        			this.perimeter -= edge.getTotalLength();
        		} else {
        			border.add(edge);
        			Precinct other = edge.getConnection(precinct);
        			if (other.getId() != 0) {
	        			addedMovables.add(other);
        			}
        			this.perimeter += edge.getTotalLength();
        		}
        	}
        	precinct.setDistrict(this);
        	this.population += precinct.getPopulation();
        	this.minorityPopulation += precinct.getDemopraphic(DemographicType.AFRICAN_AMERICAN);
        	updateMajMinDistrict();
        	updateCompactness();
        	updateConnected();
            return addedMovables;
        }
    }
    
    public void addPrecinctSafe (Precinct precinct) {
        if (precincts.contains(precinct)) {
            return;
        } else {
            precincts.add(precinct);
            this.area += precinct.getArea();
        	for (Edge edge : precinct.getEdges()) {
        		if (border.contains(edge)) {
        			border.remove(edge);
        			this.perimeter -= edge.getTotalLength();
        		} else {
        			border.add(edge);
        			this.perimeter += edge.getTotalLength();
        		}
        	}
        	precinct.setDistrict(this);
        	this.population += precinct.getPopulation();
        	this.minorityPopulation += precinct.getDemopraphic(DemographicType.AFRICAN_AMERICAN);
        	updateMajMinDistrict();
        	updateCompactness();
        }
    }
    
    public Collection<Precinct> removePrecinct (Precinct precinct) {
    	
        if (!precincts.contains(precinct)) {
            return null;
        } else {
            precincts.remove(precinct);
            this.area -= precinct.getArea();
        	Collection<Precinct> removedMovables = new HashSet<>();
        	for (Edge edge : precinct.getEdges()) {
        		if (border.contains(edge)) {
        			border.remove(edge);
        			removedMovables.add(edge.getConnection(precinct));
        			this.perimeter -= edge.getTotalLength();
        		} else {
        			border.add(edge);
        			this.perimeter += edge.getTotalLength();
        		}
        	}
        	precinct.setDistrict(null);
        	this.population -= precinct.getPopulation();
        	this.minorityPopulation -= precinct.getDemopraphic(DemographicType.AFRICAN_AMERICAN);
        	updateMajMinDistrict();
        	updateCompactness();
        	if (precincts.size() > 0) {
        		updateConnected();
        	}
            return removedMovables;
        }
    } 

    private void updateArea() {
    	double area = 0;
    	for(Precinct precinct : precincts){
    		area += precinct.getArea();
    	}
    	this.area = area;
    }
    
    private void updatePerimeter() {
    	double perimeter = 0;
    	for(Edge edge : border) {
    		perimeter += edge.getTotalLength();
    	}
    	this.perimeter = perimeter;
    }
    
    private void updateCompactness() {
        this.compactness =  (4.0*Math.PI*area)/Math.pow(perimeter,2);
    }
    
	public double getPartyVote(PartyType party) {
		if(this.id == 0) return 0;
		double votes = 0;
		for(Precinct p : precincts) {
			votes += p.getPartyVote(party);
		}
		return votes;
	}
	
	public double getTotalVotes() {
		int votes = 0;
		votes += getPartyVote(PartyType.DEMOCRATIC);
		votes += getPartyVote(PartyType.REPUBLICAN);
		votes += getPartyVote(PartyType.OTHERPARTY);
		return votes;
	}

    public double getPVI() {
    	if (id == 0) {
    		return 0.0;
    	}
   		double totalVotes = getTotalVotes();
    	if(totalVotes == 0) {
   			return 0.0;
    	}
    	double demVotes = getPartyVote(PartyType.DEMOCRATIC);
    	double repVotes = getPartyVote(PartyType.REPUBLICAN);
    	return (demVotes-repVotes)/totalVotes;
    }

    public Map<DemographicType, Integer> getDemographics() {
    	Map<DemographicType, Integer> r = new HashMap<DemographicType, Integer>();
		EnumSet.allOf(DemographicType.class).forEach(demographicType->{
			r.put(demographicType, 0);
		});
    	for (Precinct precinct : precincts) {
    		Map<DemographicType, Integer> m = precinct.getDemographics();
    		EnumSet.allOf(DemographicType.class).forEach(demographicType->{
    			int value = 0;
    			if(m.containsKey(demographicType) && r.containsKey(demographicType)) {
        			value = r.get(demographicType) + m.get(demographicType);
    			}
    			r.put(demographicType, value);
    		});
    	}
    	return r;
    }

	public double getQuality() {
		// TODO Implement district quality
		return 0;
	}
	
	private void updatePopulation() {
    	int population = 0;
    	for(Precinct p : precincts) {
    		population += p.getPopulation();
    	}
    	this.population =  population;
    }
	
	private void updateConnected() {
//		return;
		
		if (precincts.size() == 1) {
			isConnected =  true;
			return;
		}
		Precinct start = precincts.iterator().next();
		int[] visited = new int[state.getPrecincts().size()];
		visited[start.getId()]++;
		ArrayDeque<Precinct> stack = new ArrayDeque<>();
		stack.push(start);
		while (!stack.isEmpty()) {
			Precinct current = stack.pop();
			for (Edge edge : current.getEdges()) {
				Precinct other = edge.getConnection(current);
				if (other.getDistrict() != this) continue;
				if (visited[other.getId()] == 0) {
					stack.push(other);
					visited[other.getId()]++;
				}
			}
		}
		int sum = 0;
		for (int i : visited) {
			sum += i;
		}
		if (sum > precincts.size())
			System.out.println("Error in district.isConnected()");
		this.isConnected =  sum == precincts.size();
		
	}
	
	private void updateMinority() {
		this.minorityPopulation = this.getDemographic(DemographicType.AFRICAN_AMERICAN);
	}
	
	private void updateMajMinDistrict() {
		if (this.population == 0) {
			this.isMajMin = false;
			return;
		}
    	double min = state.getMinMajMin();
    	double max = state.getMaxMajMin();
		double ratio = this.minorityPopulation / (double)this.population;
		this.isMajMin =  ratio >= min && ratio <= max;
    }
	
	public int getDemographic(DemographicType demographicType) {
    	if(id == 0) return 0;
    	int value = 0;
    	for (Precinct precinct : precincts) {
    		Map<DemographicType, Integer> m = precinct.getDemographics();
			if(m.containsKey(demographicType)) {
    			value += m.get(demographicType);
			}
    	}
    	return value;
    }

	public double getEfficiencyGap() {
		if(id == 0) {
			return 0.0;
		}
		double demVotes = getPartyVote(PartyType.DEMOCRATIC);
		double repVotes = getPartyVote(PartyType.REPUBLICAN);
		return Math.abs(demVotes-repVotes)/getTotalVotes();
	}
	public int getPopulation() {
		return population;
	}

	public boolean isMajMinDistrict() {
		return isMajMin;
	}

	public double getCompactness() {
		return compactness;
	}

	public boolean isConnected() {
		return isConnected;
	}
	
	public int getMinorityPopulation() {
		return minorityPopulation;
	}
	
}
package com.bluejays.gerrymandering.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MoveData {
	private HashMap<String, Double> dataMap = new HashMap<>();
	private List<DistrictData> districtDataMap = new ArrayList<>();
	
	public HashMap<String, Double> getDataMap() {
		return dataMap;
	}
	
	public void setDataMap(HashMap<String, Double> dataMap) {
		this.dataMap = dataMap;
	}

	public List<DistrictData> getDistrictDataMap() {
		return districtDataMap;
	}

	public void setDistrictDataMap(List<DistrictData> districtDataMap) {
		this.districtDataMap = districtDataMap;
	}
}

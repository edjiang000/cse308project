package com.bluejays.gerrymandering.model;

import java.util.Map;

import com.bluejays.gerrymandering.enums.PartyType;
import com.bluejays.gerrymandering.enums.WeightType;

public class BatchResult {
	private Integer batchRunId;
	private String stateName;
	private Integer resultingStateId;
	private Map<PartyType, Integer> seatsByParty;
	private Double objectiveFunctionValue;
	private Map<WeightType, Double> summaryOfMeasures;
	private Double percentageDone;
	private String status;
	
	public BatchResult() {}

	public BatchResult(Integer batchRunId, String stateName, Integer resultingStateId,
			Map<PartyType, Integer> seatsByParty, Double objectiveFunctionValue,
			Map<WeightType, Double> summaryOfMeasures, Double percentageDone, String status) {
		this.batchRunId = batchRunId;
		this.stateName = stateName;
		this.resultingStateId = resultingStateId;
		this.seatsByParty = seatsByParty;
		this.objectiveFunctionValue = objectiveFunctionValue;
		this.summaryOfMeasures = summaryOfMeasures;
		this.percentageDone = percentageDone;
		this.status = status;
	}

	public Integer getBatchRunId() {
		return batchRunId;
	}

	public void setBatchRunId(Integer batchRunId) {
		this.batchRunId = batchRunId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Integer getResultingStateId() {
		return resultingStateId;
	}

	public void setResultingStateId(Integer resultingStateId) {
		this.resultingStateId = resultingStateId;
	}

	public Map<PartyType, Integer> getSeatsByParty() {
		return seatsByParty;
	}

	public void setSeatsByParty(Map<PartyType, Integer> seatsByParty) {
		this.seatsByParty = seatsByParty;
	}

	public Double getObjectiveFunctionValue() {
		return objectiveFunctionValue;
	}

	public void setObjectiveFunctionValue(Double objectiveFunctionValue) {
		this.objectiveFunctionValue = objectiveFunctionValue;
	}

	public Map<WeightType, Double> getSummaryOfMeasures() {
		return summaryOfMeasures;
	}

	public void setSummaryOfMeasures(Map<WeightType, Double> summaryOfMeasures) {
		this.summaryOfMeasures = summaryOfMeasures;
	}

	public Double getPercentageDone() {
		return percentageDone;
	}

	public void setPercentageDone(Double percentageDone) {
		this.percentageDone = percentageDone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}

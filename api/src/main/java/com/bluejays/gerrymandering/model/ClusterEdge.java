package com.bluejays.gerrymandering.model;

import java.util.Collection;
import java.util.HashSet;

import com.bluejays.gerrymandering.function.FunctionCalculator;

public class ClusterEdge implements Comparable<ClusterEdge> {
	private Cluster cluster1;
    private Cluster cluster2;
    private Collection<Edge> edges;
    private double joinability;
    
    public ClusterEdge (Cluster cluster1, Cluster cluster2) {
        this.cluster1 = cluster1;
        this.cluster2 = cluster2;
        this.edges = new HashSet<>();
        this.joinability = -1;
    }

    public Collection<Edge> getEdges () {
        return edges;
    }

    public Cluster getCluster1() {
        return cluster1;
    }

    public Cluster getCluster2() {
        return cluster2;
    }

    public Cluster getConnection (Cluster cluster) {
        if (cluster1 == cluster)
            return cluster2;
        if (cluster2 == cluster)
            return cluster1;
        return null;
    }

    public double updateJoinability(FunctionCalculator calculator) {
    	if (cluster1.getId() == 0 || cluster2.getId() == 0) joinability = -1;
    	else joinability = calculator.computeJoinability(this);
    	return joinability;
    }
    
    public double getJoinability() {
    	return joinability;
    }

    @Override
    public int compareTo(ClusterEdge o) {
        double dis = this.getJoinability();
        double other = o.getJoinability();
        return -Double.compare(dis, other);
    }
    
    public double getEdgeLength() {
    	double totalLength = 0;
    	for(Edge edge : edges) {
    		totalLength += edge.getTotalLength();
    	}
    	return totalLength;
    }
    
    @Override
    public String toString () {
    	return "[" + cluster1.getId() + "," + cluster2.getId() + "]";
    }
    
    public boolean isSame (ClusterEdge other) {
    	return (this.cluster1 == other.cluster1 && this.cluster2 == other.cluster2) || (this.cluster1 == other.cluster2 && this.cluster2 == other.cluster1);
    }
}

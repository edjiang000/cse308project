package com.bluejays.gerrymandering.model;

import java.util.List;

public class BatchRun {
	private String stateName;
	private List<AlgorithmRun> algorithmRuns;
	
	public BatchRun() {}

	public BatchRun(String stateName, List<AlgorithmRun> algorithmRuns) {
		this.stateName = stateName;
		this.algorithmRuns = algorithmRuns;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public List<AlgorithmRun> getAlgorithmRuns() {
		return algorithmRuns;
	}

	public void setAlgorithmRuns(List<AlgorithmRun> algorithmRuns) {
		this.algorithmRuns = algorithmRuns;
	}
}

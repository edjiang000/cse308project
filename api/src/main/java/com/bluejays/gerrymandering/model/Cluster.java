package com.bluejays.gerrymandering.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Map;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.PartyType;
import com.bluejays.gerrymandering.function.FunctionCalculator;

public class Cluster {
	private int id;
    private State state;
    private Collection<Precinct> precincts;
    private Collection<ClusterEdge> border;

    public Cluster (Precinct precinct) {
        this.id = precinct.getId();
        this.state = precinct.getState();
        this.precincts = new HashSet<>();
        this.precincts.add(precinct);
        this.border = new HashSet<>();
    }

    public Cluster() {};

    public int getId() {
        return id;
    }

    public Collection<Precinct> getPrecincts() {
        return precincts;
    }

    public Collection<ClusterEdge> getBorder() {
        return border;
    }

    public State getState() {
        return state;
    }

    public ClusterEdge getNeighboring (Cluster other) {
        for(ClusterEdge clusterEdge : border) {
            if(clusterEdge.getConnection(this) == other)
                return clusterEdge;
        }
        return null;
    }

    public static MovePrecinct joinCluster (FunctionCalculator calculator, Cluster c1, Cluster c2) {
        if (c1 != null && c2 != null) {
            //c1.state.getClusters().remove(c2);
        	c1.state.removeCluster(c2);
        	c1.state.getMajMinCluster().remove(c2);
            List<Integer> precinctsToMove = c2.getPrecincts().stream().map(Precinct::getId).collect(Collectors.toList());
            c1.getPrecincts().addAll(c2.getPrecincts());
            Collection<ClusterEdge> stateEdges = c1.state.getClusterEdges();
            Collection<ClusterEdge> c1Borders = c1.getBorder();
            Collection<ClusterEdge> c2Borders = c2.getBorder();
            for (ClusterEdge c2Edge : c2Borders) {
                Cluster other = c2Edge.getConnection(c2);
                other.getBorder().remove(c2Edge);
                stateEdges.remove(c2Edge);
                if (other == c1) {
                	continue;
                } else if (c1.getNeighboring(other) == null) {
                    ClusterEdge newEdge = new ClusterEdge(c1, other);
                    newEdge.getEdges().addAll(c2Edge.getEdges());
                    c1Borders.add(newEdge);
                    other.getBorder().add(newEdge);
                } else {
                    c1.getNeighboring(other).getEdges().addAll(c2Edge.getEdges());
                }
            }
            if (!c1.isMajMinCluster()) {
            	c1.state.getMajMinCluster().remove(c1);
            } 
            if (c1.isMajMinCluster())  {
            	c1.state.getMajMinCluster().add(c1);
            }
            if (c1.state.getMajMinCluster().size() == c1.getState().getNumMajMin()) {
            	for (Cluster majMinCluster : c1.state.getMajMinCluster()) {
            		for (ClusterEdge majMinEdge: majMinCluster.getBorder()) {
                    	stateEdges.remove(majMinEdge);
                    	majMinEdge.updateJoinability(calculator);
                    	stateEdges.add(majMinEdge);
                    }
            	}
            }
            for (ClusterEdge c1Edge: c1Borders) {
            	stateEdges.remove(c1Edge);
            	c1Edge.updateJoinability(calculator);
            	stateEdges.add(c1Edge);
            }
            return new MovePrecinct(precinctsToMove.get(0), c1.getId(), c2.getId(), true);
        }
        return null;
    }
    
    public int getPopulation() {
    	int population = 0;
    	for(Precinct p : precincts) {
    		population += p.getPopulation();
    	}
    	return population;
    }
    
    public double getArea() {
    	double area = 0;
    	for(Precinct precinct : precincts) {
    		area += precinct.getArea();
    	}
    	return area;
    }
    
    public boolean isMajMinCluster() {
		int pop = this.getPopulation();
		if (pop == 0) return false;
    	double min = state.getMinMajMin();
    	double max = state.getMaxMajMin();
    	int AM = this.getDemographic(DemographicType.AFRICAN_AMERICAN);
		double ratio = AM / (double)pop;
		return ratio >= min && ratio <= max;
    }
    
    public double getPerimeter() {
    	double perimeter = 0;
    	for(ClusterEdge clusterEdge : border) {
    		for(Edge edge : clusterEdge.getEdges()) {
    			perimeter += edge.getTotalLength();
    		}
    	}
    	return perimeter;
    }
    
    public double getCompactness() {
    	double perimeter = 0;
    	double area = getArea();
    	for(ClusterEdge clusterEdge : border) {
    		for(Edge edge : clusterEdge.getEdges()) {
    			perimeter += edge.getTotalLength();
    		}
    	}
        return (4.0*Math.PI*area)/Math.pow(perimeter,2);
    }
    
	public double getSchwartzberg() {
		double r = Math.sqrt(getArea()/Math.PI);
		double c = 2 * Math.PI * r;
		double p = getPerimeter();
		return 1/(p/c);
	}
    
    public Map<DemographicType, Integer> getDemographics() {
    	Map<DemographicType, Integer> r = new HashMap<DemographicType, Integer>();
		EnumSet.allOf(DemographicType.class).forEach(demographicType->{
			r.put(demographicType, 0);
		});
    	for (Precinct precinct : precincts) {
    		Map<DemographicType, Integer> m = precinct.getDemographics();
    		EnumSet.allOf(DemographicType.class).forEach(demographicType->{
    			int value = 0;
    			if(m.containsKey(demographicType) && r.containsKey(demographicType)) {
        			value = r.get(demographicType) + m.get(demographicType);
    			}
    			r.put(demographicType, value);
    		});
    	}
    	return r;
    }
    
    public int getDemographic(DemographicType demographicType) {
    	if(id == 0) return 0;
    	int value = 0;
    	for (Precinct precinct : precincts) {
    		Map<DemographicType, Integer> m = precinct.getDemographics();
			if(m.containsKey(demographicType)) {
    			value += m.get(demographicType);
			}
    	}
    	return value;
    }
    
    public ArrayList<String> getCounties(){
    	ArrayList<String> counties = new ArrayList<String>();
		for (Precinct p : precincts) {
			String county = p.getCounty();
			if(!counties.contains(county)) {
				counties.add(county);
			}
		}
		return counties;
    }

	public double getPartyVote(PartyType party) {
		if(this.id == 0) return 0;
		double votes = 0;
		for(Precinct p : precincts) {
			votes += p.getPartyVote(party);
		}
		return votes;
	}
	
	public double getTotalVotes() {
		int votes = 0;
		votes += getPartyVote(PartyType.DEMOCRATIC);
		votes += getPartyVote(PartyType.REPUBLICAN);
		votes += getPartyVote(PartyType.OTHERPARTY);
		return votes;
	}
	
	public double getEfficiencyGap() {
		if(id == 0) {
			return 0.0;
		}
		double demVotes = getPartyVote(PartyType.DEMOCRATIC);
		double repVotes = getPartyVote(PartyType.REPUBLICAN);
		return Math.abs(demVotes-repVotes)/getTotalVotes();
	}

	public double getPVI() {
		if(id == 0) {
			return 0.0;
		}
		double demVotes = getPartyVote(PartyType.DEMOCRATIC);
		double repVotes = getPartyVote(PartyType.REPUBLICAN);
		double totalVotes = getTotalVotes();
		if(totalVotes == 0) {
			return 0.0;
		}
		return (demVotes-repVotes)/totalVotes;
	}


}
package com.bluejays.gerrymandering.model;

import java.util.HashMap;

public class DistrictData {
	private int districtId;
	private HashMap<String, Double> dataMap = new HashMap<>();
	
	public int getDistrictId() {
		return districtId;
	}
	
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	
	public HashMap<String, Double> getDataMap() {
		return dataMap;
	}
	
	public void setDataMap(HashMap<String, Double> dataMap) {
		this.dataMap = dataMap;
	}
}

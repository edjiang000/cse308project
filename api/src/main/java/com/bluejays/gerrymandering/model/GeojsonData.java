package com.bluejays.gerrymandering.model;

import java.net.URL;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "GeojsonData", uniqueConstraints = {
		@UniqueConstraint(columnNames = {
				"stateName"
		})
})
public class GeojsonData {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	
	@NaturalId
	@NotBlank
	@Size(max = 40)
	private String stateName;
    
    @Column( columnDefinition = "TEXT" )
    private String geojsonUrl;
    
    public GeojsonData() {
    	
    }
    
    public GeojsonData(int id, String stateName, String geojsonUrl) {
    	this.id = id;
    	this.stateName = stateName;
    	this.geojsonUrl = geojsonUrl;
    	
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getGeojson() {
		return geojsonUrl;
	}

	public void setGeojsonUrl(String geojsonUrl) {
		this.geojsonUrl = geojsonUrl;
	}
     
}

package com.bluejays.gerrymandering.model;

import com.bluejays.gerrymandering.enums.WeightType;

import java.util.Map;

import javax.persistence.Enumerated;

public class WeightSet {

	private long id;
	
	private String name;
	
	private Map<WeightType, Double> weights;
	
	private double total;
	
	public WeightSet() {}
	
	public WeightSet(long id, String name, Map<WeightType, Double> weights) {
		this.id = id;
		this.name = name;
		this.weights = weights;
		this.total = -1;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<WeightType, Double> getWeights() {
		return weights;
	}

	public void setWeights(Map<WeightType, Double> weights) {
		this.weights = weights;
	}
	
	public double getTotal () {
		if (total == -1) {
			total = 0;
			for (double weight : weights.values()) {
				total += weight;
			}
		}
		return total;
	}
	
	public double get (WeightType weightType) {
		return weights.get(weightType);
	}
	
}

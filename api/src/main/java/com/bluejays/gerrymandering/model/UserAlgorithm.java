package com.bluejays.gerrymandering.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.bluejays.gerrymandering.enums.AlgorithmState;

public class UserAlgorithm {
	private AlgorithmState state;
	private List<MovePrecinct> moves;
	private List<BatchResult> batchResults;
	private int moveProgress;
	private int batchMoveProgress;
	private Lock movesLock;
	
	public UserAlgorithm() {
		this.state = AlgorithmState.RUN;
		this.moves = Collections.synchronizedList(new ArrayList<MovePrecinct>());
		this.moveProgress = 0;
		this.movesLock = new ReentrantLock();
		this.batchResults = Collections.synchronizedList(new ArrayList<BatchResult>());
		this.batchMoveProgress = 0;
	}
	
	public void deactivate() {
		this.state = AlgorithmState.INACTIVE;
	}
	
	public void run() {
		this.state = AlgorithmState.RUN;
		this.moves = Collections.synchronizedList(new ArrayList<MovePrecinct>());
		this.moveProgress = 0;
	}
	
	public void pause() {
		this.state = AlgorithmState.PAUSE;
	}
	
	public void resume() {
		this.state = AlgorithmState.RUN;
	}
	
	public void runBatch() {
		this.state = AlgorithmState.BATCH;
		this.batchResults = Collections.synchronizedList(new ArrayList<BatchResult>());
		this.batchMoveProgress = 0;
	}
	
	public boolean isInactive() {
		return this.state == AlgorithmState.INACTIVE;
	}
	
	public boolean isRunning() {
		return this.state == AlgorithmState.RUN;
	}
	
	public boolean isPaused() {
		return this.state == AlgorithmState.PAUSE;
	}
	
	public boolean isRunningBatch() {
		return this.state == AlgorithmState.BATCH;
	}

	public List<MovePrecinct> getMoves() {
		return moves;
	}
	
	public int getMoveProgress() {
		return moveProgress;
	}
	
	public int increaseMoveProgress(int moveProgress) {
		this.moveProgress += moveProgress;
		return this.moveProgress;
	}
	
	public List<BatchResult> getBatchResults() {
		return batchResults;
	}
	
	public int getBatchMoveProgress() {
		return this.batchMoveProgress;
	}
	
	public int increaseBatchMoveProgress(int batchMoveProgress) {
		this.batchMoveProgress += batchMoveProgress;
		return this.batchMoveProgress;
	}

	public Lock getMovesLock() {
		return movesLock;
	}
}

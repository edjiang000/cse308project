package com.bluejays.gerrymandering.model;

import java.util.*;

import com.bluejays.gerrymandering.enums.PartyType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Map {
    private int id;
    private String name;
    private int stateId;
    private String stateName;
    private double objectiveFunction;
    private EnumMap<PartyType, Integer> politicalSeats;
    private int compactness;
    private WeightSet weightSet;
    private int userId;
}

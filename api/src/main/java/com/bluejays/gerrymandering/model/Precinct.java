package com.bluejays.gerrymandering.model;

import java.util.Map;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.PartyType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "precinct")
public class Precinct {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int dbId;
	private int id;
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "district_id")
	private District district;
	private String county;
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "state_id", nullable = false)
	private State state;
	@JsonIgnore
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    @JoinTable(
        name = "precinct_edge", 
        joinColumns = { @JoinColumn(name = "precinct_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "edge_id") }
    )
	private Collection<Edge> edges;
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "precinct_demo")
	@MapKeyEnumerated(EnumType.STRING)
	@MapKeyColumn(name = "demo_type")
	@Column(name = "value")
	private Map<DemographicType, Integer> demographics;	
	private double area;
	private int population;
	
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "precinct_party")
	@MapKeyEnumerated(EnumType.STRING)
	@MapKeyColumn(name = "party_type")
	@Column(name = "value")
	private Map<PartyType, Double> partyVotes;	

	public Precinct() {
		this.id = 0;
	}

    public Precinct(int id, District district, String county, Collection<Edge> edges,
                    Map<DemographicType, Integer> demographics, State state) {
        this.id = id;
        this.state = state;
        this.district = district;
        this.county = county;
        this.edges = edges;
        this.demographics = demographics;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public State getState () {
        return state;
    }

    public void setState (State state) {
        this.state = state;
    }

    public Collection<Edge> getEdges() {
        return Collections.unmodifiableCollection(edges);
    }

    public void addEdge(Edge edge) {
    	if (edges == null) {
    		edges = new HashSet<>();
    	}
    	edges.add(edge);
    }
    
    public void addAllEdges(Collection<Edge> edges) {
    	if (this.edges == null) {
    		this.edges = new HashSet<>();
    	}
    	this.edges.addAll(edges);
    }

    public Map<DemographicType, Integer> getDemographics() {
        return demographics;
    }

    public void setDemographics(Map<DemographicType, Integer> demographics) {
        this.demographics = demographics;
    }
    
    public int getDemopraphic(DemographicType demographicType) {
    	return demographics.get(demographicType);
    }
    
    public double getArea() {
    	return area;
    }
    
    public void setArea(double area) {
    	this.area = area;
    }

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}
	

	public Map<PartyType, Double> getPartyVotes() {
		return partyVotes;
	}

	public void setPartyVotes(Map<PartyType, Double> m) {
		this.partyVotes = m;
	}

	public double getPartyVote(PartyType party) {
		if(partyVotes.containsKey(party)) {
			return partyVotes.get(party);
		}
		return 0.0;
	}
}

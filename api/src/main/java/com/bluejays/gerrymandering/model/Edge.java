package com.bluejays.gerrymandering.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.geometry.GraphNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "edge")
public class Edge implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToMany(mappedBy = "edges", fetch = FetchType.LAZY)
	private List<Precinct> precincts;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_id", nullable = false)
    private State state;
    private double joinability;
    private double countyJoinability;
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "demo_joinability")
    @MapKeyEnumerated(EnumType.STRING)
    @MapKeyColumn(name = "demo_type")
    @Column(name = "joinability")
    private Map<DemographicType, Double> demographicJoinability;
    private double totalLength;
    @Transient
    private List<GraphNode> starts;
    @Transient
    private List<Integer> pointLength;
    @Transient
    private List<Double> length;
    @Transient
    private List<GraphNode> ends;

    public Edge () {
        this(new Precinct(), new Precinct());
    }

    public Edge (Precinct precinct1, Precinct precinct2) {
    	this.precincts = new ArrayList<>();
    	precincts.add(precinct1);
    	precincts.add(precinct2);
//        this.precinct1 = precinct1;
//        this.precinct2 = precinct2;
        starts = new ArrayList<>();
        pointLength = new ArrayList<>();
        length = new ArrayList<>();
        ends = new ArrayList<>();
        totalLength = 0;
    }

    public void appendStarts (GraphNode start) {
        starts.add(start);
    }

    public void appendPointLength (int len) {
        pointLength.add(len);
    }

    public void appendLength (double distance) {
        length.add(distance);
    }

    public void appendEnds (GraphNode end) {
        ends.add(end);
    }

    public void increaseTotalLength (double distance) {
        totalLength += distance;
    }

    public void lengthGrow (double distance) {
        length.set(length.size()-1, length.get(length.size()-1)+distance);
    }

    public void pointLengthGrow (int len) {
        pointLength.set(pointLength.size()-1, pointLength.get(pointLength.size()-1)+len);
    }

    public Precinct getPrecinct1 () {
        return precincts.get(0);
    }

    public Precinct getPrecinct2 () {
        return precincts.get(1);
    }

    public double getTotalLength() {
        return totalLength;
    }

    @Override
    public boolean equals (Object o) {
        if (o instanceof Edge) {
            Edge e = (Edge) o;
            if (e.getPrecinct1().getId() == this.getPrecinct1().getId() && e.getPrecinct2().getId() == this.getPrecinct2().getId()) return true;
        }
        return false;
    }

    private int updateJoinability() {
        return 0;
    }

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	public Precinct getOtherPrecinct(Precinct precinct) {
		return precinct.getId() == this.getPrecinct1().getId() ? this.getPrecinct1() : this.getPrecinct2();
	}
	
	public Precinct getConnection (Precinct precinct) {
        if (getPrecinct1() == precinct)
            return getPrecinct2();
        if (getPrecinct2() == precinct)
            return getPrecinct1();
        return null;
    }
	
	public String toString() {
		return "[" + getPrecinct1().getId() + "," + getPrecinct2().getId() + "]";
	}
}

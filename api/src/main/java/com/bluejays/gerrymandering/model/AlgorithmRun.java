package com.bluejays.gerrymandering.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.util.Pair;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.WeightType;

public class AlgorithmRun {
	private String stateName;
	private Integer numberOfDistricts;
	private Map<String, Double> weights;
	private WeightSet weightSet;
	private List<Double> majorityMinorityConstraints;
	
	public AlgorithmRun() {}

	public AlgorithmRun(String stateName, WeightSet weightSet,
			List<Double> majorityMinorityConstraints) {
		this.stateName = stateName;
		this.weightSet = weightSet;
		this.majorityMinorityConstraints = majorityMinorityConstraints;
	}
	
	public void setWeightSetFromWeights() {
		this.weightSet = new WeightSet();
		this.weightSet.setWeights(new HashMap<>());
		for (String weightName : weights.keySet()) {
			WeightType newWeightType = WeightType.valueOf(weightName);
			Double newWeightValue = weights.get(weightName);
			this.weightSet.getWeights().put(newWeightType, newWeightValue);
		}
		System.out.println("Weight set added");
	}

	
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public WeightSet getWeightSet() {
		return weightSet;
	}

	public void setWeightSet(WeightSet weightSet) {
		this.weightSet = weightSet;
	}

	public List<Double> getMajorityMinorityConstraints() {
		return majorityMinorityConstraints;
	}

	public void setMajorityMinorityConstraints(
			List<Double> majorityMinorityConstraints) {
		this.majorityMinorityConstraints = majorityMinorityConstraints;
	}

	public Integer getNumberOfDistricts() {
		return numberOfDistricts;
	}

	public void setNumberOfDistricts(Integer numberOfDistricts) {
		this.numberOfDistricts = numberOfDistricts;
	}

	public Map<String, Double> getWeights() {
		return weights;
	}

	public void setWeights(Map<String, Double> weights) {
		this.weights = weights;
	}
}

package com.bluejays.gerrymandering.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.util.Pair;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.PartyType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "state")
public class State {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private int numDistricts;
	private int numMajMin;
	private boolean isDefaultState;
	@Transient
	@JsonIgnore
	private Collection<Cluster> clusters;
	@Transient
	@JsonIgnore
	private PriorityQueue<ClusterEdge> clusterEdges;
	@JsonIgnore
	@OneToMany(mappedBy = "state", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Collection<Edge> edges;
	@Transient
	@JsonIgnore
	private List<Precinct> movablePrecincts;
	@Transient
	@JsonIgnore
	private Collection<District> majMinDistrict;
	@Transient
	@JsonIgnore
	private Collection<Cluster> majMinCluster;
	@JsonIgnore
	@OneToMany(mappedBy = "state", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Collection<Precinct> precincts;
	private double maxMajMin;
	private double minMajMin;
	@JsonManagedReference
	@OneToMany(mappedBy = "state", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Collection<District> districts;
	@Transient
//	@OneToMany(mappedBy = "state", cascade = CascadeType.ALL)
	private List<MovePrecinct> moves;
	@Transient
	private int population = 0;
	@Transient
	private double totalPVI = 0;
	
	public State() {}
	
	public State(int id, String name, Boolean isDefaultState) {
		this.id = id;
		this.name = name;
		this.isDefaultState = isDefaultState;
	}
	
	public State(int id, String name, Boolean isDefaultState, Collection<Edge> edges,
			Collection<Precinct> precincts, Collection<District> districts) {
		this.id = id;
		this.name = name;
		this.isDefaultState = isDefaultState;
		this.edges = edges;
		this.precincts = precincts;
		this.districts = districts;
	}
	
	public State(int id, String name, List<MovePrecinct> moves) {
		this(id, name, false);
		
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getNumMajMin() {
		return numMajMin;
	}

	public void setNumMajMin(int numMajMin) {
		this.numMajMin = numMajMin;
	}

	
	public Collection<Cluster> getMajMinCluster() {
		return majMinCluster;
	}

	public void setMajMinCluster(Collection<Cluster> majMinCluster) {
		this.majMinCluster = new HashSet<>();
		this.majMinCluster.addAll(majMinCluster);
	}

	public Collection<District> getMajMinDistrict() {
		return majMinDistrict;
	}

	public void setMajMinDistrict(Collection<District> majMinDistrict) {
		this.majMinDistrict = new HashSet<>();
		this.majMinDistrict.addAll(majMinDistrict);
	}

	public double getMaxMajMin() {
		return maxMajMin;
	}

	public void setMaxMajMin(double maxMajMin) {
		this.maxMajMin = maxMajMin;
	}

	public double getMinMajMin() {
		return minMajMin;
	}

	public void setMinMajMin(double minMajMin) {
		this.minMajMin = minMajMin;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Collection<Edge> getEdges() {
        return edges;
    }

    public Collection<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Collection<District> districts) {
        this.districts = new HashSet<>();
        this.districts.addAll(districts);
    }

    public void initNeighbors() {
        return;
    }

    public void initPrecincts() {
        return;
    }

    public double getObjectiveFunction() {
        return 0.0;
    }

	@JsonIgnore
    public Precinct getCandidatePrecinct() {
        return new Precinct();
    }

    public void updateBorders() {
        return;
    }

	@JsonIgnore
    public Collection<Cluster> getClusters() {
        return clusters;
    }

    public void setClusters(Collection<Cluster> clusters) {
        this.clusters = new HashSet<>();
        this.clusters.addAll(clusters);
    }
    
    public void removeCluster(Cluster c) {
    	this.clusters.remove(c);
    }

	@JsonIgnore
    public Collection<Precinct> getPrecincts () {
        return precincts;
    }

    public void setPrecincts(Collection<Precinct> precincts) {
        this.precincts = new HashSet<>();
        this.precincts.addAll(precincts);
    }

    public void setEdges(Collection<Edge> edges) {
        this.edges = new HashSet<>();
        this.edges.addAll(edges);
    }

	@JsonIgnore
    public PriorityQueue<ClusterEdge> getClusterEdges() {
        return clusterEdges;
    }

	@JsonIgnore
    public void setClusterEdges(Collection<ClusterEdge> clusterEdges) {
        this.clusterEdges = new PriorityQueue<>();
        this.clusterEdges.addAll(clusterEdges);
    }

	@JsonIgnore
    public ClusterEdge getBestClusterEdge() {
        return clusterEdges.peek();
    }

    public boolean isDefaultState() {
        return isDefaultState;
    }

    public void setIsDefaultState(boolean isDefaultState) {
        this.isDefaultState = isDefaultState;
    }

    public int getNumDistricts() {
        return numDistricts;
    }

    public void setNumDistricts(int numDistricts) {
        this.numDistricts = numDistricts;
    }

    public List<Precinct> getMovablePrecincts() {
        return movablePrecincts;
    }

    public List<MovePrecinct> getMoves() {
		return moves;
	}

	public void setMoves(List<MovePrecinct> moves) {
		this.moves = moves;
	}

	public void setMovablePrecincts(Collection<Precinct> movablePrecincts) {
    	this.movablePrecincts = new ArrayList<>();
        this.movablePrecincts.addAll(movablePrecincts);
    }

	public void setMajorityMinorityConstraints(
			List<Map<DemographicType, Pair<Double, Double>>> majorityMinorityConstraints) {
		
		
	}
	
	public int getPopulation() {
		if(population != 0) {
			return population;
		}
		for(Precinct p : precincts) {
			population += p.getPopulation();
		}
		return population;
	}

	public double getAvgPVI() {
		if (totalPVI != 0) {
			return totalPVI;
		}
		double totalVotes = 0;
		for (Cluster c : clusters) {
			if(c!=null) {
				totalPVI += c.getPartyVote(PartyType.DEMOCRATIC);
				totalPVI -= c.getPartyVote(PartyType.REPUBLICAN);
				totalVotes += c.getTotalVotes();
			}
		}
		totalPVI = totalPVI/totalVotes;
		return totalPVI;
	}
	
	public void reloadDistrict() {
		for (District district : districts) {
			Collection<Precinct> precincts = district.getPrecincts();
			district.setPrecinctsSafe(precincts);
		}
	}
}

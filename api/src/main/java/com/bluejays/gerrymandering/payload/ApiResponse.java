package com.bluejays.gerrymandering.payload;

public class ApiResponse {
	private String message;
	
	public ApiResponse() {}
	
	public ApiResponse(String message) {
		this.message = message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return this.message;
	}
}

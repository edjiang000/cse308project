package com.bluejays.gerrymandering.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluejays.gerrymandering.geometry.JsonTool;
import com.bluejays.gerrymandering.model.GeojsonData;
import com.bluejays.gerrymandering.model.State;
import com.bluejays.gerrymandering.payload.ApiResponse;
import com.bluejays.gerrymandering.repository.GeojsonDataRepository;

@RestController
@RequestMapping("/geojson")
public class GeojsonDataController {

	private GeojsonDataRepository geojsonDataRepository;
	private JsonTool jsonTool;
	
	@Autowired
	public GeojsonDataController(GeojsonDataRepository geojsonDataRepository, JsonTool jsonTool) {
		this.geojsonDataRepository = geojsonDataRepository;
		this.jsonTool = jsonTool;
	}
	
	@GetMapping("/{stateName}")
	public GeojsonData getGeojsonData(@PathVariable String stateName) {
		return this.geojsonDataRepository.findByStateName(stateName)
				.map(geojsonData -> {
					return new GeojsonData(geojsonData.getId(), geojsonData.getStateName(), geojsonData.getGeojson());
				}).orElseThrow(() -> new RuntimeException());
	}
	
	@PostMapping("")
	public ResponseEntity<ApiResponse> overrideGeojsonData(@RequestBody GeojsonData geojsonData) {
		this.geojsonDataRepository.save(geojsonData);
		return new ResponseEntity<ApiResponse>(new ApiResponse("Geojson Saved"), HttpStatus.OK);
	}
	
	@PostMapping("/upload")
	public void dumpGeojsonData() throws IOException {
		State state = new State();
		System.out.println("hit");
		jsonTool.preprocess(state, "C:\\Users\\Nick\\Documents\\cse308project\\data\\maine\\maine.geojson");
	}
}

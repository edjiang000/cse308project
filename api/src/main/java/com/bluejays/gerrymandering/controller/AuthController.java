package com.bluejays.gerrymandering.controller;

import java.util.Collections;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluejays.gerrymandering.model.Role;
import com.bluejays.gerrymandering.model.Role.RoleName;
import com.bluejays.gerrymandering.model.User;
import com.bluejays.gerrymandering.payload.ApiResponse;
import com.bluejays.gerrymandering.payload.LoginRequest;
import com.bluejays.gerrymandering.payload.RegisterRequest;
import com.bluejays.gerrymandering.repository.RoleRepository;
import com.bluejays.gerrymandering.repository.UserRepository;
import com.bluejays.gerrymandering.security.JwtTokenProvider;

@RestController
@RequestMapping("/auth")
public class AuthController {

	AuthenticationManager authenticationManager;
	UserRepository userRepository;
	RoleRepository roleRepository;
	PasswordEncoder passwordEncoder;
	JwtTokenProvider tokenProvider;
	
	class TokenResponse {
		private String accessToken;
		private String tokenType;
		
		public TokenResponse() {}
		
		public TokenResponse(String accessToken, String tokenType) {
			this.accessToken = accessToken;
			this.tokenType = tokenType;
		}
		
		public void setAccessToken(String accessToken) {
			this.accessToken = accessToken;
		}
		
		public String getAccessToken() {
			return this.accessToken;
		}

		public String getTokenType() {
			return tokenType;
		}

		public void setTokenType(String tokenType) {
			this.tokenType = tokenType;
		}
	}
	
	@Autowired
	public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository,
			RoleRepository roleRepository, PasswordEncoder passwordEncoder, JwtTokenProvider tokenProvider) {
		this.authenticationManager = authenticationManager;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.passwordEncoder = passwordEncoder;
		this.tokenProvider = tokenProvider;
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
			new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword())
		);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = tokenProvider.generateToken(authentication);
		return ResponseEntity.ok(new TokenResponse(jwt, "Bearer"));
	}
	
	@PostMapping("/register")
	public ResponseEntity<ApiResponse> registerUser(@Valid @RequestBody RegisterRequest registerRequest) {
		if (userRepository.existsByUsername(registerRequest.getEmail())) {
			return new ResponseEntity<ApiResponse>(new ApiResponse("Username already taken!"), HttpStatus.BAD_REQUEST);
		}
		User user = new User(registerRequest.getName(), registerRequest.getEmail(),
				registerRequest.getPassword());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new RuntimeException("User role not available"));
		user.setRoles(Collections.singleton(userRole));
		userRepository.save(user);
		return ResponseEntity.ok(new ApiResponse("User successfully registered"));
	}
}

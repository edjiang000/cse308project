package com.bluejays.gerrymandering.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bluejays.gerrymandering.model.User;
import com.bluejays.gerrymandering.payload.ApiResponse;
import com.bluejays.gerrymandering.repository.UserRepository;
import com.bluejays.gerrymandering.security.CurrentUser;
import com.bluejays.gerrymandering.security.IsAdmin;
import com.bluejays.gerrymandering.security.IsUser;
import com.bluejays.gerrymandering.security.UserPrincipal;

@RestController
@RequestMapping("/user")
public class UserController {

	private UserRepository userRepository;
	
	@Autowired
	public UserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@GetMapping("/me")
	@IsUser
	public User me(@CurrentUser UserPrincipal currentUser) {
		return this.userRepository.findById(currentUser.getId())
			.map(user -> {
				return new User(user.getId(), user.getName(), user.getUsername());
			}).orElseThrow(() -> new RuntimeException());
	}
	
	@GetMapping("")
	@IsAdmin
	public List<User> getAllUsers(@CurrentUser UserPrincipal currentUser, @RequestParam(required = false) Long userId) {
		return userId == null ? this.userRepository.findAll() :
			this.userRepository.findAll().stream().filter(user -> user.getId() == currentUser.getId())
				.collect(Collectors.toList());
	}
	
	@PutMapping("")
	@IsUser
	@PreAuthorize("#user.id == authentication.principal.id or hasRole('ADMIN')")
	public User updateUser(@CurrentUser UserPrincipal currentUser, @RequestBody User user) {
		return this.userRepository.save(user);
	}
	
	@DeleteMapping("/{id}")
	@IsAdmin
	public ResponseEntity<ApiResponse> deleteUser(@PathVariable Long id) {
		this.userRepository.deleteById(id);
		return new ResponseEntity<ApiResponse>(new ApiResponse("User has been deleted"), HttpStatus.OK);
	}
}

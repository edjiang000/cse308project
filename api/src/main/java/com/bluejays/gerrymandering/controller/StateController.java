package com.bluejays.gerrymandering.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluejays.gerrymandering.model.MovePrecinct;
import com.bluejays.gerrymandering.model.State;
import com.bluejays.gerrymandering.repository.StateRepository;

@RestController
@RequestMapping("/state")
public class StateController {

	private StateRepository stateRepository;
	
	@Autowired
	public StateController(StateRepository stateRepository) {
		this.stateRepository = stateRepository;
	}
	
	@GetMapping("/{id}")
	public State getState(@PathVariable int id) {
		return this.stateRepository.findById(id)
				.map(state -> state).orElseThrow(() -> new RuntimeException());
	}
	
	@PostMapping("/save")
	public void saveMap(@RequestBody List<MovePrecinct> moves) {
		State state = new State();
		state.setMoves(moves);
		this.stateRepository.save(state);
	}
}

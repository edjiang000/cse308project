package com.bluejays.gerrymandering.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluejays.gerrymandering.model.Precinct;
import com.bluejays.gerrymandering.payload.ApiResponse;
import com.bluejays.gerrymandering.repository.PrecinctRepository;

@RestController
@RequestMapping("/precinct")
public class PrecinctController {

	private PrecinctRepository precinctRepository;
	
	@Autowired
	public PrecinctController(PrecinctRepository precinctRepository) {
		this.precinctRepository = precinctRepository;
	}
	
	@GetMapping("/{id}")
	public Precinct getPrecinct(@PathVariable int id) {
		return this.precinctRepository.findById(id)
				.map(precinct -> {
					return new Precinct(precinct.getId(), precinct.getDistrict(), precinct.getCounty(), precinct.getEdges(), precinct.getDemographics(), precinct.getState());
				}).orElseThrow(() -> new RuntimeException());
	}
}

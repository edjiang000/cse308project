package com.bluejays.gerrymandering.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluejays.gerrymandering.model.AlgorithmRun;
import com.bluejays.gerrymandering.model.MovePrecinct;
import com.bluejays.gerrymandering.payload.ApiResponse;
import com.bluejays.gerrymandering.security.CurrentUser;
import com.bluejays.gerrymandering.security.IsUser;
import com.bluejays.gerrymandering.security.UserPrincipal;
import com.bluejays.gerrymandering.service.AlgorithmService;

@RestController
@RequestMapping("/algorithm")
public class AlgorithmController {
	
	private static final Logger logger = LoggerFactory.getLogger(AlgorithmController.class);

	private AlgorithmService service;

	@Autowired
	public AlgorithmController(AlgorithmService service) {
		this.service = service;
	}
	
	@GetMapping("/updates")
	@IsUser
	public List<MovePrecinct> pullMoveUpdates(@CurrentUser UserPrincipal user) {
		return this.service.pullMoveUpdates(user.getId());
	}
	
	@PostMapping("/start")
	@IsUser
	public ResponseEntity<ApiResponse> startAlgorithm(@RequestBody AlgorithmRun algorithmRun, @CurrentUser UserPrincipal user) {
		algorithmRun.setWeightSetFromWeights();
		return this.service.startAlgorithm(algorithmRun, user.getId()) ? 
				ResponseEntity.ok(new ApiResponse("Algorithm finished for: " + user.getUsername()))
				: new ResponseEntity<ApiResponse>(new ApiResponse("Algorithm failed to start for: " + user.getUsername()), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("/resume")
	@IsUser
	public ResponseEntity<ApiResponse> resumeAlgorithm(@CurrentUser UserPrincipal user) {
		service.resumeAlgorithm(user.getId());
		return ResponseEntity.ok(new ApiResponse("Algorithm resumed for: " + user.getUsername()));
	}
	
	@PostMapping("/end")
	@IsUser
	public ResponseEntity<ApiResponse> endAlgorithm(@CurrentUser UserPrincipal user) {
		this.service.endAlgorithm(user.getId());
		return ResponseEntity.ok(new ApiResponse("Algorithm stopped for: " + user.getUsername()));
	}
}

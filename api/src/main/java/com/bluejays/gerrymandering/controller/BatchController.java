package com.bluejays.gerrymandering.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluejays.gerrymandering.model.BatchResult;
import com.bluejays.gerrymandering.model.BatchRun;
import com.bluejays.gerrymandering.payload.ApiResponse;
import com.bluejays.gerrymandering.security.CurrentUser;
import com.bluejays.gerrymandering.security.IsUser;
import com.bluejays.gerrymandering.security.UserPrincipal;
import com.bluejays.gerrymandering.service.AlgorithmService;

@RestController
@RequestMapping("/batch")
public class BatchController {
	private AlgorithmService algorithmService;
	
	@Autowired
	public BatchController(AlgorithmService algorithmService) {
		this.algorithmService = algorithmService;
	}

	@GetMapping("/{batchId}")
	@IsUser
	public BatchResult loadBatchResult(@PathVariable Integer batchId, @CurrentUser UserPrincipal currentUser) {
		return algorithmService.loadBatchResult(batchId, currentUser.getId());
	}
	
	@GetMapping("")
	@IsUser
	public List<BatchResult> pullBatchUpdates(@CurrentUser UserPrincipal currentUser) {
		return new ArrayList<>();
	}
	
	@PostMapping("")
	@IsUser
	public ResponseEntity<ApiResponse> startBatchRun(@RequestBody BatchRun batchRun, @CurrentUser UserPrincipal currentUser) {
		batchRun.getAlgorithmRuns().forEach(algorithmRun -> algorithmRun.setWeightSetFromWeights());
		return this.algorithmService.startBatchRun(batchRun, currentUser.getId()) ? 
				ResponseEntity.ok(new ApiResponse("Batch algorithm finished for: " + currentUser.getUsername()))
				: new ResponseEntity<ApiResponse>(new ApiResponse("Batch algorithm failed to start for: " + currentUser.getUsername()), HttpStatus.BAD_REQUEST);
	}
}

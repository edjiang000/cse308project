package com.bluejays.gerrymandering.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.lang.Object;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.PartyType;
import com.bluejays.gerrymandering.enums.WeightType;
import com.bluejays.gerrymandering.enums.WeightType.*;
import com.bluejays.gerrymandering.model.Cluster;
import com.bluejays.gerrymandering.model.ClusterEdge;
import com.bluejays.gerrymandering.model.District;
import com.bluejays.gerrymandering.model.DistrictData;
import com.bluejays.gerrymandering.model.MoveData;
import com.bluejays.gerrymandering.model.Precinct;
import com.bluejays.gerrymandering.model.State;
import com.bluejays.gerrymandering.model.WeightSet;

@Service
public class FunctionCalculator {
	
	private State state;
	
	private WeightSet weightSet;
	
	private boolean initialized;
	
	@Autowired
	public FunctionCalculator() {
		initialized = false;
	}
	
	public void init (State state, WeightSet weightSet) {
		this.state = state;
		this.weightSet = weightSet;
		this.initialized = true;
	}
	
	public double computeJoinability (ClusterEdge edge) {
		if (!initialized) return -1;
		if (!majMinSatisfied(edge))
			return -0.5;
		double total = 0;
		double res = 0;
		double value = 0;
		double score = 0;
		
		value = weightSet.get(WeightType.POPULATION_EQUALITY_WEIGHT);
		if (value != 0) {
			score = calculatePopulationJoinability(edge);
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.COMPACTNESS_POLSBY_POPPER_WEIGHT);
		if (value != 0) {
			score = calculateCompactnessJoinability(edge);
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.COMPACTNESS_SCHWARTZBERG_WEIGHT);
		if (value != 0) {
			score = calculateCompactnessSchwartzbergJoinability(edge);
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.COUNTY_WEIGHT);
		if (value != 0) {
			score = calculateCountyJoinability(edge);
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.RACIAL_EQUALITY_WEIGHT);
		if (value != 0) {
			score = calculateRacialJoinability(edge);
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.HISPANIC_EQUALITY_WEIGHT);
		if (value != 0) {
			score = calculateHispanicJoinability(edge);
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.POLITICAL_FAIRNESS_PVI_WEIGHT);
		if (value != 0) {
			score = calculatePVIJoinability(edge);
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.POLITICAL_FAIRNESS_EFFICIENCY_GAP_WEIGHT);
		if (value != 0) {
			score = calculateEfficiencyGapJoinability(edge);
			total += value;
			res += (value * score);
		}
		return res / total;
	}
	
	private boolean majMinSatisfied(ClusterEdge clusterEdge) {
		double min = state.getMinMajMin();
		double max = state.getMaxMajMin();
		Cluster c1 = clusterEdge.getCluster1();
		int c1AM = c1.getDemographic(DemographicType.AFRICAN_AMERICAN);
		double c1pop = c1.getPopulation();
		double c1ratio = c1AM / c1pop;
		boolean c1IsMajMin =  c1ratio >= min && c1ratio <= max;
		Cluster c2 = clusterEdge.getCluster2();
		int c2AM = c2.getDemographic(DemographicType.AFRICAN_AMERICAN);
		double c2pop = c2.getPopulation();
		double c2ratio = c2AM / c2pop;
		boolean c2IsMajMin =  c2ratio >= min && c2ratio <= max;
		if (c1IsMajMin && c2IsMajMin) {
			return state.getMajMinCluster().size() > state.getNumMajMin();
		} else if (c1IsMajMin || c2IsMajMin) {
			if (state.getMajMinCluster().size() > state.getNumMajMin())
				return true;
			return (c1AM + c2AM) / (c1pop + c2pop) >= min;
		} else {
			return true;
		}
	}
	
	public double calculatePopulationJoinability(ClusterEdge clusterEdge) {
		int pop1 = clusterEdge.getCluster1().getPopulation();
		int pop2 = clusterEdge.getCluster2().getPopulation();
		int totalPop = state.getPopulation();
		int idealPop = totalPop / state.getNumDistricts();
		double popDiff = (idealPop - (pop1 + pop2)) / (double)idealPop;
		double mean = 0.0;
		double stddev = 0.4;
		double yOffset = 0.3;
		double joinability;
		joinability = popDiff/2 + 0.5;
		if(joinability > 1.0) joinability = 1.0;
		else if(joinability < 0.0) joinability = 0.0;
		//System.out.println(totalPop);
		//if(pop1 + pop2 > idealPop) {
		//	System.out.println(pop2);
		//	joinability = (1/(stddev * Math.sqrt(2*Math.PI))) * Math.pow(Math.E, (-Math.pow(popDiff - mean,2)/(2*Math.pow(stddev, 2))));
		//}
		//else {
		//	joinability = 1 - yOffset + popDiff;
		//}
		return joinability;
	}	
	
	public double calculateCompactnessJoinability(ClusterEdge clusterEdge) {
		double comp1 = clusterEdge.getCluster1().getCompactness();
		double comp2 = clusterEdge.getCluster2().getCompactness();
		double avgComp = (comp1 + comp2) / 2;
		
		Cluster c1 = clusterEdge.getCluster1();
		Cluster c2 = clusterEdge.getCluster2();
		
		double perimeter = 0;
		double area = c1.getArea() + c2.getArea();
		if (c1 != null && c2 != null) {
            Collection<ClusterEdge> c1Borders = c1.getBorder();
            Collection<ClusterEdge> c2Borders = c2.getBorder();
            for (ClusterEdge c2Edge : c2Borders) {
                Cluster other = c2Edge.getConnection(c2);
                if (other != c1)
                    perimeter += c2Edge.getEdgeLength();
            }
            for (ClusterEdge c1Edge : c1Borders) {
                Cluster other = c1Edge.getConnection(c1);
                if (other != c2)
                    perimeter += c1Edge.getEdgeLength();
            }
        }
		double newComp = (4.0*Math.PI*area)/Math.pow(perimeter,2);
		double joinability;
		joinability = (newComp - avgComp);
		//System.out.println("Joinability: " + joinability);
		if(joinability > 1.0) joinability = 1.0;
		else if(joinability < 0.0) joinability = 0.0;
		return joinability;
	}	
	
	public double calculateCompactnessSchwartzbergJoinability(ClusterEdge clusterEdge) {
		double comp1 = clusterEdge.getCluster1().getSchwartzberg();
		double comp2 = clusterEdge.getCluster2().getSchwartzberg();
		double avgComp = (comp1 + comp2) / 2;

		Cluster c1 = clusterEdge.getCluster1();
		Cluster c2 = clusterEdge.getCluster2();
		
		double perimeter = 0;
		double area = c1.getArea() + c2.getArea();
		if (c1 != null && c2 != null) {
            Collection<ClusterEdge> c1Borders = c1.getBorder();
            Collection<ClusterEdge> c2Borders = c2.getBorder();
            for (ClusterEdge c2Edge : c2Borders) {
                Cluster other = c2Edge.getConnection(c2);
                if (other != c1)
                    perimeter += c2Edge.getEdgeLength();
            }
            for (ClusterEdge c1Edge : c1Borders) {
                Cluster other = c1Edge.getConnection(c1);
                if (other != c2)
                    perimeter += c1Edge.getEdgeLength();
            }
        }
		double r = Math.sqrt(area/Math.PI);
		double c = 2 * Math.PI * r;
		double newComp = 1/(c/perimeter);
		double joinability;
		joinability = (newComp - avgComp);
		if(joinability > 1.0) joinability = 1.0;
		else if(joinability < 0.0) joinability = 0.0;
		return joinability;
	}
	
	public double calculateCountyJoinability(ClusterEdge clusterEdge) {
		Cluster c1 = clusterEdge.getCluster1();
		Cluster c2 = clusterEdge.getCluster2();
		ArrayList<String> counties1 = c1.getCounties();
		ArrayList<String> counties2 = c2.getCounties();

		for(String county : counties2) {
			if(counties1.contains(county)) {
				return 1.0;
			}
		}
		return 0.0;
	}
	
	public double computeDistance(Integer[] array1, Integer[] array2)
    {
        double Sum = 0.0;
        for(int i=0;i<array1.length;i++) {
           Sum = Sum + Math.pow((array1[i]-array2[i]),2.0);
        }
        return Math.sqrt(Sum);
    }
	
	public double calculateRacialJoinability(ClusterEdge clusterEdge) {
		double joinability = 0.0;
		Map<DemographicType, Integer> demo1 = clusterEdge.getCluster1().getDemographics();
		Map<DemographicType, Integer> demo2 = clusterEdge.getCluster2().getDemographics();
		int pop1 = clusterEdge.getCluster1().getPopulation();
		int pop2 = clusterEdge.getCluster2().getPopulation();
		if(pop1 == 0 || pop2 == 0) {
			joinability = 0.5;
			return joinability;
		}
		
		Object[] objArray1 = demo1.values().toArray();
		Object[] objArray2 = demo2.values().toArray();
		Integer val1[] = Arrays.stream(objArray1)
				.map(Object::toString)
				.map(Integer::valueOf)
				.toArray(Integer[]::new);
		Integer val2[] = Arrays.stream(objArray2)
				.map(Object::toString)
				.map(Integer::valueOf)
				.toArray(Integer[]::new);

		double distance = computeDistance(val1, val2);
		joinability = 1 - distance/(pop1+pop2);
		//System.out.println(joinability);
		return joinability;
	}
	
	public double calculateHispanicJoinability(ClusterEdge clusterEdge) {
		double joinability = 0.0;
		int demo1 = clusterEdge.getCluster1().getDemographic(DemographicType.HISPANIC);
		int demo2 = clusterEdge.getCluster2().getDemographic(DemographicType.HISPANIC);
		int pop1 = clusterEdge.getCluster1().getPopulation();
		int pop2 = clusterEdge.getCluster2().getPopulation();
		
		if(pop1 == 0 || pop2 == 0) {
			joinability = 0.5;
			return joinability;
		}
		
		double demo1Percentage = demo1/(pop1*1.0);
		double demo2Percentage = demo2/(pop2*1.0);
		double demoDiff = Math.abs(demo2Percentage - demo1Percentage);
		joinability = demoDiff*100;
		return joinability;	
	}
	
	public double calculatePVIJoinability(ClusterEdge clusterEdge) {
		double joinability = 0.0;
		double idealPVI = state.getAvgPVI();
		double pvi1 = clusterEdge.getCluster1().getPVI();
		double pvi2 = clusterEdge.getCluster2().getPVI();
		double pviDiff = (pvi1 + pvi2);
		//System.out.println(idealPVI);			
		double mean = idealPVI;
		double stddev = 0.4;
		double yOffset = 0.3;
		joinability = -(1/(stddev * Math.sqrt(2*Math.PI))) * Math.pow(Math.E, (-Math.pow(pviDiff - mean,2)/(2*Math.pow(stddev, 2))));
		joinability += 1;
		//System.out.println(joinability);
		if(joinability > 1.0) joinability = 1.0;
		else if(joinability < 0.0) joinability = 0.0;
		return joinability;
	}
	
	public double calculateEfficiencyGapJoinability(ClusterEdge clusterEdge) {
		double joinability = 0.0;
		double demVotes1 = clusterEdge.getCluster1().getPartyVote(PartyType.DEMOCRATIC);
		double repVotes1 = clusterEdge.getCluster1().getPartyVote(PartyType.REPUBLICAN);
		double demVotes2 = clusterEdge.getCluster1().getPartyVote(PartyType.DEMOCRATIC);
		double repVotes2 = clusterEdge.getCluster1().getPartyVote(PartyType.REPUBLICAN);
		double demVotes = demVotes1 + demVotes2;
		double repVotes = repVotes1 + repVotes2;
		double totalVotes = clusterEdge.getCluster1().getTotalVotes() + clusterEdge.getCluster2().getTotalVotes();
		
		double c1Gap = clusterEdge.getCluster1().getEfficiencyGap();
		double c2Gap = clusterEdge.getCluster2().getEfficiencyGap();
		double avgGap = (c1Gap+c2Gap)/2;

		double efficiencyGap;
		efficiencyGap = Math.abs(demVotes - repVotes)/totalVotes;
		
		double idealGap = 0.1;
		if(efficiencyGap<idealGap) {
			joinability = 0.5;
		}
		else {
			joinability = 1 - Math.abs(efficiencyGap - avgGap);
		}
		if(joinability > 1.0) joinability = 1.0;
		else if(joinability < 0.0) joinability = 0.0;
		return joinability;
	}

	public double computeObjectiveFunction () {
		if (!majMinSatisfied()) return -1;
		
		double res = 0;
		double total = 0;
		double value = 0;
		double score = 0;
		
		value = weightSet.get(WeightType.POPULATION_EQUALITY_WEIGHT);
		if (value != 0) {
			score = calculatePopulationObjectiveFunction();
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.COMPACTNESS_POLSBY_POPPER_WEIGHT);
		if (value != 0) {
			score = calculateCompactnessObjectiveFunction();
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.POLITICAL_FAIRNESS_EFFICIENCY_GAP_WEIGHT);
		if (value != 0) {
			score = calculateEfficiencyGapObjectiveFunction();
			total += value;
			res += (value * score);
		}
		value = weightSet.get(WeightType.POLITICAL_FAIRNESS_PVI_WEIGHT);
		if (value != 0) {
			score = calculatePVIObjectiveFunction();
			total += value;
			res += (value * score);
		}

		return res / total;
	}
	
	private boolean majMinSatisfied() {
		int count = 0;
		for (District district : state.getDistricts()) {
			if (district.isMajMinDistrict()) count++;
		}
		return count >= state.getNumMajMin();
	}

	private double calculateEfficiencyGapObjectiveFunction() {
		double totalEffGap = 0;
		for (District district : state.getDistricts()) {
			double effGap = district.getEfficiencyGap();
			if (effGap > 1) effGap = 1;
			totalEffGap += effGap;
		}
		double avgEffGap = totalEffGap / state.getNumDistricts();
		return 1 - avgEffGap;
	}

	private double calculatePVIObjectiveFunction() {
		double totalPVI = 0;
		for (District district : state.getDistricts()) {
			totalPVI += district.getPVI();
		}
		return totalPVI >= 0 ? 1 - totalPVI : 1 + totalPVI;
	}
	
	private double calculatePopulationObjectiveFunction() {
		double total = 0;
		double ideal = state.getPopulation() / state.getNumDistricts();
		for (District district : state.getDistricts()) {
			if (district.getId() == 0) continue;
			double population = district.getPopulation();
			if (population < ideal) {
				total += Math.pow(population / ideal, 0.9);
			} else if (population > ideal) {
				total += (ideal / population);
			} else {
				total += 1;
			}
		}
		return total / state.getNumDistricts();
	}
	
	private double calculateCompactnessObjectiveFunction() {
		double total = 0;
		for (District district : state.getDistricts()) {
			total += district.getCompactness();
		}
		return total / state.getNumDistricts();
	}

	public MoveData computeMoveData() {
		MoveData moveData = new MoveData();
		moveData.getDataMap().put("Objective Function", computeObjectiveFunction());
		double ppcScore = weightSet.get(WeightType.COMPACTNESS_POLSBY_POPPER_WEIGHT) != 0 ? calculateCompactnessObjectiveFunction() : 0; 
		moveData.getDataMap().put("Polsby Popper Compactness", ppcScore);
		double peScore = weightSet.get(WeightType.POPULATION_EQUALITY_WEIGHT) != 0 ? calculatePopulationObjectiveFunction() : 0; 
		moveData.getDataMap().put("Population Equality", peScore);
		double pfegScore = weightSet.get(WeightType.POLITICAL_FAIRNESS_EFFICIENCY_GAP_WEIGHT) != 0 ? calculateEfficiencyGapObjectiveFunction() : 0;
		moveData.getDataMap().put("Efficiency Gap", pfegScore);
		double pviScore = weightSet.get(WeightType.POLITICAL_FAIRNESS_PVI_WEIGHT) != 0 ? calculatePVIObjectiveFunction() : 0;
		moveData.getDataMap().put("PVI", pviScore);

		ArrayList<DistrictData> districtDataList = new ArrayList<>();
		for (District district : state.getDistricts()) {
			if (district.getId() == 0) continue;
			DistrictData newDistrictData = new DistrictData();
			newDistrictData.setDistrictId(district.getId());
			
			newDistrictData.getDataMap().put("Population", Double.parseDouble(district.getPopulation() + ""));
			newDistrictData.getDataMap().put("Compactness", district.getCompactness());
			newDistrictData.getDataMap().put("PVI", district.getPVI());
			newDistrictData.getDataMap().put("Efficiency Gap", district.getEfficiencyGap());
			districtDataList.add(newDistrictData);
		}
		moveData.setDistrictDataMap(districtDataList);
		return moveData;
	}
	
	
}

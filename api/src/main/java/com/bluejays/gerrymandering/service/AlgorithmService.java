package com.bluejays.gerrymandering.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.WeightType;
import com.bluejays.gerrymandering.function.FunctionCalculator;
import com.bluejays.gerrymandering.geometry.JsonTool;
import com.bluejays.gerrymandering.model.AlgorithmRun;
import com.bluejays.gerrymandering.model.BatchResult;
import com.bluejays.gerrymandering.model.BatchRun;
import com.bluejays.gerrymandering.model.Cluster;
import com.bluejays.gerrymandering.model.ClusterEdge;
import com.bluejays.gerrymandering.model.District;
import com.bluejays.gerrymandering.model.Edge;
import com.bluejays.gerrymandering.model.MoveData;
import com.bluejays.gerrymandering.model.MovePrecinct;
import com.bluejays.gerrymandering.model.Precinct;
import com.bluejays.gerrymandering.model.State;
import com.bluejays.gerrymandering.model.WeightSet;
import com.bluejays.gerrymandering.repository.EdgeRepository;
import com.bluejays.gerrymandering.repository.StateRepository;

@Service
public class AlgorithmService {
	private UserAlgorithmService userAlgorithmService; 
	private EdgeRepository edgeRepository;
	private StateRepository stateRepository;
	private FunctionCalculator calculator;
	
	@Value("${app.simulatedAnnealing.maxMoveCount}")
	private int maxMoveCount;
	
	@Autowired
	public AlgorithmService(UserAlgorithmService userAlgorithmService, EdgeRepository edgeRepository,
			StateRepository stateRepository, FunctionCalculator calculator) {
		this.userAlgorithmService = userAlgorithmService;
		this.edgeRepository = edgeRepository;
		this.stateRepository = stateRepository;
		this.calculator = calculator;
	}
	
	private State initializeAlgorithmData(AlgorithmRun algorithmRun) {
//		State state = null;
		JsonTool jt = new JsonTool(null);
        State state = new State();
        String stateName = algorithmRun.getStateName();
        try {
			jt.preprocess(state, "..\\data\\" + stateName + "\\" + stateName + ".geojson");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		if (algorithmRun.getStateName().equals("maine")) {
//			System.out.println("getting maine");
//			state = this.stateRepository.findById(6).map(foundState -> foundState)
//					.orElseThrow(() -> new RuntimeException("Could not find state for " + algorithmRun.getStateName()));
//		} else {
//			state = this.stateRepository.findDefaultStateByName(algorithmRun.getStateName()).map(foundState -> foundState)
//					.orElseThrow(() -> new RuntimeException("Could not find state for " + algorithmRun.getStateName()));
//		}
        
//		state.setMajorityMinorityConstraints(algorithmRun.getMajorityMinorityConstraints());
//		state.setNumDistricts(3);
		calculator.init(state, algorithmRun.getWeightSet());
		state.setNumMajMin(algorithmRun.getMajorityMinorityConstraints().get(0).intValue());
		state.setMaxMajMin(algorithmRun.getMajorityMinorityConstraints().get(1));
		state.setMinMajMin(algorithmRun.getMajorityMinorityConstraints().get(2));
//		state.reloadDistrict();
		initializeClusters(state);
		return state;
		
	}
	
    private void graphPartitioning(State state, long userId) {
        while (state.getClusters().size() != state.getNumDistricts() + 1) {
        	if (!userAlgorithmService.allowProcessing(userId)) {
				return;
			};
        	ClusterEdge edge = state.getBestClusterEdge();
            Cluster c1 = edge.getCluster1();
            Cluster c2 = edge.getCluster2();
            MovePrecinct newMove = Cluster.joinCluster(calculator, c1, c2);
        
            if (c1.getNeighboring(c2) != null) {
            	System.out.println("graphPartitioningerror1");
            }
            if (state.getClusters().contains(c2)) {
            	System.out.println("graphPartitioningerror2");
            }
            int c = 0;
            for (ClusterEdge e : state.getClusterEdges()) {
            	if (e.getConnection(c2) != null) {
            		c++;
            	}
            }
            if (c != 0) {
            	System.out.println("graphPartitioningerror3");
            }
            System.out.println("In gp");
            userAlgorithmService.addMove(newMove, userId);
        }
        ArrayList<District> districts = new ArrayList<>();
        int lastKnowSequentialId = 1;
        HashSet<Integer> clusterIdTaken = new HashSet<Integer>();
        for (Cluster cluster : state.getClusters()) {
        	if (cluster.getId() == 0 || cluster.getId() > state.getNumDistricts()) continue;
        	clusterIdTaken.add(cluster.getId());
        }
        for (Cluster cluster : state.getClusters()) {
        	if (cluster.getId() == 0) {
        		District empty = new District();
        		empty.setId(0);
        		empty.setState(state);
            	districts.add(empty);
        	} else {
        		if (clusterIdTaken.contains(cluster.getId())) {
        			// Clusters id is already in the appropriate range
        			districts.add(new District(cluster.getId(), cluster));
        		} else {
        			// Clusters id is not in the appropriate range, find a new one
        			while(clusterIdTaken.contains(lastKnowSequentialId)) {
        				lastKnowSequentialId++;
        			}
        			clusterIdTaken.add(lastKnowSequentialId);
        			userAlgorithmService.addMove(new MovePrecinct(0, lastKnowSequentialId, cluster.getId(), true), userId);
            		districts.add(new District(lastKnowSequentialId, cluster));
            		lastKnowSequentialId++;
        		}
        	}
        	System.out.println("Population: " + cluster.getPopulation());
            System.out.println("Compactness: " + cluster.getCompactness() + " Size: " + cluster.getPrecincts().size());
        	System.out.println("Efficiency Gap: " + cluster.getEfficiencyGap());
            System.out.println("PVI: " + cluster.getPVI());
        }
        state.setDistricts(districts);
        MoveData moveData = calculator.computeMoveData();
        userAlgorithmService.addFinalMove(moveData, state.getNumDistricts(), userId);
    }

    private void initializeClusters(State state) {
        Map<Integer, Cluster> clusters = new HashMap<>();
        Set<ClusterEdge> clusterEdges = new HashSet<>();
        state.getPrecincts().forEach (precinct -> {
            Cluster cluster = new Cluster(precinct);
            clusters.put(cluster.getId(), cluster);
        });
        state.getEdges().forEach (edge -> {
            Cluster cluster1 = clusters.get(edge.getPrecinct1().getId());
            Cluster cluster2 = clusters.get(edge.getPrecinct2().getId());
            ClusterEdge clusterEdge = new ClusterEdge(cluster1, cluster2);
            clusterEdge.getEdges().add(edge);
            cluster1.getBorder().add(clusterEdge);
            cluster2.getBorder().add(clusterEdge);
            clusterEdges.add(clusterEdge);
        });
        HashSet<Cluster> initializedClusters = new HashSet<>();
        initializedClusters.addAll(clusters.values());
        state.setClusters(initializedClusters);
        ArrayList<Cluster> majMinCluster = new ArrayList<>();
        initializedClusters.forEach(cluster -> {
        	if (cluster.isMajMinCluster()) {
        		majMinCluster.add(cluster);
        	}
        });
        state.setMajMinCluster(majMinCluster);
        for (ClusterEdge clusterEdge : clusterEdges) {
            clusterEdge.updateJoinability(calculator);
        }
        state.setClusterEdges(clusterEdges);
    }
	
	private void simulatedAnnealing(State state, Long userId) {
		generateMovablePrecincts(state);
		boolean foundMove = true;
		int moveCount = 0;
		while (foundMove && moveCount < maxMoveCount) {
			if (!userAlgorithmService.allowProcessing(userId)) {
				moveCount = maxMoveCount + 1;
				return;
			};
			foundMove = false;
			List<Precinct> movables = state.getMovablePrecincts();
			Collections.shuffle(movables);
			double oldObjectiveValue = calculator.computeObjectiveFunction();
			List<Collection<Precinct>> movings = new ArrayList<>(); // added
			for (Precinct precinct : movables) {
				if (foundMove) break;
				if (precinct.getId() == 0) continue;
				District from = precinct.getDistrict();
				for (Edge edge : precinct.getEdges()) {
					Precinct connection = edge.getConnection(precinct);
					if (connection.getId() == 0) continue;
					District to = connection.getDistrict();
					if (from.getId() != to.getId() && to.getId() != 0) {
						movings = transferPrecinct(precinct, from, to);
						if (from.getPrecincts().size() == 0 
								|| calculator.computeObjectiveFunction() <= oldObjectiveValue 
								|| !from.isConnected()
								) {
							transferPrecinct(precinct, to, from);
						} else {
							foundMove = true;
							moveCount++;
							addSimulatedAnnealingMove(to, from, precinct, userId);
							break;
						}
					}
				}
			}
			if (foundMove) {
				movables.removeAll(movings.get(0));
				movables.addAll(movings.get(1));
			}
		}
		System.out.println("\nAfter:");
		state.getDistricts().forEach(d -> {
            System.out.println("Compactness: " + d.getCompactness() + " Size: " + d.getPrecincts().size());
            System.out.println("Population: " + d.getPopulation());
            if (d.isMajMinDistrict()) {
    			System.out.println("This is a majmin district.");
            }
			System.out.println();
		});
	}
	
	private List<Collection<Precinct>> transferPrecinct(Precinct precinct, District from, District to) {
    	Collection<Precinct> removedMovables = from.removePrecinct(precinct);
    	Collection<Precinct> addedMovables = to.addPrecinct(precinct);
		return Arrays.asList(removedMovables, addedMovables);
    }
	
	private void addSimulatedAnnealingMove(District to, District from,
			Precinct movePrecinct, Long userId) {
		MovePrecinct newMove = new MovePrecinct(movePrecinct.getId(), to.getId(), from.getId(), false);
		userAlgorithmService.addMove(newMove, userId);
	}
	
	private void generateMovablePrecincts(State state) {
		List<Precinct> movablePrecincts = new ArrayList<>();
        for (District district : state.getDistricts()) {
        	if (district.getId() == 0) continue;
            for (Edge edge : district.getBorder()) {
                if (edge.getPrecinct1().getId() != 0 && edge.getPrecinct2().getId() != 0) {
                    movablePrecincts.add(edge.getPrecinct1());
                    movablePrecincts.add(edge.getPrecinct2());
                }
            }
        }
        state.setMovablePrecincts(movablePrecincts);
    }

	private void redistrict(AlgorithmRun algorithmRun, Long userId, boolean isBatchRun) {
		State state = initializeAlgorithmData(algorithmRun);
		MovePrecinct oldObjectFunctionMove = new MovePrecinct();
		MoveData objectiveFunctionData = new MoveData();
		objectiveFunctionData.getDataMap().put("Objective Function", calculator.computeObjectiveFunction());
		oldObjectFunctionMove.setMoveData(objectiveFunctionData);
		userAlgorithmService.addMove(oldObjectFunctionMove, userId);
		graphPartitioning(state, userId);
		System.out.println("Pausing");
		userAlgorithmService.pause(userId);
		System.out.println("Done with pause");
		simulatedAnnealing(state, userId);
		if (!isBatchRun) {
			MoveData moveData = calculator.computeMoveData();
			userAlgorithmService.addFinalMove(moveData, state.getNumDistricts(), userId);
		} else {
			State savedState = stateRepository.save(state);
			BatchResult batchResult = createBatchResult(savedState);
			userAlgorithmService.addBatchResult(batchResult, userId);
		}
	}

	public boolean startAlgorithm(AlgorithmRun algorithmRun, Long userId) {
		if (userAlgorithmService.allowNewRun(userId)) {
			userAlgorithmService.addUserInstance(userId, false);
			redistrict(algorithmRun, userId, false);
			userAlgorithmService.deactivate(userId);
			return true;
		}
		return false;
	}
	
	public void resumeAlgorithm(Long userId) {
		userAlgorithmService.resume(userId);
	}

	public void endAlgorithm(Long userId) {
		userAlgorithmService.deactivate(userId);
	}

	public List<MovePrecinct> pullMoveUpdates(Long userId) {
		return userAlgorithmService.getMoveUpdates(userId);
	}
	
	public BatchResult loadBatchResult(Integer batchId, Long userId) {
		return null;
	}
	
	public BatchResult pullBatchUpdate(Long userId) {
		return null;
	}
	
	public boolean startBatchRun(BatchRun batchRun, Long userId) {
		if (userAlgorithmService.allowNewRun(userId)) {
			userAlgorithmService.addUserInstance(userId, true);
			for (AlgorithmRun run : batchRun.getAlgorithmRuns()) {
				redistrict(run, userId, true);
			}
			userAlgorithmService.deactivate(userId);
			return true;
		}
		return false;
	}
	
	private BatchResult createBatchResult(State savedState) {
		return new BatchResult();
	}
}

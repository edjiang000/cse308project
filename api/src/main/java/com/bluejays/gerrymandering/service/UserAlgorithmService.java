package com.bluejays.gerrymandering.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bluejays.gerrymandering.model.BatchResult;
import com.bluejays.gerrymandering.model.MoveData;
import com.bluejays.gerrymandering.model.MovePrecinct;
import com.bluejays.gerrymandering.model.UserAlgorithm;

@Service
public class UserAlgorithmService {
	private static final Logger logger = LoggerFactory.getLogger(UserAlgorithmService.class);
	
	@Value("${app.moves.numberOfMovesToSend}")
	private Integer numberOfMovesToSend;
	
	private ConcurrentHashMap<Long, UserAlgorithm> userAlgorithmPool = new ConcurrentHashMap<Long, UserAlgorithm>();
	
	@Autowired
	public UserAlgorithmService() {
		
	}
	
	public void addUserInstance(Long userId, boolean runBatch) {
		if (!userAlgorithmPool.containsKey(userId)) {
			logger.info("Add new user instance: " + userId);
			userAlgorithmPool.put(userId, new UserAlgorithm());
		}
		if (runBatch) {
			userAlgorithmPool.get(userId).runBatch();
		} else {
			userAlgorithmPool.get(userId).run();
		}
	}
	
	public boolean allowProcessing(Long userId) {
		while(this.userAlgorithmPool.get(userId).isPaused()) {
			// Do nothing
		}
		if (this.userAlgorithmPool.get(userId).isInactive()) {
			logger.info("Exited algorithm: " + userId);
			return false;
		}
		return true;
	}
	
	public boolean allowNewRun(Long id) {
		return Optional.ofNullable(this.userAlgorithmPool.get(id)).map(userAlgorithm -> {
			return userAlgorithm.isInactive() || userAlgorithm.isPaused();
		}).orElse(true);
	}
	
	public void deactivate(Long userId) {
		Optional.ofNullable(this.userAlgorithmPool.get(userId)).ifPresent(userAlgorithm -> {
			userAlgorithm.deactivate();
		});
	}
	
	public void run(Long userId) {
		Optional.ofNullable(this.userAlgorithmPool.get(userId)).ifPresent(userAlgorithm -> {
			if (userAlgorithm.isInactive()) {
				userAlgorithm.run();
			}
		});
	}
	
	public void pause(Long userId) {
		Optional.ofNullable(this.userAlgorithmPool.get(userId)).ifPresent(userAlgorithm -> {
			if (userAlgorithm.isRunning()) {
				userAlgorithm.pause();
			}
		});
	}
	
	public void resume(Long userId) {
		Optional.ofNullable(this.userAlgorithmPool.get(userId)).ifPresent(userAlgorithm -> {
			if (userAlgorithm.isPaused()) {
				userAlgorithm.resume();
			}
		});
	}
	
	public void runBatch(Long userId) {
		Optional.ofNullable(this.userAlgorithmPool.get(userId)).ifPresent(userAlgorithm -> {
			if (userAlgorithm.isInactive()) {
				userAlgorithm.runBatch();
			}
		});
	}

	public List<MovePrecinct> getMoveUpdates(Long userId) {
		return Optional.ofNullable(this.userAlgorithmPool.get(userId)).map(userAlgorithm -> {
			List<MovePrecinct> returnList = new ArrayList<>();
			try {
				userAlgorithm.getMovesLock().lock();
//				System.out.println("Getting moves");
				if ((userAlgorithm.isInactive() || userAlgorithm.isPaused()) && userAlgorithm.getMoveProgress() >= userAlgorithm.getMoves().size()) {
					return createFinalMoveList();
				}
				if (userAlgorithm.getMoveProgress() >= userAlgorithm.getMoves().size()) {
					return createEmptyMoveList();
				}
				int startIndex = userAlgorithm.getMoveProgress();
				int endIndex = userAlgorithm.getMoveProgress() + numberOfMovesToSend >= userAlgorithm.getMoves().size() ?
				    userAlgorithm.increaseMoveProgress(userAlgorithm.getMoves().size() - userAlgorithm.getMoveProgress())
				    : userAlgorithm.increaseMoveProgress(numberOfMovesToSend);
				returnList = new ArrayList<>(userAlgorithm.getMoves().subList(startIndex, endIndex));
			} finally {
				userAlgorithm.getMovesLock().unlock();
			}
			return returnList;
		}).orElseGet(() -> {
			logger.info("Create empty move list");
			return createFinalMoveList();
		});
	}
	
	public void addMove(MovePrecinct move, Long userId) {
		if (move != null) {
//			System.out.print("Adding move");
			try {
				this.userAlgorithmPool.get(userId).getMovesLock().lock();
				List<MovePrecinct> moves = this.userAlgorithmPool.get(userId).getMoves();
					moves.add(move);
			} finally {
				this.userAlgorithmPool.get(userId).getMovesLock().unlock();
			}
		}
	}
	
	public void addFinalMove(MoveData moveData, int desiredNumDistricts, Long userId) {
		try {
			this.userAlgorithmPool.get(userId).getMovesLock().lock();
			List<MovePrecinct> moves = this.userAlgorithmPool.get(userId).getMoves();
			MovePrecinct newMove = new MovePrecinct(true);
			newMove.setMoveData(moveData);
			newMove.setDesiredNumDistricts(desiredNumDistricts);
			moves.add(newMove);
		} finally {
			this.userAlgorithmPool.get(userId).getMovesLock().unlock();
		}
	}
	
	private List<MovePrecinct> createEmptyMoveList() {
		return new ArrayList<MovePrecinct>();
	}
	
	private List<MovePrecinct> createFinalMoveList() {
		ArrayList<MovePrecinct> noRemainingMoves = new ArrayList<MovePrecinct>();
		noRemainingMoves.add(new MovePrecinct(true));
		return noRemainingMoves;
	}
	
	public BatchResult getBatchResult(Long userId) {
		return (BatchResult) Optional.ofNullable(this.userAlgorithmPool.get(userId)).map(userAlgorithm -> {
			synchronized (userAlgorithm.getBatchResults()) {
				int resultIndex = userAlgorithm.getBatchMoveProgress() >= userAlgorithm.getBatchResults().size() ?
						userAlgorithm.getBatchResults().size() - 1 : userAlgorithm.getBatchMoveProgress();
				return userAlgorithm.getBatchResults().get(resultIndex);
			}
		}).orElseGet(() -> {
			return null;
		});
	}
	
	public void addBatchResult(BatchResult batchResult, Long userId) {
		if (batchResult != null) {
			List<BatchResult> batchResults = this.userAlgorithmPool.get(userId).getBatchResults();
			synchronized(batchResults) {
				batchResults.add(batchResult);
			}
		}
	}
}
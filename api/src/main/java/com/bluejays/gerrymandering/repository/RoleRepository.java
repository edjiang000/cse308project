package com.bluejays.gerrymandering.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bluejays.gerrymandering.model.Role;
import com.bluejays.gerrymandering.model.Role.RoleName;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(RoleName roleUser);
}

package com.bluejays.gerrymandering.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bluejays.gerrymandering.model.Edge;

@Repository
public interface EdgeRepository extends JpaRepository<Edge, Long> {
//	@Query("SELECT e FROM Edge e WHERE e.state.id = ?1 AND (e.precinct1.id = ?2 OR e.precinct2.id = ?2)")
//	public List<Edge> findRelatedEdges(int stateId, int i);
}

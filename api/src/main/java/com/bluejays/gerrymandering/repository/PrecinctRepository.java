package com.bluejays.gerrymandering.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluejays.gerrymandering.model.Precinct;

@Repository
public interface PrecinctRepository extends JpaRepository<Precinct, Integer> {
	Optional<Precinct> findById(int id);
		
	@Transactional
	void deleteById(int id);
}

package com.bluejays.gerrymandering.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluejays.gerrymandering.model.GeojsonData;
import com.bluejays.gerrymandering.model.State;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {
	@Query("SELECT s FROM State s WHERE s.isDefaultState = true AND s.name = ?1")
	Optional<State> findDefaultStateByName(String stateName);
	
	Optional<State> findById(int id);
	
	Optional<State> findByName(String stateName);
	
	@Transactional
	void deleteById(int id);
}

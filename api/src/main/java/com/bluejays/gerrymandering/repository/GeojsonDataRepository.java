package com.bluejays.gerrymandering.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluejays.gerrymandering.model.GeojsonData;

@Repository
public interface GeojsonDataRepository extends JpaRepository<GeojsonData, Integer> {
	Optional<GeojsonData> findById(int id);
	
	Optional<GeojsonData> findByStateName(String stateName);
	
	@Transactional
	void deleteById(int id);
}

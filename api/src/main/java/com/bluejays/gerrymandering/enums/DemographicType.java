package com.bluejays.gerrymandering.enums;

public enum DemographicType {
	POPULATION, CAUCASIAN, AFRICAN_AMERICAN, ASIAN,
	NATIVE_AMERICAN, ISLANDER, OTHER, HISPANIC, TWO_OR_MORE
}

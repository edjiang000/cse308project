package com.bluejays.gerrymandering.enums;

public enum AlgorithmState {
	INACTIVE, RUN, PAUSE, BATCH
}

package com.bluejays.gerrymandering.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum WeightType {
	COMPACTNESS_POLSBY_POPPER_WEIGHT,
	COMPACTNESS_SCHWARTZBERG_WEIGHT,
	HISPANIC_EQUALITY_WEIGHT,
	POLITICAL_FAIRNESS_EFFICIENCY_GAP_WEIGHT,
	POLITICAL_FAIRNESS_PVI_WEIGHT,
	POPULATION_EQUALITY_WEIGHT,
	RACIAL_EQUALITY_WEIGHT,
	COUNTY_WEIGHT;
}

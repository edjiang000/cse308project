package com.bluejays.gerrymandering.enums;

public enum PartyType {
	DEMOCRATIC, REPUBLICAN, OTHERPARTY
}

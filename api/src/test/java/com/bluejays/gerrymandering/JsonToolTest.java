
package com.bluejays.gerrymandering;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import com.bluejays.gerrymandering.geometry.JsonTool;
import com.bluejays.gerrymandering.model.Precinct;
import com.bluejays.gerrymandering.model.State;

public class JsonToolTest {

	private static final String FILEPATH = "..\\data\\maine\\maine.geojson";
	
	public static void main(String[] args) throws Exception {
        test();
    }
	
	public static State test() throws Exception {
		JsonTool jt = new JsonTool(null);
        State state = new State();
        jt.preprocess(state, FILEPATH);
        state.setNumMajMin(1);
        state.setMaxMajMin(1);
        state.setMinMajMin(0.02);
        return state;
	}

}

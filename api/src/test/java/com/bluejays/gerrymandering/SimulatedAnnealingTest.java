package com.bluejays.gerrymandering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import com.bluejays.gerrymandering.function.FunctionCalculator;
import com.bluejays.gerrymandering.model.District;
import com.bluejays.gerrymandering.model.Edge;
import com.bluejays.gerrymandering.model.Precinct;
import com.bluejays.gerrymandering.model.State;

public class SimulatedAnnealingTest {

	public static FunctionCalculator calculator;
	public static double maxMoveCount = 500;
	
	public static void main(String[] args) throws Exception {
		simulatedAnnealing();
	}
	
	public static State simulatedAnnealing() throws Exception {
		State state = GraphPartitioningTest.partitionTest();
		calculator = GraphPartitioningTest.calculator;
		
		simulatedAnnealing(state);

		System.out.println("Max: " + state.getMaxMajMin());
		System.out.println("Min: " + state.getMinMajMin());
		System.out.println("ideal: " + (state.getPopulation() / state.getNumDistricts()));
		
		System.out.println("\nAfter:");
		state.getDistricts().forEach(d -> {
            System.out.println("Compactness: " + d.getCompactness() + " Size: " + d.getPrecincts().size());
            System.out.println("Population: " + d.getPopulation());
            if (d.isMajMinDistrict()) {
    			System.out.println("This is a majmin district.");
            }
			System.out.println();
		});
		
		
		
		
		return state;
		
		
	}
	
	
	private static void simulatedAnnealing(State state) {
		generateMovablePrecincts(state);
		boolean foundMove = true;
		int moveCount = 0;
		while (foundMove && moveCount < maxMoveCount) {
			foundMove = false;
			List<Precinct> movables = state.getMovablePrecincts();
			Collections.shuffle(movables);
			double oldObjectiveValue = calculator.computeObjectiveFunction();
			List<Collection<Precinct>> movings = new ArrayList<>();
			for (Precinct precinct : movables) {
				if (foundMove) break;
				District from = precinct.getDistrict();
				for (Edge edge : precinct.getEdges()) {
					Precinct connection = edge.getConnection(precinct);
					if (connection.getId() == 0) continue;
					District to = connection.getDistrict();
					if (from.getId() != 
							to.getId() && 
							to.getId() != 0) {
						movings = transferPrecinct(precinct, from, to);
						if (from.getPrecincts().size() == 0 
								|| calculator.computeObjectiveFunction() <= oldObjectiveValue 
								|| !from.isConnected()
								) {
							transferPrecinct(precinct, to, from);
						} else {
							foundMove = true;
							System.out.println(
									moveCount++
									);
							break;
						}
					}
				}
			}
			if (foundMove) {
				movables.removeAll(movings.get(0));
				movables.addAll(movings.get(1));
			}
		}
	}
	
	private static void generateMovablePrecincts(State state) {
		ArrayList<Precinct> movablePrecincts = new ArrayList<>();
        for (District district : state.getDistricts()) {
            for (Edge edge : district.getBorder()) {
                if (edge.getPrecinct1().getId() != 0 && edge.getPrecinct2().getId() != 0) {
                    movablePrecincts.add(edge.getPrecinct1());
                    movablePrecincts.add(edge.getPrecinct2());
                }
            }
        }
        state.setMovablePrecincts(movablePrecincts);
    }
	
	private static List<Collection<Precinct>> transferPrecinct(Precinct precinct, District from, District to) {
    	Collection<Precinct> removedMovables = from.removePrecinct(precinct);
    	Collection<Precinct> addedMovables = to.addPrecinct(precinct);
		return Arrays.asList(removedMovables, addedMovables);
    }
}

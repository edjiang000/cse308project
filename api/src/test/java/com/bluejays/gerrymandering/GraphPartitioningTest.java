package com.bluejays.gerrymandering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.bluejays.gerrymandering.enums.DemographicType;
import com.bluejays.gerrymandering.enums.WeightType;
import com.bluejays.gerrymandering.function.FunctionCalculator;
import com.bluejays.gerrymandering.model.Cluster;
import com.bluejays.gerrymandering.model.ClusterEdge;
import com.bluejays.gerrymandering.model.District;
import com.bluejays.gerrymandering.model.Edge;
import com.bluejays.gerrymandering.model.MovePrecinct;
import com.bluejays.gerrymandering.model.Precinct;
import com.bluejays.gerrymandering.model.State;
import com.bluejays.gerrymandering.model.WeightSet;

public class GraphPartitioningTest {
	
	public static FunctionCalculator calculator;
	
	
	private static final boolean IS_ORIGINAL_TEST = false;
	
	public static void main (String[] args) throws Exception {
		partitionTest();
	}
	
	public static State partitionTest() throws Exception {
		System.out.println("Initializing JSON...");
		State state = JsonToolTest.test();
		System.out.println("Initializing Calculator...");
		calculator = new FunctionCalculator();
		System.out.println("Initializing Weights...");
		Map<WeightType, Double> weightMap = new EnumMap<>(WeightType.class);
		weightMap.put(WeightType.POPULATION_EQUALITY_WEIGHT, 0.63);
		weightMap.put(WeightType.COMPACTNESS_POLSBY_POPPER_WEIGHT, 0.5);
		weightMap.put(WeightType.COMPACTNESS_SCHWARTZBERG_WEIGHT, 0.5);
		weightMap.put(WeightType.RACIAL_EQUALITY_WEIGHT, 0.0);
		weightMap.put(WeightType.COUNTY_WEIGHT, 0.0);
		weightMap.put(WeightType.HISPANIC_EQUALITY_WEIGHT, 0.0);
		weightMap.put(WeightType.POLITICAL_FAIRNESS_PVI_WEIGHT, 0.0);
		weightMap.put(WeightType.POLITICAL_FAIRNESS_EFFICIENCY_GAP_WEIGHT, 0.0);

		WeightSet weightSet = new WeightSet(0, "", weightMap);
		calculator.init(state, weightSet);

		System.out.println(calculator.computeObjectiveFunction());
		System.out.println("Initializing Clusters...");
		initializeClusters(state);
		System.out.println("Partitioning Graph...");
		graphPartitioning(state);
		return state;
	}
	
	
	
	private static void graphPartitioning(State state) {
        while (state.getClusters().size() != state.getNumDistricts()+1) {
        	//System.out.println(state.getClusters().size());
        	ClusterEdge edge = state.getBestClusterEdge();
        	System.out.println(edge.getJoinability());
            Cluster c1 = edge.getCluster1();
            Cluster c2 = edge.getCluster2();
            Cluster.joinCluster(calculator, c1, c2);
            if (c1.getNeighboring(c2) != null) {
            	System.out.println("error1");
            }
            if (state.getClusters().contains(c2)) {
            	System.out.println("error2");
            }
            int c = 0;
            for (ClusterEdge e : state.getClusterEdges()) {
            	if (e.getConnection(c2) != null) {
            		c++;
            	}
            }
            if (c != 0) {
            	System.out.println("error3");
            }
        }
        /*ArrayList<District> districts = new ArrayList<>();
        int lastKnowSequentialId = 1;
        HashSet<Integer> clusterIdTaken = new HashSet<Integer>();
        for (Cluster cluster : state.getClusters()) {
        	if (cluster.getId() == 0 || cluster.getId() > state.getNumDistricts()) continue;
        	clusterIdTaken.add(cluster.getId());
        }
        for (Cluster cluster : state.getClusters()) {
        	if (cluster.getId() == 0) {
        		districts.add(new District(0, cluster));
        	} else {
        		if (clusterIdTaken.contains(cluster.getId())) {
        			// Clusters id is already in the appropriate range
        			districts.add(new District(cluster.getId(), cluster));
        		} else {
        			// Clusters id is not in the appropriate range, find a new one
        			while(clusterIdTaken.contains(lastKnowSequentialId)) {
        				lastKnowSequentialId++;
        			}
        			clusterIdTaken.add(lastKnowSequentialId);
            		districts.add(new District(lastKnowSequentialId, cluster));
            		lastKnowSequentialId++;
        		}
        	}
        }*/
        ArrayList<District> districts = new ArrayList<>();
        int id = 1;
        for (Cluster cluster : state.getClusters()) {
        	if (cluster.getId() != 0) {
		        districts.add(new District(id++, cluster));
/*		        System.out.println("Compactness: " + cluster.getCompactness() + " Size: " + cluster.getPrecincts().size());
		        System.out.println("Population: " + cluster.getPopulation());
		        System.out.println("PVI: " + cluster.getPVI());
		        System.out.println("Efficiency Gap: " + cluster.getEfficiencyGap());
                System.out.println("Hispanic Population: " + cluster.getDemographic(DemographicType.HISPANIC));*/
        	} else {
        		District empty = new District();
        		empty.setId(0);
        		empty.setState(state);
            	districts.add(empty);
            }
        }
        state.setDistricts(districts);
        System.out.println("\nPartitioned Districts: ");
        state.getDistricts().forEach(d -> {
            System.out.println("Id: " + d.getId());
            System.out.println("Compactness: " + d.getCompactness() + " Size: " + d.getPrecincts().size());
            System.out.println("Population: " + d.getPopulation());
            System.out.println("PVI: " + d.getPVI());
            System.out.println("Efficiency Gap: " + d.getEfficiencyGap());
            if (d.isMajMinDistrict()) {
    			System.out.println("This is a majmin district.");
            }
			System.out.println();
		});
        System.out.println();
    }

    private static void initializeClusters(State state) {
        Map<Integer, Cluster> clusters = new HashMap<>();
        Set<ClusterEdge> clusterEdges = new HashSet<>();
        state.getPrecincts().forEach (precinct -> {
            Cluster cluster = new Cluster(precinct);
            clusters.put(cluster.getId(), cluster);
        });
        state.getEdges().forEach (edge -> {
            Cluster cluster1 = clusters.get(edge.getPrecinct1().getId());
            Cluster cluster2 = clusters.get(edge.getPrecinct2().getId());
            ClusterEdge clusterEdge = new ClusterEdge(cluster1, cluster2);
            clusterEdge.getEdges().add(edge);
            cluster1.getBorder().add(clusterEdge);
            cluster2.getBorder().add(clusterEdge);
            clusterEdges.add(clusterEdge);
        });
        HashSet<Cluster> initializedClusters = new HashSet<>();
        initializedClusters.addAll(clusters.values());
        state.setClusters(initializedClusters);
        ArrayList<Cluster> majMinCluster = new ArrayList<>();
        initializedClusters.forEach(cluster -> {
        	if (cluster.isMajMinCluster()) {
        		majMinCluster.add(cluster);
        	}
        });
        state.setMajMinCluster(majMinCluster);
        for (ClusterEdge clusterEdge : clusterEdges) {
            clusterEdge.updateJoinability(calculator);
        }
        state.setClusterEdges(clusterEdges);
        
        
        if (IS_ORIGINAL_TEST) {
	        //original Test
	        ArrayList<Cluster> cluster1 = new ArrayList<>();
	        ArrayList<Cluster> cluster2 = new ArrayList<>();
	        for (Precinct p : state.getPrecincts()) {
	        	if (p.getId() == 0) continue;
	        	if (p.getDistrict().getId() == 1) {
	        		cluster1.add(clusters.get(p.getId()));
	        	} else if (p.getDistrict().getId() == 2) {
	        		cluster2.add(clusters.get(p.getId()));
	        	}
	        }
	        joinAllClusters(calculator, cluster1);
	        joinAllClusters(calculator, cluster2);
	        System.out.println("Compactness: " + cluster1.get(0).getCompactness() + " Size: " + cluster1.get(0).getPrecincts().size());
	        System.out.println("Compactness: " + cluster2.get(0).getCompactness() + " Size: " + cluster2.get(0).getPrecincts().size());
	        System.exit(0);
        }
    }
    
    private static void joinAllClusters (FunctionCalculator calculator, List<Cluster> clusters) {
    	while (clusters.size() != 1) {
    		Cluster c1 = clusters.get(0);
    		Cluster c2 = clusters.remove(clusters.size()-1);
    		Cluster.joinCluster(calculator, c1, c2);
    	}
    }
}

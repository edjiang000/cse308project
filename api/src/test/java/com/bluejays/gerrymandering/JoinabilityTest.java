package com.bluejays.gerrymandering;

import java.util.Collection;
import java.util.HashSet;

import com.bluejays.gerrymandering.model.Cluster;
import com.bluejays.gerrymandering.model.ClusterEdge;
import com.bluejays.gerrymandering.function.FunctionCalculator;
import com.bluejays.gerrymandering.model.Precinct;
import com.bluejays.gerrymandering.model.State;
import com.bluejays.gerrymandering.model.WeightSet;

public class JoinabilityTest{
	public static void main(String[] args) {
		int totalPop = 1000;
		Precinct p1 = new Precinct();
		p1.setPopulation(100);
		Precinct p2 = new Precinct();
		Precinct otherP = new Precinct();
		
		Collection<Precinct> precincts = new HashSet();
		precincts.add(p1);
		precincts.add(p2);
		precincts.add(otherP);
		
		State s = new State();
		s.setPrecincts(precincts);
		s.setNumDistricts(2);
		
		p1.setState(s);
		p2.setState(s);
		otherP.setState(s);

		Cluster c1 = new Cluster(p1);
		Cluster c2 = new Cluster(p2);
		
		ClusterEdge clusterEdge = new ClusterEdge(c1, c2);
		
		WeightSet w = new WeightSet();
		
		FunctionCalculator j = new FunctionCalculator();
		j.init(s, w);
		
		//System.out.println(j.calculateCompactnessJoinability(clusterEdge));

		//double expected = (c1.getPopulation()+c2.getPopulation())/(s.getPopulation()/(double)s.getNumDistricts());
		for(int pop = 100; pop<=800; pop+=100) {
			p2.setPopulation(pop);
			otherP.setPopulation(totalPop - (p1.getPopulation()+p2.getPopulation()));
			
			p1.setState(s);
			p2.setState(s);
			otherP.setState(s);
			
			precincts = new HashSet();
			precincts.add(p1);
			precincts.add(p2);
			precincts.add(otherP);
			
			s.setPrecincts(precincts);

			double popJoin = j.calculatePopulationJoinability(clusterEdge);
			int p1p2pop = p1.getPopulation() + p2.getPopulation();
			System.out.println("Population Joinability for " + p1p2pop + ": " + popJoin);
		}
	}
}
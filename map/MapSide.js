"use strict"; {
let side = document.querySelector(`#side`);
let sideFloat = document.querySelector(`#side-float`);
const TRANSLATE = "320px";
document.querySelector(`#side-toggle-button`).addEventListener("click", function(event) {
	if (side.classList.contains("side-closed")) {
		side.style.transform = "";
		side.className = "side-opened";
		sideFloat.style.transform = `translateX(${TRANSLATE})`;
		this.textContent = "<"
	}
	else if (side.classList.contains("side-opened")) {
		side.style.transform = `translateX(-${TRANSLATE})`;
		side.className = "side-closed";
		sideFloat.style.transform = "";
		this.textContent = ">";
	}
});
}

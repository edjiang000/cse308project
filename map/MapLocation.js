"use strict"; {
let xhr = new XMLHttpRequest();
let modalProgress = $(`#modal-progress`);
let progressBar = document.querySelector(`#modal-progress-bar`);
xhr.addEventListener("error", function(event) {
	console.error("xhr error");
});
xhr.addEventListener("loadstart", function(event) {
	progressBar.setAttribute("aria-valuenow", 0);
	progressBar.setAttribute("aria-valuemax", event.total);
	progressBar.style.width = `0%`;
	progressBar.textContent = ``;
	modalProgress.modal("show");
});
var layerMap = {};
var precinctMap = {};
xhr.addEventListener("load", function(event) {
	precincts.addData(JSON.parse(xhr.response));
	currentLocation.boundary.removeFrom(map);

	console.log('current',precincts);
	/*
		{
			1: {
				color: 'red',
				precincts: [LayerObject,LayerObject]
			}
		}
	*/
	let precinctLayers = Object.values(precincts._layers);
	console.log('mapLoc', precinctLayers);
	precinctLayers.forEach(layer => {
		const precinctId = layer.feature.properties.OBJECTID;
		const districtId = layer.feature.properties.DISTRICT_ID;
		const color = colorOptions[districtId];
		const district = {
			color,
			precincts: [
				{
					id: precinctId,
					layerObject: layer 
				}
			]
		}
		layerMap[precinctId] = district;
		precinctMap[precinctId] = layer;
		layer.setStyle({fillColor: color, fillOpacity: .5, color: color});
	});
	num_districts = precinctLayers.length;
});

xhr.addEventListener("loadend", function(event) {
	modalProgress.modal("hide");
});
xhr.addEventListener("progress", function(event) {
	progressBar.setAttribute("aria-valuenow", event.loaded);
	let progressPercent = event.loaded / event.total * 100;
	progressBar.style.width = `${progressPercent}%`;
	progressBar.textContent = `${Math.round(progressPercent)}% (${event.loaded}/${event.total} bytes)`;
});
document.querySelector(`#search`).addEventListener("input", function(event) {
	let locationName = this.value;
	let location = LOCATIONS[locationName];
	if (currentLocation && currentLocation.boundary) { //Previous location
		currentLocation.boundary.addTo(map);
	}
	precincts.clearLayers();
	for (let element of document.querySelectorAll("#display-switches > * > input")) {
		element.checked = false;
	}
	currentLocation = location;
	map.fitBounds(location.boundingbox);
	if (!location.precincts) {
		switch (locationName) {
			case "Contiguous United States": case "United States": default:
				return;
		}
	}
	xhr.open("GET", location.precincts);
	xhr.send();
});
}

function resetLayerMap() {
	const precinctMapLength = Object.values(precinctMap).length;
	for (let i = 1;i <= precinctMapLength;i++) {
		const precinctLayer = precinctMap[i];
		const precinctId = precinctLayer.feature.properties.OBJECTID;
		const districtId = precinctLayer.feature.properties.DISTRICT_ID;
		const color = getRandomColor();
		const district = {
			color,
			precincts: [
				{
					id: precinctId,
					layerObject: precinctLayer
				}
			]
		}
		layerMap[precinctId] = district;
		precinctLayer.setStyle({fillColor: color, fillOpacity: .5, color: color});
	}
}

"use strict"; {
{
	function setPartnerElementValue(event) {
		this.partnerElement.value = this.value;
	};
	for (let i = 1; ; i++) {
		if (!document.querySelector(`#weight-${i}-container`)) {break;}
		let rangeInput = document.querySelector(`#weight-${i}-range`);
		let numberInput = document.querySelector(`#weight-${i}`);
		rangeInput.partnerElement = numberInput;
		numberInput.partnerElement = rangeInput;
		rangeInput.addEventListener("input", setPartnerElementValue);
		numberInput.addEventListener("input", setPartnerElementValue);
	}
}
{
	function setAllWeights(value) {
		for (let i = 1; ; i++) {
			let numberInput = document.querySelector(`#weight-${i}`);
			if (!numberInput) {break;}
			if (numberInput.value == value) {continue;}
			numberInput.value = value;
			numberInput.dispatchEvent(new Event("input"));
		}
	}
	document.querySelector(`#weights-set-zero`).addEventListener("click", function(event) {
		setAllWeights(0);
	});
	document.querySelector(`#weights-reset`).addEventListener("click", function(event) {
		setAllWeights(0.5);
	});
	document.querySelector(`#weights-set-one`).addEventListener("click", function(event) {
		setAllWeights(1);
	});
}
}

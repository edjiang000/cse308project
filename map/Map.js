"use strict";
const SERVER = "http://localhost:8080";
{
	L.GeoJSON.include({
		_resetStyle: function(layer) {
			Object.assign(layer.options, layer._defaultOptions);
			layer.setStyle(layer.options.style);
			return this;
		}
	});
}
const LOCATIONS = { //boundingbox: NE, SW
	"Contiguous United States": {boundary: undefined, boundingbox: L.latLngBounds([[49.384472, -66.947028], [24.520833, -124.771694]]), precincts: undefined}, //https://en.wikipedia.org/wiki/List_of_extreme_points_of_the_United_States
	"United States": {boundary: undefined, boundingbox: L.latLngBounds([[71.388889, -66.947028], [18.910833, -187.562222]]), precincts: undefined},
	//State bounds at `https://nominatim.openstreetmap.org/search.php?q=${state}&format=json&limit=2&polygon_geojson=1`
	"Alabama": {boundary: undefined, boundingbox: L.latLngBounds([[35.0081121, -84.8882885], [30.1375221, -88.4731355]]), precincts: undefined},
	"Alaska": {boundary: undefined, boundingbox: undefined /*L.latLngBounds([[71.6048217, 180], [51.0228712, -180]])*/, precincts: undefined}, //Alaska crosses anti-meridian
	"Arizona": {boundary: "../data/arizona/boundary.geojson", boundingbox: L.latLngBounds([[37.00426, -109.045131], [31.3322135, -114.8183584]]), precincts: "../data/arizona/arizona.geojson"},
	"Arkansas": {boundary: undefined, boundingbox: L.latLngBounds([[36.4996, -89.6422486], [33.004106, -94.6178557]]), precincts: undefined},
	"California": {boundary: undefined, boundingbox: L.latLngBounds([[42.009499, -114.1307816], [32.5295236, -124.482003]]), precincts: undefined},
	"Colorado": {boundary: undefined, boundingbox: L.latLngBounds([[41.0034002, -102.041585], [36.992426, -109.0601879]]), precincts: undefined},
	"Connecticut": {boundary: undefined, boundingbox: L.latLngBounds([[42.050587, -71.786994], [40.9492617, -73.727775]]), precincts: undefined},
	"Delaware": {boundary: undefined, boundingbox: L.latLngBounds([[39.8394337, -74.9849355], [38.4511276, -75.7890403]]), precincts: undefined},
	"Florida": {boundary: undefined, boundingbox: L.latLngBounds([[31.000968, -79.974306], [24.396308, -87.634896]]), precincts: undefined},
	"Georgia": {boundary: undefined, boundingbox: L.latLngBounds([[35.0013544, -80.751429], [30.355757, -85.6052418]]), precincts: undefined},
	"Hawaii": {boundary: undefined, boundingbox: L.latLngBounds([[28.517269, -154.7576591], [18.864031, -178.443593]]), precincts: undefined},
	"Idaho": {boundary: undefined, boundingbox: L.latLngBounds([[49.0008447, -111.043564], [41.989104, -117.2430324]]), precincts: undefined},
	"Illinois": {boundary: undefined, boundingbox: L.latLngBounds([[42.5082935, -87.0199244], [36.9701313, -91.5130518]]), precincts: undefined},
	"Indiana": {boundary: undefined, boundingbox: L.latLngBounds([[41.761368, -84.784609], [37.771742, -88.0997019]]), precincts: undefined},
	"Iowa": {boundary: undefined, boundingbox: L.latLngBounds([[43.5011333, -90.140061], [40.3756007, -96.6397162]]), precincts: undefined},
	"Kansas": {boundary: undefined, boundingbox: L.latLngBounds([[40.003095, -94.5882049], [36.993125, -102.0517563]]), precincts: undefined},
	"Kentucky": {boundary: undefined, boundingbox: L.latLngBounds([[39.1477997, -81.9645413], [36.496719, -89.571509]]), precincts: undefined},
	"Louisiana": {boundary: undefined, boundingbox: L.latLngBounds([[33.0194557, -88.758388], [28.855127, -94.0431869]]), precincts: undefined},
	"Maine": {boundary: "../data/maine/boundary.geojson", boundingbox: L.latLngBounds([[47.4598397, -66.8854162], [42.9222206, -71.0841694]]), precincts: "../data/maine/maine.geojson"},
	"Maryland": {boundary: undefined, boundingbox: L.latLngBounds([[39.7229331, -74.9851723], [37.886605, -79.4873055]]), precincts: undefined},
	"Massachusetts": {boundary: undefined, boundingbox: L.latLngBounds([[42.8867135, -69.8601042], [41.1888589, -73.508142]]), precincts: undefined},
	"Michigan": {boundary: undefined, boundingbox: L.latLngBounds([[48.306063, -82.1228056], [41.6960455, -90.4186202]]), precincts: undefined},
	"Minnesota": {boundary: undefined, boundingbox: L.latLngBounds([[49.3844901, -89.483385], [43.4994288, -97.2392619]]), precincts: undefined},
	"Mississippi": {boundary: undefined, boundingbox: L.latLngBounds([[34.996052, -88.0977945], [30.1477908, -91.655009]]), precincts: undefined},
	"Missouri": {boundary: undefined, boundingbox: L.latLngBounds([[40.6136347, -89.098843], [35.995683, -95.7741441]]), precincts: undefined},
	"Montana": {boundary: undefined, boundingbox: L.latLngBounds([[49.0011094, -104.039563], [44.357915, -116.0492317]]), precincts: undefined},
	"Nebraska": {boundary: undefined, boundingbox: L.latLngBounds([[43.0012719, -95.3080545], [39.9999776, -104.053514]]), precincts: undefined},
	"Nevada": {bboundary: undefined, oundingbox: L.latLngBounds([[42.002207, -114.039648], [35.0018894, -120.0057276]]), precincts: undefined},
	"New Hampshire": {boundary: undefined, boundingbox: L.latLngBounds([[45.3057789, -70.5613593], [42.6970417, -72.5572359]]), precincts: undefined},
	"New Jersey": {boundary: undefined, boundingbox: L.latLngBounds([[41.357757, -73.8940326], [38.7911303, -75.563386]]), precincts: undefined},
	"New Mexico": {boundary: undefined, boundingbox: L.latLngBounds([[37.0001523, -103.0022266], [31.3322114, -109.0502229]]), precincts: undefined},
	"New York": {boundary: undefined, boundingbox: L.latLngBounds([[45.0158611, -71.7955694], [40.477399, -79.761944]]), precincts: undefined},
	"North Carolina": {boundary: undefined, boundingbox: L.latLngBounds([[36.588157, -75.400119], [33.752878, -84.321869]]), precincts: undefined},
	"North Dakota": {boundary: undefined, boundingbox: L.latLngBounds([[49.0004918, -96.5543975], [45.9350359, -104.049265]]), precincts: undefined},
	"Ohio": {boundary: undefined, boundingbox: L.latLngBounds([[42.3232365, -80.518991], [38.4031419, -84.8203361]]), precincts: undefined},
	"Oklahoma": {boundary: undefined, boundingbox: L.latLngBounds([[37.0022994, -94.4312192], [33.6191955, -103.0024615]]), precincts: undefined},
	"Oregon": {boundary: undefined, boundingbox: L.latLngBounds([[46.2928325, -116.463504], [41.991794, -124.703541]]), precincts: undefined},
	"Pennsylvania": {boundary: "../data/pennsylvania/boundary.geojson", boundingbox: L.latLngBounds([[42.5146891, -74.689502], [39.7197662, -80.5210833]]), precincts: "../data/pennsylvania/pennsylvania.geojson"},
	"Rhode Island": {boundary: undefined, boundingbox: L.latLngBounds([[42.0188529, -71.0999607], [41.0959868, -71.9074127]]), precincts: undefined},
	"South Carolina": {boundary: undefined, boundingbox: L.latLngBounds([[35.2154852, -78.5413495], [32.033454, -83.3539979]]), precincts: undefined},
	"South Dakota": {boundary: undefined, boundingbox: L.latLngBounds([[45.94545, -96.4363403], [42.4798925, -104.057698]]), precincts: undefined},
	"Tennessee": {boundary: undefined, boundingbox: L.latLngBounds([[36.678118, -81.6469], [34.9829822, -90.310298]]), precincts: undefined},
	"Texas": {boundary: undefined, boundingbox: L.latLngBounds([[36.5004529, -93.5078217], [25.83706, -106.6458459]]), precincts: undefined},
	"Utah": {boundary: undefined, boundingbox: L.latLngBounds([[42.0015016, -109.0415767], [36.997968, -114.0528823]]), precincts: undefined},
	"Vermont": {boundary: undefined, boundingbox: L.latLngBounds([[45.016665, -71.465386], [42.72696, -73.43774]]), precincts: undefined},
	"Virginia": {boundary: undefined, boundingbox: L.latLngBounds([[39.466012, -75.166435], [36.5407896, -83.675413]]), precincts: undefined},
	"Washington": {boundary: undefined, boundingbox: L.latLngBounds([[49.0024392, -116.9174298], [45.5437226, -124.8360916]]), precincts: undefined},
	"West Virginia": {boundary: undefined, boundingbox: L.latLngBounds([[40.638801, -77.719029], [37.201483, -82.644739]]), precincts: undefined},
	"Wisconsin": {boundary: undefined, boundingbox: L.latLngBounds([[47.3025, -86.249548], [42.4919515, -92.8893149]]), precincts: undefined},
	"Wyoming": {boundary: undefined, boundingbox: L.latLngBounds([[45.0058281, -104.05216], [40.9948223, -111.055331]]), precincts: undefined},
	//Debug
	//"MaineTest": {boundary: "../data/maine/maineTestBoundary.geojson", boundingbox: L.latLngBounds([[47.4598397, -66.8854162], [42.9222206, -71.0841694]]), precincts: undefined}
};
let currentLocation = undefined;
let map = L.map('map', {
	fadeAnimation: false,
	zoomControl: false
})
.fitBounds(LOCATIONS["Contiguous United States"].boundingbox);
L.control.zoom({position: "bottomright"}).addTo(map);
L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}{r}.{ext}', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors', subdomains: 'abcd', minZoom: 0, maxZoom: 20, ext: 'png',
	//bounds: LOCATIONS["Contiguous United States"].boundingbox,
	detectRetina: true
}).addTo(map);
// L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png', {
// 	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
// 	subdomains: 'abcd',
// 	maxZoom: 19
// }).addTo(map);
map.addEventListener("moveend", function(event) {
	if (!LOCATIONS["Contiguous United States"].boundingbox.overlaps(map.getBounds())) {
		console.log("Map outside Contiguous United States");
	}
})
for (let [locationKey, locationValue] of Object.entries(LOCATIONS)) {
	if (typeof locationValue.boundary !== "string") {continue;}
	let xhr = new XMLHttpRequest();
	xhr.addEventListener("load", function(event) {
		let layer = L.geoJSON(JSON.parse(xhr.response), {
			attribution: "© OpenStreetMap contributors",
			style: {fillOpacity: 0},
			_state: locationKey

		}).addTo(map);
		layer.addEventListener("click", function(event) {
			let locationSelect = document.querySelector(`#search`);
			locationSelect.value = this.options._state;
			locationSelect.dispatchEvent(new Event("input"));
		})
		layer.addEventListener("mouseover", function(event) {
			this.setStyle({fillOpacity: 0.5});
		})
		layer.addEventListener("mouseout", function(event) {
			this.setStyle({fillOpacity: 0.5});
		})
		layer.bindTooltip(locationKey);
		layer._defaultOptions = Object.assign({}, layer.options);
		locationValue.boundary = layer;
	});
	xhr.open("GET", locationValue.boundary);
	xhr.send();
}
let colorOptions = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080'];

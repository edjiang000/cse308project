"use strict";
let selectedPrecincts = new Set();
{
	map.addEventListener("contextmenu", function(event) {
		if (selectedPrecincts.size < 2) {return;}
		let modal = $(`#modal-precincts-compare`);
		let table = modal[0].querySelector(".modal-body table");
		let properties = new Set();
		for (let precinct of selectedPrecincts) {
			for (let key of Object.keys(precinct.feature.properties)) {
				properties.add(key);
			}
		}
		for (let property of properties) {
			let row = table.insertRow();
			row.insertCell().textContent = property;
			for (let precinct of selectedPrecincts) {
				row.insertCell().textContent = precinct.feature.properties[property];
			}
		}
		modal.modal("show");
	});
	$(`#modal-precincts-compare`).on("hidden.bs.modal", function(event) {
		this.querySelector(`.modal-body table tbody`).textContent = "";
	});	
}

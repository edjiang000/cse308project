"use strict"; {
var majorityMinorityConstraints = 0;
var majorityMinorityConstraintsElement = document.querySelector(`#majority-minority-constraints`);
var majorityMinorityConstraintsList = [];
var RACES = ["white", "african-american", "native-american", "asian", "pacific-islander"];
function setPartnerElementValue(event) {
	this.partnerElement.value = this.value;
};

document.querySelector(`#majority-minority-constraint-add`).addEventListener("click", function(event) {
	majorityMinorityConstraints++;
	let baseId = `majority-minority-constraint-district-${majorityMinorityConstraints}`;
	majorityMinorityConstraintsElement.insertAdjacentHTML("beforeend", `
		<div id="${baseId}-container">
			<button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="collapse" data-target="#${baseId}" aria-expanded="false" aria-controls="${baseId}">District ${majorityMinorityConstraints}</button>
			<div class="collapse" id="${baseId}"></div>
		</div>
	`);
	let majorityMinorityConstraint = document.getElementById(baseId);
	for (let race of RACES) {
		let raceProperCase = race.replace("-", " ").replace(/\b\w/g, function(match) {return match.toUpperCase();})
		majorityMinorityConstraint.insertAdjacentHTML("beforeend", `
			<label for="${baseId}-${race}-min" id="${baseId}-${race}-name">${raceProperCase} minimum</label><br>
			<input class="custom-range" id="${baseId}-${race}-min-range" min="0" max="1" step="0.01" type="range" value="0">
			<input class="form-control" id="${baseId}-${race}-min" min="0" max="1" step="0.001" type="number" value="0">
			<label for="${baseId}-${race}-max" id="${baseId}-${race}-name">${raceProperCase} maximum</label><br>
			<input class="custom-range" id="${baseId}-${race}-max-range" min="0" max="1" step="0.01" type="range" value="1">
			<input class="form-control" id="${baseId}-${race}-max" min="0" max="1" step="0.001" type="number" value="1">
		`);
		for (let id of ["min", "max"]) {
			let rangeInput, numberInput;
			rangeInput = document.getElementById(`${baseId}-${race}-${id}-range`);
			numberInput = document.getElementById(`${baseId}-${race}-${id}`);
			rangeInput.partnerElement = numberInput;
			numberInput.partnerElement = rangeInput;
			rangeInput.addEventListener("input", setPartnerElementValue);
			numberInput.addEventListener("input", setPartnerElementValue);
		}
	}
});
document.querySelector(`#majority-minority-constraint-remove`).addEventListener("click", function(event) {
	if (majorityMinorityConstraints <= 0) {return;}
	majorityMinorityConstraintsElement.removeChild(document.querySelector(`#majority-minority-constraint-district-${majorityMinorityConstraints}-container`))
	majorityMinorityConstraints--;
});

}

function setPartnerElementValue(event) {
	this.partnerElement.value = this.value;
};

function addMinMajControl(num) {
	document.querySelector(`#majority-minority-constraint-add` + '-' + num).addEventListener("click", function(event) {
		majorityMinorityConstraintsList[num]++;
		let baseId = `majority-minority-constraint-district-${majorityMinorityConstraintsList[num]}-${num}`;
		let majorityMinorityConstraintsElement = document.querySelector(`#majority-minority-constraints-${num}`);
		majorityMinorityConstraintsElement.insertAdjacentHTML("beforeend", `
			<div id="${baseId}-container-${num}">
				<button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="collapse" data-target="#${baseId}" aria-expanded="false" aria-controls="${baseId}">District ${majorityMinorityConstraintsList[num]}</button>
				<div class="collapse" id="${baseId}"></div>
			</div>
		`);
		let majorityMinorityConstraint = document.getElementById(baseId);
		for (let race of RACES) {
			let raceProperCase = race.replace("-", " ").replace(/\b\w/g, function(match) {return match.toUpperCase();})
			majorityMinorityConstraint.insertAdjacentHTML("beforeend", `
				<label for="${baseId}-${race}-min-${num}" id="${baseId}-${race}-name">${raceProperCase} minimum</label><br>
				<input class="custom-range" id="${baseId}-${race}-min-range" min="0" max="1" step="0.01" type="range" value="0">
				<input class="form-control" id="${baseId}-${race}-min" min="0" max="1" step="0.001" type="number" value="0">
				<label for="${baseId}-${race}-max-${num}" id="${baseId}-${race}-name">${raceProperCase} maximum</label><br>
				<input class="custom-range" id="${baseId}-${race}-max-range" min="0" max="1" step="0.01" type="range" value="1">
				<input class="form-control" id="${baseId}-${race}-max" min="0" max="1" step="0.001" type="number" value="1">
			`);
			for (let id of ["min", "max"]) {
				let rangeInput, numberInput;
				rangeInput = document.getElementById(`${baseId}-${race}-${id}-range`);
				numberInput = document.getElementById(`${baseId}-${race}-${id}`);
				rangeInput.partnerElement = numberInput;
				numberInput.partnerElement = rangeInput;
				rangeInput.addEventListener("input", setPartnerElementValue);
				numberInput.addEventListener("input", setPartnerElementValue);
			}
		}
	});
}

function removeMinMajControl(num) {
	document.querySelector(`#majority-minority-constraint-remove`).addEventListener("click", function(event) {
		if (majorityMinorityConstraintsList[num] <= 0) {return;}
		majorityMinorityConstraintsList[num]--;
		let majorityMinorityConstraintsElement = document.querySelector(`#majority-minority-constraints-${num}`);
		majorityMinorityConstraintsElement.removeChild(document.querySelector(`#majority-minority-constraint-district-${majorityMinorityConstraintsList[num]}-container`))
	});
}

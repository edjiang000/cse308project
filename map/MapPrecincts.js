"use strict";
function getDemographicTooltip(layer) {
	var properties = layer.feature.properties;
	var precinctId = properties['OBJECTID'];
	var carouselName = 'dataCarousel' + precinctId;
	var textColor = 'ivory';
	var canvasWidth = '100';
	var canvasHeight = '100';

	var demographicData = {
		datasets: [{
			data: [properties['white'], properties['black'], properties['native'], properties['asian'], properties['islander'], properties['twoormore'], properties['other']],
			backgroundColor: ['#FF6E29', '#E82AE2', '#2AE8C3', '#3B4EFF', '#86FF26', 'grey', '#fff'],
			borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
		}],
		labels: ['White', 'African American', 'Native American', 'Asian', 'Pacific Islander', 'Two or More Races', 'Other']
	}

	var hispanicData = {
		datasets: [{
			data: [properties['POP10']-properties['hispanic'], properties['hispanic']],
			backgroundColor: ['magenta', 'orange'],
			borderColor: ['transparent', 'transparent']
		}],
		labels: ['Non-Hispanic', 'Hispanic']
	}

	var votingData = {
		datasets: [{
			data: [properties['democrat'], properties['republican'], properties['otherparty']],
			backgroundColor: ['blue', 'red', 'grey'],
			borderColor: ['transparent', 'transparent', 'transparent']
		}],
		labels: ['Democrat', 'Republican', 'Other']
	}


	var chartDiv = document.createElement('div');
	chartDiv.classList.add("PrecinctTooltip");

	var exitIcon = document.createElement('span');
	exitIcon.textContent = '×';

	var exitButton = document.createElement('button');
	exitButton.id = 'tooltipExitButton' + precinctId;
	exitButton.type = 'button';
	exitButton.className = 'close';
	exitButton.appendChild(exitIcon);

	var titleText = document.createElement('h6');
	titleText.appendChild(document.createTextNode("Precinct " + precinctId));
	titleText.style.color = textColor;
	titleText.align = 'center';

	var topText = document.createElement('p');
	topText.appendChild(document.createTextNode(properties['COUNTY'] + " County"));
	topText.style.color = textColor;
	topText.align = 'center';

	var carousel = document.createElement('div');
	carousel.id = carouselName;
	carousel.className = 'carousel slide';
	carousel.setAttribute('data-ride','carousel');
	carousel.setAttribute('data-interval', 'false');

	var carouselInner = document.createElement('div');
	carouselInner.className = 'carousel-inner';

	// Racial Demographics
	var raceCarouselItem = document.createElement('div');
	raceCarouselItem.className = 'carousel-item active';

	var raceCanvas = document.createElement('canvas');
	raceCanvas.height = canvasHeight;
	raceCanvas.width = canvasWidth;

	var populationText = document.createElement('p');
	populationText.appendChild(document.createTextNode("Total Population: " + properties['POP10']));
	populationText.style.color = textColor;

	var raceDiv = document.createElement('div');
	raceDiv.appendChild(raceCanvas);
	raceDiv.appendChild(populationText);

	raceCarouselItem.appendChild(raceDiv);

	// Hispanic Demographics
	var hispanicCarouselItem = document.createElement('div');
	hispanicCarouselItem.className = 'carousel-item';

	var hispanicCanvas = document.createElement('canvas');
	hispanicCanvas.height = canvasHeight;
	hispanicCanvas.width = canvasWidth;

	var hispanicDiv = document.createElement('div');
	hispanicDiv.appendChild(hispanicCanvas);

	hispanicCarouselItem.appendChild(hispanicDiv);

	// Voting Data
	var votingCarouselItem = document.createElement('div');
	votingCarouselItem.className = 'carousel-item';

	var votingCanvas = document.createElement('canvas');
	votingCanvas.height = canvasHeight;
	votingCanvas.width = canvasWidth;

	var votingDiv = document.createElement('div');
	votingDiv.appendChild(votingCanvas);

	votingCarouselItem.appendChild(votingDiv);


	// Add items to carousel
	carouselInner.appendChild(raceCarouselItem);
	carouselInner.appendChild(hispanicCarouselItem)
	carouselInner.appendChild(votingCarouselItem)
	carousel.appendChild(carouselInner);

	// Add buttons
	var carouselPrevButton = document.createElement('a');
	carouselPrevButton.className = 'carousel-control-prev';
	carouselPrevButton.href = '#' + carouselName;
	carouselPrevButton.role = 'button';
	carouselPrevButton.setAttribute('data-slide', 'prev');

	var carouselPrevIcon = document.createElement('span');
	carouselPrevIcon.className = 'carousel-control-prev-icon';
	carouselPrevButton.appendChild(carouselPrevIcon);

	var carouselNextButton = document.createElement('a');
	carouselNextButton.className = 'carousel-control-next';
	carouselNextButton.href = '#' + carouselName;
	carouselNextButton.role = 'button';
	carouselNextButton.setAttribute('data-slide', 'next');

	var carouselNextIcon = document.createElement('span');
	carouselNextIcon.className = 'carousel-control-next-icon';
	carouselNextButton.appendChild(carouselNextIcon);

	carousel.appendChild(carouselNextButton);
	carousel.appendChild(carouselPrevButton);

	chartDiv.setAttribute('style','width:250px;height:350px');
	chartDiv.appendChild(exitButton);
	chartDiv.appendChild(titleText);
	chartDiv.appendChild(topText);
	chartDiv.appendChild(carousel);

	// Init charts
	var raceDoughnut = new Chart(raceCanvas, {
		type: 'doughnut',
		data: demographicData,
		options: {
			legend: {
				labels: {
					fontColor: textColor
				}
			},
			cutoutPercentage: 70
		}
	});
	var hispanicPie = new Chart(hispanicCanvas, {
		type: 'doughnut',
		data: hispanicData,
		options: {
			legend: {
				labels: {
					fontColor: textColor
				}
			},
			cutoutPercentage: 70
		}
	});
	var votingPie = new Chart(votingCanvas, {
		type: 'doughnut',
		data: votingData,
		options: {
			legend: {
				labels: {
					fontColor: textColor
				}
			},
			cutoutPercentage: 70
		}
	});
	//console.log(carousel);
	exitButton.addEventListener('click', function(){
		chartDiv.remove();
		selectedPrecincts.delete(layer);
		precincts.setStyle({fillOpacity: 0.5});
		//precincts._resetStyle(layer);
	});
	return chartDiv;
};
function getRandomColor() {
	let randomColor = "#";
	for (let i = 0; i < 6; i++) {
		randomColor += ((Math.random() * 16) | 0).toString(16);
	}
	return randomColor;
};
function showDistrictDiv(districtId){
	var resultsTab = $('#results-tab');
	resultsTab[0].innerHTML = '';
	resultsTab[0].appendChild(layerMap[districtId]['divStorage']);
}
let precincts = L.geoJSON(null, {
	onEachFeature: function(feature, layer) {
		layer.addEventListener("click", function(event) {
			if (selectedPrecincts.has(this)) {return;}
			selectedPrecincts.add(this);
			this.setStyle({fillOpacity: .75});
			document.querySelector(`#selected-precincts`).appendChild(getDemographicTooltip(this));
			var districtId;
			if(useNewDistrictId){
				for(var i = 1; i<Object.values(layerMap).length; i++){
					const district = layerMap[i];
					district.precincts.forEach(precinct => {					
						if(precinct.id == feature['properties']['OBJECTID']){
							districtId = i;
						}
					});
					if(districtId == i){
						break;
					}
				}
			}
			else{
				districtId = feature['properties']['DISTRICT_ID'];
			}
			if(!('divStorage' in layerMap[districtId])){	
				var districtDiv = getDistrictDiv(districtId, true)
				console.log(districtDiv);
				layerMap[districtId].divStorage = districtDiv;
			}
			showDistrictDiv(districtId);			
		});
		layer.addEventListener("mouseover", function(event) {
			if (selectedPrecincts.has(this)) {return;}
			this.setStyle({fillOpacity: .75});
		});
		layer.addEventListener("mouseout", function(event) {
			if (selectedPrecincts.has(this)) {return;}
			this.setStyle({fillOpacity: .5});
		});
		console.log('mapPre');
		layer.bindTooltip(""+feature.properties.OBJECTID, {direction: "top", sticky: true});
		// layer.setStyle({fillColor: getRandomColor()});
		// layer._defaultOptions = Object.assign({}, layer.options);
	},
	style: {weight: 1}
})
.addTo(map);
{
	function getMaxParty(partyVotes) {
		let parties = Object.keys(partyVotes);
		let maxParty = parties[0];
		let maxVotes = partyVotes[parties[0]];
		for (let i = 1; i < parties.length; i++) {
			let party = parties[i];
			let votes = partyVotes[party];
			if (votes > maxVotes) {
				maxParty = party;
				maxVotes = votes;
			}
		}
		return maxParty;
	}

	function getPartyColor(party) {
		switch (party) {
			case "democrat": return "blue";
			case "republican": return "red";
			default: return "gray";
		}
	}
	function toggleOffOtherSwitches(switchElement) {
		for (let element of document.querySelectorAll("#display-switches > * > input")) {
			if (switchElement !== element) {
				element.checked = false;
			}
		}
		resetDistrictColors(Object.keys(layerMap).length);
	}

	document.querySelector(`#display-original-districts`).addEventListener("input", function(event) {
		toggleOffOtherSwitches(this);
		if (this.checked) {
			precincts.eachLayer(function(layer) {
				let districtColor = colorOptions[layer.feature.properties["DISTRICT_ID"] - 1];
				layer.setStyle({color: districtColor, fillColor: districtColor, fillOpacity: 0.5});
			});
		}
		else {
			// precincts.eachLayer(function(layer) {
			// 	precincts._resetStyle(layer);
			// });
			resetDistrictColors(Object.values(precincts._layers).length);
		}
	});
	
	document.querySelector(`#display-original-districts-party`).addEventListener("input", function(event) {
		toggleOffOtherSwitches(this);
		if (this.checked) {
			let districtsVotes = {};
			let PARTIES = ["democrat", "republican"];
			precincts.eachLayer(function(layer) {
				let properties = layer.feature.properties;
				let DISTRICT_ID = properties["DISTRICT_ID"];
				if (!districtsVotes.hasOwnProperty(DISTRICT_ID)) {
					let districtVotes = {};
					for (let party of PARTIES) {
						districtVotes[party] = 0;
					}
					districtsVotes[DISTRICT_ID] = districtVotes;
				}
				let districtVotes = districtsVotes[DISTRICT_ID];
				for (let party of PARTIES) {
					districtVotes[party] += properties[party];
				}
			});
			let districtsParty = {};
			for (let DISTRICT_ID of Object.keys(districtsVotes)) {
				districtsParty[DISTRICT_ID] = getMaxParty(districtsVotes[DISTRICT_ID]);
			}
			precincts.eachLayer(function(layer) {
				let partyColor = getPartyColor(districtsParty[layer.feature.properties["DISTRICT_ID"]])
				layer.setStyle({color: partyColor, fillColor: partyColor});
			});
		}
		else {
			resetDistrictColors(Object.values(precincts._layers).length);
		}
	});

	document.querySelector(`#display-original-state-party`).addEventListener("input", function(event) {
		toggleOffOtherSwitches(this);
		if (this.checked) {
			let PARTIES = ["democrat", "republican"];
			let partyVotes = {};
			for (let party of PARTIES) {
				partyVotes[party] = 0;
			}
			for (let precinct of Object.values(precincts._layers)) {
				let properties = precinct.feature.properties;
				for (let party of PARTIES) {
					partyVotes[party] += properties[party];
				}
			}
			let partyColor = getPartyColor(getMaxParty(partyVotes));
			precincts.setStyle({color: partyColor, fillColor: partyColor});
		}
		else {
			resetDistrictColors(Object.values(precincts._layers).length);
		}
	})

	document.querySelector(`#display-african-american-population-distribution`).addEventListener("input", function(event) {
		toggleOffOtherSwitches(this);
		if (this.checked) {
			precincts.eachLayer(function(layer) {
				let blackRatio = layer.feature.properties["black"] / layer.feature.properties["POP10"];
				let fillColor;
				//http://colorbrewer2.org/#type=sequential&scheme=Reds&n=9
				//and #340000
					 if (blackRatio < 0.0) {fillColor = "#ffffff";}
				else if (blackRatio < 0.1) {fillColor = "#fff5f0";}
				else if (blackRatio < 0.2) {fillColor = "#fee0d2";}
				else if (blackRatio < 0.3) {fillColor = "#fcbba1";}
				else if (blackRatio < 0.4) {fillColor = "#fc9272";}
				else if (blackRatio < 0.5) {fillColor = "#fb6a4a";}
				else if (blackRatio < 0.6) {fillColor = "#ef3b2c";}
				else if (blackRatio < 0.7) {fillColor = "#cb181d";}
				else if (blackRatio < 0.8) {fillColor = "#a50f15";}
				else if (blackRatio < 0.9) {fillColor = "#67000d";}
				else if (blackRatio < 1.0) {fillColor = "#340000";}
				else {fillColor = "#000000"}
				layer.setStyle({fillColor: fillColor, color: fillColor});
			});
		}
		else {
			resetDistrictColors(Object.values(precincts._layers).length);
		}
	})

	document.querySelector(`#display-majority-minority-districts`).addEventListener("input", function(event) {
		if (!(phaseOneEnded || phaseTwoEnded)) {
			this.checked = false;
			return;
		}
		toggleOffOtherSwitches(this);
		if (this.checked) {
			let majorityMinorityDistricts = highlightMajorityMinorityDistricts();
			console.log(majorityMinorityDistricts);
			for (let i = 1; i <= Object.keys(layerMap).length; i++) {
				if (majorityMinorityDistricts.has(""+i)) {continue;}
				console.log(i);
				for (let precinct of layerMap[i].precincts) {
					console.log(precinct);
					precinct.layerObject.setStyle({fillColor: "gray", fillOpacity: 0.5, color: "gray"});
				}
			}
		}
		else {
			resetDistrictColors(Object.values(precincts._layers).length);
		}
	})
}

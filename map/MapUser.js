"use strict";
// User login, logout, register, response messages

let currentUser;

function onLogin() {
	toggleLoginOutButton();
	document.querySelector(`#side-toggle-button`).toggleAttribute("disabled", false);
}

function toggleLoginOutButton() {
	let loginOutButton = document.querySelector(`#login-out-button`);
	let icon = loginOutButton.querySelector("use")
	let registerButton = document.querySelector(`#register-button`)
	switch (loginOutButton.getAttribute("data-action")) {
		case "login":
			icon.setAttribute("href", "#account-logout");
			loginOutButton.lastChild.nodeValue = " Log out";
			loginOutButton.setAttribute("data-toggle-inactive", loginOutButton.getAttribute("data-toggle")); loginOutButton.removeAttribute("data-toggle");
			loginOutButton.setAttribute("data-target-inactive", loginOutButton.getAttribute("data-target")); loginOutButton.removeAttribute("data-target");
			loginOutButton.setAttribute("data-action", "logout");
			registerButton.style.display = "none";
			break;
		case "logout":
			icon.setAttribute("href", "#account-login");
			loginOutButton.lastChild.nodeValue = " Log in";
			loginOutButton.setAttribute("data-toggle", loginOutButton.getAttribute("data-toggle-inactive")); loginOutButton.removeAttribute("data-toggle-inactive");
			loginOutButton.setAttribute("data-target", loginOutButton.getAttribute("data-target-inactive")); loginOutButton.removeAttribute("data-target-inactive");
			loginOutButton.setAttribute("data-action", "login");
			registerButton.style.display = "";
			break;
	}
}

// Attempt to login on open
if (localStorage.getItem('access_token')) {
	axios.get(`${SERVER}/api/user/me`, {headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}})
	.then((res) => {
		const user = { ...res.data };
		onLogin();
		setMessage('info', `Welcome ${user.name}`);
		// Do user account stuff
		currentUser = user;
	}).catch((err) => {

	});
}

function setMessage(type, message) {
	let textClass = "";
	switch (type) {
		case "error": textClass = "text-danger"; break;
	}
	let modalMessagesAccount = $(`#modal-messages-account`);
	modalMessagesAccount[0].querySelector(`.modal-body`).innerHTML = `<span class="${textClass}">${message}</span>`;
	modalMessagesAccount.modal({focus: true}); //Focus does not work
}

document.getElementById('modal-login-button').addEventListener("click", async (event) => {
	try {
		// TODO abstract API url
		const res = await axios.post(`${SERVER}/api/auth/login`, {
			email: document.getElementById('modal-login-email').value,
			password: document.getElementById('modal-login-password').value
		});
		event.target.form.reset();
		const credentials = { ...res.data };
		try {
			// TODO abstract API url
			const res = await axios.get(`${SERVER}/api/user/me`, {headers: {'Authorization': `${credentials.tokenType} ${credentials.accessToken}`}});
			const user = { ...res.data };
			localStorage.setItem('access_token', credentials.accessToken);
			onLogin();
			setMessage('info', `Welcome ${user.name}`);
			// TODO Do account settings stuff
			currentUser = user;
		} catch (err) {
			setMessage('error', 'Error while logging in. Try again');
		}
	} catch (err) {
		setMessage('error', 'Error while logging in. Try again');
	}
});

document.getElementById('modal-register-button').addEventListener("click", async (event) => {
	try {
		// TODO abstract API url
		const res = await axios.post(`${SERVER}/api/auth/register`, {
			name: document.getElementById('modal-register-name').value,
			email: document.getElementById('modal-register-email').value,
			password: document.getElementById('modal-register-password').value
		});
		event.target.form.reset();
		const { message } = res.data;
		setMessage('info', message);
	} catch (err) {
		setMessage('error', 'Error registering user. Try again');
	}
});

document.getElementById("login-out-button").addEventListener("click", function(event) {
	if (this.getAttribute("data-action") !== "logout") {return;}
	if (!localStorage.getItem("access_token")) {return;}
	localStorage.removeItem("access_token");
	currentUser = null;
	toggleLoginOutButton();
	event.stopPropagation();
})

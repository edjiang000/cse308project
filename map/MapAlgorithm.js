"use strict"; 
const MINORITY_ID = "african-american";
const MINORITY_PROPERTY = "black";
const PULL_UPDATES_TIME = 500;
var EMPTY_PULLS_TILL_STOP = 4;
let majorityMinorityRequested = false;
var phaseOneEnded = false;
var phaseTwoEnded = false;
var realTimeUpdates;
let pullUpdateInterval;
let updates = [];
let phaseOneDataMap = null;
var useNewDistrictId = false;
var defaultObjectiveFunction = 0.0;
function toggleAlgorithm() {
	let noInput;
	{
		let algorithmButton = document.querySelector(`#algorithm-button`);
		let icon = algorithmButton.querySelector("use");
		let mapElement = document.querySelector(`#map`);
		switch (algorithmButton.getAttribute("data-action")) {
			case "algorithm-start":
				if (!currentUser) {return;}
				algorithmButton.setAttribute("data-action", "algorithm-stop");
				algorithmButton.classList.remove("btn-primary");
				algorithmButton.classList.add("btn-danger");
				icon.setAttribute("href", "#media-stop");
				algorithmButton.lastChild.nodeValue = " Stop";
				mapElement.style.cursor = "progress";
				phaseOneEnded ? resumeAlgorithm() : startAlgorithm();
				noInput = true;
				useNewDistrictId = true;
				break;
			case "algorithm-stop":
				algorithmButton.setAttribute("data-action", "algorithm-start");
				algorithmButton.classList.remove("btn-danger");
				algorithmButton.classList.add("btn-primary");
				icon.setAttribute("href", "#media-play");
				algorithmButton.lastChild.nodeValue = " Start";
				mapElement.style.cursor = "";
				console.log(phaseOneEnded);
				console.log(phaseTwoEnded);
				!phaseOneEnded && !phaseTwoEnded ? exitAlgorithm() : stopAlgorithm();
				noInput = false;
				break;
		}
	}


	document.querySelector(`#search`).toggleAttribute("disabled", noInput);
	document.querySelector(`#algorithm-update`).toggleAttribute("disabled", noInput);
	for (let i = 1; ; i++) {
		let baseId = `weight-${i}`;
		if (!document.getElementById(`${baseId}-container`)) {break;}
		let rangeInput = document.getElementById(`${baseId}-range`);
		let numberInput = document.getElementById(`${baseId}`);
		rangeInput.toggleAttribute("disabled", noInput);
		numberInput.toggleAttribute("readonly", noInput);
	}
	for (let i = 1; ; i++) {
		let baseId = `majority-minority-constraint-district-${i}`;
		if (!document.getElementById(`${baseId}-container`)) {break;}
		for (let race of RACES) {
			for (let id of ["min", "max"]) {
				document.getElementById(`${baseId}-${race}-${id}-range`).toggleAttribute("disabled", noInput);
				document.getElementById(`${baseId}-${race}-${id}`).toggleAttribute("readonly", noInput);
			}
		}
	}
	document.querySelector(`#weights-set-zero`).toggleAttribute("disabled", noInput);
	document.querySelector(`#weights-reset`).toggleAttribute("disabled", noInput);
	document.querySelector(`#weights-set-one`).toggleAttribute("disabled", noInput);
	document.querySelector(`#majority-minority-constraint-add`).toggleAttribute("disabled", noInput);
	document.querySelector(`#majority-minority-constraint-remove`).toggleAttribute("disabled", noInput);
	for (let element of document.querySelectorAll("#display-switches > * > input")) {
		element.toggleAttribute("disabled", noInput);
	}
}
function startAlgorithm() {
	resetLayerMap();
	realTimeUpdates = document.getElementById('algorithm-update').checked;
	let data = {};
	for (let i = 1; ; i++) {
		let elementId = `weight-${i}`;
		let element = document.getElementById(elementId);
		if (!element) {break;}
		let elementName = document.getElementById(elementId + '-name');
		if (!elementName) {break;}
		let refactoredName = elementName.innerText;
		refactoredName = refactoredName.toUpperCase();
		refactoredName = refactoredName.replace(/\s/g, "_");
		refactoredName = refactoredName + '_WEIGHT';
		data[refactoredName] = element.value;

	}
	// let minMajData = {}
	// for (let i = 1; ; i++) {
	// 	let baseId = `majority-minority-constraint-district-${i}`;
	// 	if (!document.getElementById(`${baseId}-container`)) {break;}
	// 	for (let race of RACES) {
	// 		for (let id of ["min", "max"]) {
	// 			let elementId = `${baseId}-${race}-${id}`;
	// 			minMajData[elementId] = document.getElementById(elementId).value;
	// 		}
	// 	}
	// }
	// console.log('minMajData',minMajData);
	console.log(document.getElementById('search').value);
	let stateName = document.getElementById('search').value;
	if (stateName === 'Contiguous United States') {
		toggleAlgorithm();
		return;
	}
	stateName = stateName.toLowerCase();
	let majorityMinorityDistricts = 0;
	for (let i = 1; ; i++) {
		let baseId = `majority-minority-constraint-district-${i}`;
		if (!document.getElementById(`${baseId}-container`)) {break;}
		majorityMinorityDistricts = i;
	}
	majorityMinorityRequested = majorityMinorityDistricts > 0;
	let minorityMinimum = 0, minorityMaximum = 0;
	if (majorityMinorityRequested) {
		let baseId = `majority-minority-constraint-district-1-${MINORITY_ID}`
		minorityMinimum = Number(document.getElementById(`${baseId}-min`).value);
		minorityMaximum = Number(document.getElementById(`${baseId}-max`).value);
	}
	const dataObj = {
		stateName: stateName,
		weights: data,
		majorityMinorityConstraints: [
			majorityMinorityDistricts, minorityMaximum, minorityMinimum
		]
	};
	console.log('dataObj', dataObj);
	axios.post(`${SERVER}/api/algorithm/start`, dataObj, {headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}})
	.then((res) => {
		// Algorithm finished, updates may still be available
	})
	.catch((err) => {
		// stopAlgorithm2();
		// toggleAlgorithm();
	});
	var amountOfEmptyPulls = 0;
	var moveData;
	var moveDataGotten = 0;
	pullUpdateInterval = setInterval(() => {
		axios.get(`${SERVER}/api/algorithm/updates`, {headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}})
		.then(({data}) => {
			console.log('data', data);
			phaseTwoEnded = false;
			if (data[0].noMovesRemaining && phaseOneEnded === false) {
				console.log('maybe done');
				amountOfEmptyPulls++;
				if (amountOfEmptyPulls > EMPTY_PULLS_TILL_STOP) {
					phaseOneEnded = true;
					toggleAlgorithm();
					if (!realTimeUpdates) {
						updates.forEach(move => {
							if (move.moveData) {
								console.log('HAS MOVEDATA', move);								
								if(moveDataGotten == 0){
									console.log("MoveDataGotten: ", moveDataGotten);
									defaultObjectiveFunction = move.moveData.dataMap['Objective Function'];
								}
								else{
									moveData = move.moveData;									
								}
								moveDataGotten++;																	
								console.log('MoveData: ', move.moveData);								
								//processMoveData(move.moveData);
							}
							if (move.desiredNumDistricts && move.desiredNumDistricts > 0) {
								console.log('GOT THIS MNAY DISTRICTS', move.desiredNumDistricts);
								num_districts = move.desiredNumDistricts;
							}
							if(!(move.toDistrictId == 0 && move.fromDistrictId == 0)){
								processMove(move);								
							}
						});
					}
					console.log('about to adjust color');
					adjustDistrictColors();
					let phaseOneDoneMessage = "Phase 1 of redistricting has completed. Please click 'Start' to begin Phase 2!";
					setMessage("", "Phase 1 of redistricting has completed. Please click 'Start' to begin Phase 2! Check results tab for results.");
					console.log('Processing Data... ');
					processMoveData(moveData);
					highlightMajorityMinorityDistricts();
				}
			} else {
				amountOfEmptyPulls = 0;
				if (realTimeUpdates) {
					data.forEach(move => {
						if (move.moveData) {
							console.log('HAS MOVEDATA', move);								
							if(moveDataGotten == 0){
								console.log("MoveDataGotten: ", moveDataGotten);
								defaultObjectiveFunction = move.moveData.dataMap['Objective Function']								
							}
							else{
								moveData = move.moveData;									
							}
							moveDataGotten++;							
							console.log('MoveData: ', move.moveData);	
						}
						if (move.desiredNumDistricts && move.desiredNumDistricts > 0) {
							console.log('GOT THIS MNAY DISTRICTS', move.desiredNumDistricts);
							num_districts = move.desiredNumDistricts;
						}
						if(!(move.toDistrictId == 0 && move.fromDistrictId == 0)){
							processMove(move);								
						}
					});
				}
				updates = updates.concat(data);
			}
			//processMoveData(moveData);
			// console.log(data);
		})
		.catch((err) => {
			console.warn("issue with algorithm update")
			// stopAlgorithm2();
			// toggleAlgorithm();
		});
	}, PULL_UPDATES_TIME);
}

function resumeAlgorithm() {
	var moveData;
	var EMPTY_PULLS_TILL_STOP_2 = 10;
	var amountOfEmptyPulls = 0;
	axios.post(`${SERVER}/api/algorithm/resume`, {}, {headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}})
	.then((res) => {
		// Algorithm resumed
		phaseOneEnded = false;
		setMessage("", "Phase 2 has started!");
	})
	.catch((err) => {
		// Issue resuming algorithm
	});
	pullUpdateInterval = setInterval(() => {
		axios.get(`${SERVER}/api/algorithm/updates`, {headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}})
		.then(({data}) => {
			console.log('sim', data);
			if (data[0].noMovesRemaining && phaseTwoEnded === false) {
				console.log('this');
				amountOfEmptyPulls++;
				if (amountOfEmptyPulls > EMPTY_PULLS_TILL_STOP_2) {
					phaseTwoEnded = true;
					toggleAlgorithm();
					if (!realTimeUpdates) {
						updates.forEach(move => {
							if (move.moveData) {
								console.log('HAS MOVEDATA', move);
								moveData = move.moveData;																
							}
							processMove(move);
						});
					}
					setMessage("", "Phase 2 of redistricting has completed.");
					processMoveData(moveData);
					highlightMajorityMinorityDistrict();
				}
			} else {
				console.log('that');
				amountOfEmptyPulls = 0;
				console.log('real time', realTimeUpdates);
				if (realTimeUpdates) {

					console.log('start processing');
					data.forEach(move => {
						if (move.moveData) {
							console.log('HAS MOVEDATA', move);
							moveData = move.moveData;									
						}
						console.log('going into process', move);
						processMove(move);
						
					});
				}
				updates = updates.concat(data);
				console.log('that2');
			}
		})
		.catch((err) => {
			// Issue pulling update, keep trying
		});
	}, 1250);
}

function stopAlgorithm() {
	//Tell server we are stopping
	clearInterval(pullUpdateInterval);
	pullUpdateInterval = null;
}

function stopAlgorithm2() {
	clearInterval(pullUpdateInterval);
	pullUpdateInterval = null;
	phaseOneEnded = false;
	phaseTwoEnded = false;
}

function exitAlgorithm() {
	axios.post(`${SERVER}/api/algorithm/end`, {}, {headers: {'Authorization': `Bearer ${localStorage.getItem('access_token')}`}}).then((res) => {
		console.log('exited algo');
		setMessage("","Algorithm has been stopped");
		clearInterval(pullUpdateInterval);
		pullUpdateInterval = null;
    }).catch(err => {
        setMessage("","Error has occured while stopping the algorithm");
    });
}

function processMove(move) {
	if (!move.noMovesRemaining) {
		console.log(move);
		if (move.join) {
			console.log('part move');
			// Graph partitioning move
			if (move.toDistrictId !== move.fromDistrictId) {
				console.log('not the same');
				const toDistrictMap = layerMap[move.toDistrictId];
				const fromDistrictMap = layerMap[move.fromDistrictId];
				toDistrictMap.precincts = toDistrictMap.precincts.concat(fromDistrictMap.precincts);
				toDistrictMap.precincts.forEach(precinct => {
					precinct.layerObject.setStyle({fillColor: toDistrictMap.color, color: toDistrictMap.color});
				});
				fromDistrictMap.precincts = [];
			}
		} else {
			// Simulated annealing move
			console.log('sim move');
			const toDistrictMap = layerMap[move.toDistrictId];
			const fromDistrictMap = layerMap[move.fromDistrictId];
			// console.log(toDistrictMap);
			// console.log(fromDistrictMap);
			const precinct = precinctMap[move.precinctId];
			console.log('size of to', toDistrictMap.precincts.length);
			toDistrictMap.precincts.push({id: move.precinctId, layerObject: precinct});
			console.log('size of to after', toDistrictMap.precincts.length)
			console.log('found precinct', precinct);
			precinct.setStyle({fillColor: toDistrictMap.color, color: toDistrictMap.color});
			console.log('size of from', fromDistrictMap.precincts.length);
			fromDistrictMap.precincts = fromDistrictMap.precincts.filter(precinct => precinct.id !== move.precinctId);
			console.log('size of from after', fromDistrictMap.precincts.length);
		}
	}
}
var num_districts = 0;
function adjustDistrictColors() {
	console.log('adjusting color');
	colorOptions = shuffle(colorOptions);
	for (let i = 1;i <= num_districts;i++) {
		const district = layerMap[i];
		district.color = colorOptions[i - 1];
		district.precincts.forEach(precinct => {
			precinct.layerObject.setStyle({fillColor: district.color, fillOpacity: .5, color: district.color});
		});
	}
	console.log('Color adjusted layerMap',layerMap);
	console.log(`For ${num_districts} districts`);
}

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

document.querySelector(`#algorithm-button`).addEventListener("click", toggleAlgorithm);

function resetDistrictColors(num_districts) {
	console.log('resetting color');
	for (let i = 1;i <= num_districts;i++) {
		const district = layerMap[i];
		district.precincts.forEach(precinct => {
			precinct.layerObject.setStyle({fillColor: district.color, fillOpacity: .5, color: district.color});
		});
	}
}

function processMoveData(moveData){
	console.log('Processing Move Data... ');
	console.log(moveData);
	let resultsTab = $('#results-tab');

	var originalObjectiveFunction = defaultObjectiveFunction;
	var dataMap = moveData.dataMap;
	var objectiveFunction = dataMap['Objective Function'];
	var ppCompactness = dataMap['Polsby Popper Compactness'];
	var popEquality = dataMap['Population Equality'];
	var stateEfficiencyGap = dataMap['Efficiency Gap'];	
	var statePVI = dataMap['PVI'];		

	console.log("State data set");


	//var moveDataDiv = document.createElement('div');

	var districtMaps = moveData.districtDataMap;
	for(let districtMap of districtMaps){
		
	var stateResultDiv = document.createElement('div');
	var stateOriginalObjectiveP = document.createElement('p');
	stateOriginalObjectiveP.innerText = "Original Objective Function: " + originalObjectiveFunction;
	var stateObjectiveP = document.createElement('p');
	stateObjectiveP.innerText = "State Objective Function: " + objectiveFunction;
	var stateCompactnessP = document.createElement('p');
	stateCompactnessP.innerText = "State Compactness: " + ppCompactness;
	var statePopEqP = document.createElement('p');
	statePopEqP.innerText = "State Population Equality: " + popEquality;
	var statePVIP = document.createElement('p');
	statePVIP.innerText = "State PVI: " + statePVI;
	var stateEfficiencyGapP = document.createElement('p');
	stateEfficiencyGapP.innerText = "State Efficiency Gap: " + stateEfficiencyGap;
	stateResultDiv.appendChild(stateOriginalObjectiveP);
	stateResultDiv.appendChild(stateObjectiveP);
	stateResultDiv.appendChild(stateCompactnessP);
	stateResultDiv.appendChild(statePopEqP);
	stateResultDiv.appendChild(statePVIP);
	stateResultDiv.appendChild(stateEfficiencyGapP);

	console.log("State div done");
		console.log("State", stateResultDiv);		
		//console.log(districtMap);
		var districtId = districtMap.districtId
		var ddataMap = districtMap.dataMap;
		console.log(ddataMap);		
		var districtCompactness = ddataMap['Compactness'];
		var districtPVI = ddataMap['PVI'];
		var districtPopulation = ddataMap['Population'];
		var districtEfficiencyGap = ddataMap['Efficiency Gap'];
		
		console.log("Data ok...");		
		//var districtColor = layerMap[districtId].color;
		//console.log(districtColor);				

		var districtDiv = getDistrictDiv(districtId, false)

		var moveDataDiv = document.createElement('div');
		//moveDataDiv.appendChild("Objective Function: " + objectiveFunction);
		var districtCompactnessP = document.createElement('p');
		districtCompactnessP.innerText = "Compactness: " + districtCompactness;
		var districtPopulationP = document.createElement('p');		
		districtPopulationP.innerText = "Population: " + districtPopulation;	
		var districtPVIP = document.createElement('p');		
		districtPVIP.innerText = "PVI: " + districtPVI;	
		var districtEfficiencyGapP = document.createElement('p');		
		districtEfficiencyGapP.innerText = "Efficiency Gap: " + districtEfficiencyGap;	
		moveDataDiv.appendChild(districtPopulationP);				
		moveDataDiv.appendChild(districtCompactnessP);
		moveDataDiv.appendChild(districtPVIP);		
		moveDataDiv.appendChild(districtEfficiencyGapP);				
		console.log("District", moveDataDiv);		
		districtDiv.appendChild(moveDataDiv);
		console.log("State", stateResultDiv);
		districtDiv.appendChild(stateResultDiv);
		console.log(moveDataDiv);		
		
		layerMap[districtId].divStorage = districtDiv;		
		
		console.log("div set");
		//moveDataDiv.innerHTML = '';
		//moveDataDiv.appendChild(districtDiv);
		/*var districtButton = document.createElement('button');
		districtButton.className = "btn btn-primary";
		districtButton.style.backgroundColor = districtColor;
		districtButton.style.borderColor = districtColor;	
		console.log(districtButton);
		districtButton.appendChild("District " + districtId);	
		console.log("District", districtId);

		moveDataDiv.appendChild(districtButton);*/
	}

	console.log(resultsTab);
	/*resultsTab[0].innerHTML = '';
	resultsTab[0].appendChild(moveDataDiv);*/
}
function getDistrictDiv(districtId, isOriginal) {	
	var properties = getDistrictProperties(districtId, isOriginal);
	console.log('properties', properties);
	var textColor = 'ivory';
	var canvasWidth = '100';
	var canvasHeight = '100';

	var demographicData = {
		datasets: [{
			data: [properties['white'], properties['black'], properties['native'], properties['asian'], properties['islander'], properties['twoormore'], properties['other']],
			backgroundColor: ['#FF6E29', '#E82AE2', '#2AE8C3', '#3B4EFF', '#86FF26', 'grey', '#fff'],
			borderColor: ['transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent']
		}],
		labels: ['White', 'African American', 'Native American', 'Asian', 'Pacific Islander', 'Two or More Races', 'Other']
	}

	var hispanicData = {
		datasets: [{
			data: [properties['population']-properties['hispanic'], properties['hispanic']],
			backgroundColor: ['magenta', 'orange'],
			borderColor: ['transparent', 'transparent']
		}],
		labels: ['Non-Hispanic', 'Hispanic']
	}

	var votingData = {
		datasets: [{
			data: [properties['democrat'], properties['republican'], properties['otherparty']],
			backgroundColor: ['blue', 'red', 'grey'],
			borderColor: ['transparent', 'transparent', 'transparent']
		}],
		labels: ['Democrat', 'Republican', 'Other']
	}

	//console.log("Initialized Data")

	var chartDiv = document.createElement('div');

	var titleText = document.createElement('h6');
	titleText.appendChild(document.createTextNode("District " + districtId));
	titleText.style.color = textColor;
	titleText.align = 'center';

	var topText = document.createElement('p');
	topText.appendChild(document.createTextNode(''));
	topText.style.color = textColor;
	topText.align = 'center';

	// Racial Demographics
	//console.log("Initialing Racial Data...")
	
	var raceCanvas = document.createElement('canvas');
	raceCanvas.height = canvasHeight;
	raceCanvas.width = canvasWidth;

	var populationText = document.createElement('p');
	populationText.appendChild(document.createTextNode("Total Population: " + properties['population']));
	populationText.style.color = textColor;

	var raceDiv = document.createElement('div');
	raceDiv.appendChild(raceCanvas);

	//console.log("Initialing Hispanic Data...")	
	// Hispanic Demographics
	var hispanicCanvas = document.createElement('canvas');
	hispanicCanvas.height = canvasHeight;
	hispanicCanvas.width = canvasWidth;

	var hispanicDiv = document.createElement('div');
	hispanicDiv.appendChild(hispanicCanvas);

	//console.log("Initialing Voting Data...")
	// Voting Data
	var votingCanvas = document.createElement('canvas');
	votingCanvas.height = canvasHeight;
	votingCanvas.width = canvasWidth;

	var votingDiv = document.createElement('div');
	votingDiv.appendChild(votingCanvas);

	chartDiv.setAttribute('style','width:250px;height:350px');
	chartDiv.appendChild(titleText);
	chartDiv.appendChild(topText);
	chartDiv.appendChild(populationText);	
	chartDiv.appendChild(raceDiv);
	chartDiv.appendChild(hispanicDiv);
	chartDiv.appendChild(votingDiv);

	console.log('appended');	

	// Init charts
	var raceDoughnut = new Chart(raceCanvas, {
		type: 'doughnut',
		data: demographicData,
		options: {
			legend: {
				labels: {
					fontColor: textColor
				}
			},
			cutoutPercentage: 70
		}
	});
	var hispanicPie = new Chart(hispanicCanvas, {
		type: 'doughnut',
		data: hispanicData,
		options: {
			legend: {
				labels: {
					fontColor: textColor
				}
			},
			cutoutPercentage: 70
		}
	});
	var votingPie = new Chart(votingCanvas, {
		type: 'doughnut',
		data: votingData,
		options: {
			legend: {
				labels: {
					fontColor: textColor
				}
			},
			cutoutPercentage: 70
		}
	});
	return chartDiv;
}

function getDistrictProperties(districtId, isOriginal){
	var properties = {
		'population': 0,
		"white": 0,
		"black": 0, 
		"native": 0, 
		"asian": 0, 
		"islander": 0, 
		"other": 0, 
		"twoormore": 0,
		"hispanic": 0, 
		"democrat": 0, 
		"republican": 0, 
		"otherparty": 0
	};

	var district;
	if(!isOriginal){
		district = layerMap[districtId];
	}
	else{
		district = {
			'precincts': []
		};
		var precinctMapLength = Object.values(precinctMap).length
		console.log(precinctMapLength);							
		var i = 1;
		for(var i = 1; i<precinctMapLength; i++){
			var precinct = precinctMap[i];
			//console.log(precinct);		
			if(districtId == precinct.feature['properties']['DISTRICT_ID']){
				district.precincts.push(precinct);
			}
		}
	}
	//console.log('District', district);
	
	district.precincts.forEach(precinct => {
		if(!isOriginal){
			var preProp = precinct.layerObject.feature.properties;
		}
		else{
			var preProp = precinct.feature.properties;			
		}
		properties['population'] += preProp['POP10'];
		properties['white'] += preProp['white'];			
		properties['black'] += preProp['black'];			
		properties['native'] += preProp['native'];			
		properties['asian'] += preProp['asian'];
		properties['islander'] += preProp['islander'];			
		properties['other'] += preProp['other'];			
		properties['twoormore'] += preProp['twoormore'];			
		properties['hispanic'] += preProp['hispanic'];		
		properties['democrat'] += parseFloat(preProp['democrat']);			
		properties['republican'] += parseFloat(preProp['republican']);							
		properties['otherparty'] += parseFloat(preProp['otherparty']);									
		console.log(preProp);		
	});
	//console.log(properties);
	return properties;
}

function highlightMajorityMinorityDistricts() {
	console.log("highlight");
	if (!majorityMinorityRequested) {return;}
	let majorityMinorityDistricts = new Set();
	for (let districtId of Object.keys(layerMap)) {
		let minorityMinimum = 0, minorityMaximum = 0;
		{
			let baseId = `majority-minority-constraint-district-1-${MINORITY_ID}`
			minorityMinimum = Number(document.getElementById(`${baseId}-min`).value);
			minorityMaximum = Number(document.getElementById(`${baseId}-max`).value);
		}
		let districtProperties = getDistrictProperties(districtId);
		let minorityRatio = districtProperties[MINORITY_PROPERTY] / districtProperties["population"];
		if ((minorityRatio >= minorityMinimum) && (minorityRatio <= minorityMaximum)) {
			console.log(`District ${districtId} is a majority-minority district`);
			majorityMinorityDistricts.add(districtId);
		}
	}
	return majorityMinorityDistricts;
}
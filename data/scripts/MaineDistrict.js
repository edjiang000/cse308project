{
let xhr = new XMLHttpRequest();
xhr.open("GET", "../data/maine/maine.geojson", false);
xhr.send();
let geoJSON = JSON.parse(xhr.response)
let district = [];
for (let feature of geoJSON.features) {
	if (feature.properties["DISTRICT_ID"] === 1) {
		district.push(feature.properties.OBJECTID);
	}
}
district.sort(function(element1, element2) {
	return element1 - element2;
});
JSON.stringify(district)
}

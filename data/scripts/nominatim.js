"use strict"; {
	let queries = {
	"Alabama": {q: true, limit: 1, polygon_geojson: 1},
	"Alaska": {q: true, limit: 1, polygon_geojson: 1},
	"Arizona": {q: true, limit: 1, polygon_geojson: 1},
	"Arkansas": {q: true, limit: 1, polygon_geojson: 1},
	"California": {q: true, limit: 1, polygon_geojson: 1},
	"Colorado": {q: true, limit: 1, polygon_geojson: 1},
	"Connecticut": {q: true, limit: 1, polygon_geojson: 1},
	"Delaware": {q: true, limit: 1, polygon_geojson: 1},
	"Florida": {q: true, limit: 1, polygon_geojson: 1},
	"Georgia": {q: true, limit: 2, polygon_geojson: 1},
	"Hawaii": {q: true, limit: 1, polygon_geojson: 1},
	"Idaho": {q: true, limit: 1, polygon_geojson: 1},
	"Illinois": {q: true, limit: 1, polygon_geojson: 1},
	"Indiana": {q: true, limit: 1, polygon_geojson: 1},
	"Iowa": {q: true, limit: 1, polygon_geojson: 1},
	"Kansas": {q: true, limit: 1, polygon_geojson: 1},
	"Kentucky": {q: true, limit: 1, polygon_geojson: 1},
	"Louisiana": {q: true, limit: 1, polygon_geojson: 1},
	"Maine": {q: true, limit: 1, polygon_geojson: 1},
	"Maryland": {q: true, limit: 1, polygon_geojson: 1},
	"Massachusetts": {q: true, limit: 1, polygon_geojson: 1},
	"Michigan": {q: true, limit: 1, polygon_geojson: 1},
	"Minnesota": {q: true, limit: 1, polygon_geojson: 1},
	"Mississippi": {q: true, limit: 1, polygon_geojson: 1},
	"Missouri": {q: true, limit: 1, polygon_geojson: 1},
	"Montana": {q: true, limit: 1, polygon_geojson: 1},
	"Nebraska": {q: true, limit: 1, polygon_geojson: 1},
	"Nevada": {q: true, limit: 1, polygon_geojson: 1},
	"New Hampshire": {q: true, limit: 1, polygon_geojson: 1},
	"New Jersey": {q: true, limit: 1, polygon_geojson: 1},
	"New Mexico": {q: true, limit: 1, polygon_geojson: 1},
	"New York": {q: true, limit: 2, polygon_geojson: 1},
	"North Carolina": {q: true, limit: 1, polygon_geojson: 1},
	"North Dakota": {q: true, limit: 1, polygon_geojson: 1},
	"Ohio": {q: true, limit: 1, polygon_geojson: 1},
	"Oklahoma": {q: true, limit: 1, polygon_geojson: 1},
	"Oregon": {q: true, limit: 1, polygon_geojson: 1},
	"Pennsylvania": {q: true, limit: 1, polygon_geojson: 1},
	"Rhode Island": {q: true, limit: 1, polygon_geojson: 1},
	"South Carolina": {q: true, limit: 1, polygon_geojson: 1},
	"South Dakota": {q: true, limit: 1, polygon_geojson: 1},
	"Tennessee": {q: true, limit: 1, polygon_geojson: 1},
	"Texas": {q: true, limit: 1, polygon_geojson: 1},
	"Utah": {q: true, limit: 1, polygon_geojson: 1},
	"Vermont": {q: true, limit: 1, polygon_geojson: 1},
	"Virginia": {q: true, limit: 1, polygon_geojson: 1},
	"Washington": {q: true, limit: 2, polygon_geojson: 1},
	"West Virginia": {q: true, limit: 1, polygon_geojson: 1},
	"Wisconsin": {q: true, limit: 1, polygon_geojson: 1},
	"Wyoming": {q: true, limit: 1, polygon_geojson: 1}
}
let responses = {};
let xhr = new XMLHttpRequest();
for (let [queryKey, queryValue] of Object.entries(queries)) {
	if (!queryValue.q) {continue;}
	console.log(queryKey);
	let urlSearchParams = new URLSearchParams();
	urlSearchParams.append("q", queryKey);
	urlSearchParams.append("format", "json");
	for (let [key, value] of Object.entries(queryValue)) {
		if (key === "q") {continue;}
		if (value === 0) {continue;}
		urlSearchParams.append(key, value);
	}
	xhr.open("GET", `https://nominatim.openstreetmap.org/search.php?${urlSearchParams.toString()}`, false); //Single-thread as per usage policy: https://operations.osmfoundation.org/policies/nominatim/
	xhr.send()
	let response = JSON.parse(xhr.response)[queryValue.limit - 1];
	let boundingbox = response["boundingbox"];
	responses[queryKey] = {
		"boundingbox": [[boundingbox[1], boundingbox[3]], [boundingbox[0], boundingbox[2]]],
		"geojson": response["geojson"]
	};
}
responses;
}

import geojson
import csv
from area import area


class CountyDemographic:
    def __init__(self, county, pop, white, black, native, asian, islander, other, twoormore, hispanic, precinct):
        self.county = county
        self.pop = int(pop)
        self.white = int(white)
        self.black = int(black)
        self.native = int(native)
        self.asian = int(asian)
        self.islander = int(islander)
        self.other = int(other)
        self.twoormore = int(twoormore)
        self.hispanic = int(hispanic)
        self.precinct = str(precinct)

class PrecinctVotes:
    def __init__(self, dem, rep, oth):
        self.dem = dem
        self.rep = rep
        self.oth = oth


file = open("../raw/arizonaraw.json", "r+")

# Open geojson
geodata = geojson.load(file)


precinctDict = {}
row = 1
# Open csv
csv.register_dialect('myDialect', delimiter=',')
with open("../raw/arizonaDemographics.csv") as data:
    reader = csv.reader(data, dialect='myDialect')
    next(reader)
    for row in reader:
        countyFP = row[8]
        county = row[7][:-7]
        pop = row[34]
        white = row[36]
        black = row[37]
        native = row[38]
        asian = row[39]
        islander = row[40]
        other = row[41]
        twoormore = row[42]
        hispanic = row[106]
        precinct = str(int(countyFP)) + 'p' + str((row[27]))
        precinctDict[str(precinct)] = CountyDemographic(county, pop, white, black, native, asian, islander, other, twoormore, hispanic, precinct)


votingDict = {}

csv.register_dialect('myDialect', delimiter=',')
with open("../raw/arizonaVoting.csv") as voting:
    reader = csv.reader(voting, dialect='myDialect')
    next(reader)
    for row in reader:
        rep = row[33]
        dem = row[34]
        oth = row[35]
        geoID = str(row[4])
        votingDict[geoID] = PrecinctVotes(dem, rep, oth)

cgDict = {}

with open("../raw/arizonaCG.csv") as cg:
    reader = csv.reader(cg, dialect = 'myDialect')
    next(reader)
    for row in reader:
        precName = row[5]
        names = precName.split(' ')
        precName = str(names[0])
        try:
            int(precName)
        except Exception:
            precName = row[4]
        county = str(row[3])
        cd = row[10]
        while precName.startswith('0'):
            precName = precName[1:]
        cgId = county + 'p' + str(precName)
        cgDict[cgId] = cd


for i, feature in enumerate(geodata['features'], start=1):
    precinctId = str(int(feature['properties']['COUNTYFP10'])) + 'p' + str((feature['properties']['VTDST10']))
    geoId = str(feature['properties']['GEOID10'])
    if precinctId not in precinctDict:
        print("ERROR: " + str(precinctId))
        continue
    if geoId not in votingDict:
        print("ERROR: " + str(precinctId))
        continue
    precinctDemographic = precinctDict[precinctId]
    feature['properties']['OBJECTID'] = i
    feature['properties']['POP10'] = precinctDemographic.pop
    feature['properties']['COUNTY'] = precinctDemographic.county
    feature['properties']['white'] = precinctDemographic.white
    feature['properties']['black'] = precinctDemographic.black
    feature['properties']['native'] = precinctDemographic.native
    feature['properties']['asian'] = precinctDemographic.asian
    feature['properties']['islander'] = precinctDemographic.islander
    feature['properties']['other'] = precinctDemographic.other
    feature['properties']['twoormore'] = precinctDemographic.twoormore
    feature['properties']['hispanic'] = precinctDemographic.hispanic

    precinctVoting = votingDict[geoId]
    feature['properties']['democrat'] = precinctVoting.dem
    feature['properties']['republican'] = precinctVoting.rep
    feature['properties']['otherparty'] = precinctVoting.oth

    polyArea = area(feature['geometry'])
    feature['properties']['SHAPEarea'] = polyArea

    precinctIdCG = str((feature['properties']['VTDST10']))
    while precinctIdCG.startswith('0'):
        precinctIdCG = precinctIdCG[1:]
    cgId = precinctDemographic.county + 'p' + precinctIdCG

    if cgId not in cgDict:
        if cgId.startswith('Gilap'):
            feature['properties']['DISTRICT_ID'] = 1
            continue
        elif cgId.startswith('Grahamp'):
            feature['properties']['DISTRICT_ID'] = 1
            continue
        elif cgId.startswith('Maricopap'):
            cgId = cgId[0:-1]
            if cgId not in cgDict:
                print("ERROR CG: " + str(precinctId) + ", " + str(cgId))
                continue
            feature['properties']['DISTRICT_ID'] = cgDict[cgId]
            continue

        print("ERROR CG: " + str(precinctId) + ", " + str(cgId))
        continue
    feature['properties']['DISTRICT_ID'] = cgDict[cgId]
    if feature['properties']['DISTRICT_ID'] == 'NA':
        print(feature['properties']['VTDST10'])
        feature['properties']['DISTRICT_ID'] = 5


with open("arizona.geojson", "w") as output:
    geojson.dump(geodata, output)




import geojson
import csv
from area import area


class CountyDemographic:
    def __init__(self, county, pop, white, black, native, asian, islander, other, twoormore, hispanic, precinct):
        self.county = county
        self.pop = int(pop)
        self.white = int(white)
        self.black = int(black)
        self.native = int(native)
        self.asian = int(asian)
        self.islander = int(islander)
        self.other = int(other)
        self.twoormore = int(twoormore)
        self.hispanic = int(hispanic)
        self.precinct = str(precinct)


class PrecinctVotes:
    def __init__(self, dem, rep, oth):
        self.dem = dem
        self.rep = rep
        self.oth = oth


file = open("../raw/pennsylvaniaraw.geojson", "r+")

# Open geojson
geodata = geojson.load(file)


precinctDict = {}
row = 1
# Open csv
csv.register_dialect('myDialect', delimiter=',')
with open("../raw/pennsylvaniaDemographics.csv") as data:
    reader = csv.reader(data, dialect='myDialect')
    next(reader)
    for row in reader:
        countyFP = row[8]
        county = row[7][:-7]
        pop = row[34]
        white = row[36]
        black = row[37]
        native = row[38]
        asian = row[39]
        islander = row[40]
        other = row[41]
        twoormore = row[42]
        hispanic = row[106]
        precinct = str(int(countyFP)) + '0' + str((row[27]))
        precinctDict[str(precinct)] = CountyDemographic(county, pop, white, black, native, asian, islander, other, twoormore, hispanic, precinct)


# votingDict = {}
#
# csv.register_dialect('myDialect', delimiter=',')
# with open("../raw/pennsylvaniaVotes.csv") as voting:
#     reader = csv.reader(voting, dialect='myDialect')
#     next(reader)
#     for row in reader:
#         rep = row[32]
#         dem = row[31]
#         oth = 0
#         geoID = str(row[7])
#         votingDict[geoID] = PrecinctVotes(dem, rep, oth)
#

# cgDict = {}
# cgBackupDict = {}
#
# with open("../raw/pennsylvaniaCG.csv") as cg:
#     reader = csv.reader(cg, dialect='myDialect')
#     next(reader)
#     for row in reader:
#         precinctIdCG = str(row[7])
#         while precinctIdCG.startswith('0'):
#             precinctIdCG = precinctIdCG[1:]
#         fipsCG = str(row[6])
#         geoIdCG = fipsCG + '0' + precinctIdCG
#         districtCG = int(row[8])
#         cgDict[geoIdCG] = districtCG
#         precinctIdCG = str(row[4])
#         geoIdCG = fipsCG + '0' + precinctIdCG
#         cgBackupDict[geoIdCG] = districtCG
#
#         print(geoIdCG)
#
# print("Setting...")

districts = []
distCount = {}


def findDistrict(d):
    if d not in districts:
        districts.append(d)
        distCount[d] = 0
    distCount[d] += 1
    return districts.index(d) + 1

for i, feature in enumerate(geodata['features'], start=1):
    precinctId = str(int(feature['properties']['COUNTYFP10'])) + '0' + str((feature['properties']['VTDST10']))
#    precinctCounty = feature['properties']['COUNTY']
    if precinctId not in precinctDict:
        print("ERROR: " + str(precinctId))
        continue
    precinctDemographic = precinctDict[precinctId]
    feature['properties']['OBJECTID'] = i
    feature['properties']['DISTRICT_ID'] = findDistrict(feature['properties']['US_HOUSE_D'])

    feature['properties']['POP10'] = precinctDemographic.pop
    feature['properties']['COUNTY'] = precinctDemographic.county
    feature['properties']['white'] = precinctDemographic.white
    feature['properties']['black'] = precinctDemographic.black
    feature['properties']['native'] = precinctDemographic.native
    feature['properties']['asian'] = precinctDemographic.asian
    feature['properties']['islander'] = precinctDemographic.islander
    feature['properties']['other'] = precinctDemographic.other
    feature['properties']['twoormore'] = precinctDemographic.twoormore
    feature['properties']['hispanic'] = precinctDemographic.hispanic

    feature['properties']['democrat'] = feature['properties']['NDV']
    feature['properties']['republican'] = feature['properties']['NRV']
    feature['properties']['otherparty'] = 0

    polyArea = area(feature['geometry'])
    feature['properties']['SHAPEarea'] = polyArea


print(distCount)


with open("pennsylvania.geojson", "w") as output:
    geojson.dump(geodata, output)




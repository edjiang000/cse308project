from collections import defaultdict
import geojson
import csv


class CountyDemographic:
    def __init__(self, county, pop, white, black, native, asian, islander, other, twoormore, hispanic, precinct):
        self.county = county
        self.pop = int(pop)
        self.white = int(white)
        self.black = int(black)
        self.native = int(native)
        self.asian = int(asian)
        self.islander = int(islander)
        self.other = int(other)
        self.twoormore = int(twoormore)
        self.hispanic = int(hispanic)
        self.precinct = str(precinct)


class CountyVotes:
    def __init__(self, democrat, republican, other, total):
        self.democrat = int(democrat)
        self.republican = int(republican)
        self.other = int(other)
        self.total = int(total)


def checkDistrict(county):
    # print(county)
    # print(county in cg1)
    if county in cg1:
        return 1
    else:
        return 2


# Maine
geo_data_file = open("../raw/Maine_Voting_Districts_2010_GeoLibrary.geojson", "r+")
voting_data_file = "../raw/maineprecinctdata.txt"
demographic_data_file = "../raw/StateDemographics.csv"

# Open geojson
geodata = geojson.load(geo_data_file)

precinctDict = {}
row = 1
# Open csv
csv.register_dialect('myDialect', delimiter=',')
with open(demographic_data_file) as data:
    reader = csv.reader(data, dialect='myDialect')
    next(reader)
    for row in reader:
        county = row[7][:-7]
        pop = row[34]
        white = row[36]
        black = row[37]
        native = row[38]
        asian = row[39]
        islander = row[40]
        other = row[41]
        twoormore = row[42]
        hispanic = row[106]
        precinct = row[27]

        precinctDict[str(precinct)] = CountyDemographic(county, pop, white, black, native, asian, islander, other, twoormore, hispanic, precinct)

row = 1
county_votes_dict = {}
with open(voting_data_file) as data:
    reader = csv.reader(data, dialect='myDialect')
    next(reader)
    for row in reader:
        county = str(row[0])
        democrat = int(row[7])
        republican = int(row[10])
        other = int(row[8]) + int(row[9])
        total = int(row[12]) - int(row[11])

        # For precincts in the same county, aggregate all votes and put result into dictionary
        county_previous_votes = county_votes_dict.get(str(county), CountyVotes(democrat=0, republican=0, other=0, total=0))
        county_votes_dict[str(county)] = CountyVotes(democrat + county_previous_votes.democrat, \
        republican + county_previous_votes.republican, other + county_previous_votes.other, total + county_previous_votes.total)


cg1 = {
    'Cumberland',
    'Kennebec',
    'Knox',
    'Lincoln',
    'Sagadahoc',
    'York'
}

for i, feature in enumerate(geodata['features'], start=1):
    precinctId = feature['properties']['VTDST10']
    precinctCounty = feature['properties']['COUNTY']
    precinctDemographic = precinctDict[str(precinctId)]
    precinctPop = int(feature['properties']['POP10'])
    feature['properties']['OBJECTID'] = i
    feature['properties']['DISTRICT_ID'] = checkDistrict(precinctCounty)
    feature['properties']['white'] = precinctDemographic.white
    feature['properties']['black'] = precinctDemographic.black
    feature['properties']['native'] = precinctDemographic.native
    feature['properties']['asian'] = precinctDemographic.asian
    feature['properties']['islander'] = precinctDemographic.islander
    feature['properties']['other'] = precinctDemographic.other
    feature['properties']['twoormore'] = precinctDemographic.twoormore
    feature['properties']['hispanic'] = precinctDemographic.hispanic

    # Compute and assign voting data to precincts
    county_abbrev = str(precinctCounty)[0:3].upper()
    county_votes = county_votes_dict[county_abbrev]
    feature['properties']['democrat'] = (county_votes.democrat / county_votes.total) * precinctPop
    feature['properties']['republican'] = (county_votes.republican / county_votes.total) * precinctPop
    feature['properties']['otherparty'] = (county_votes.other / county_votes.total) * precinctPop

    # print(county_abbrev)
    # print(feature['properties']['democrat'])
    # print(feature['properties']['republican'])
    # print(feature['properties']['otherparty'])

with open("../maine/maine.geojson", "w") as output:
    geojson.dump(geodata, output)




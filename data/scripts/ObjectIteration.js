"use strict"; {
const PROPERTIES = 2**21;
const ITERATIONS = 2**5;

let performanceObserver = new PerformanceObserver(function(performanceObserverEntryList, performanceObserver) {
	
});
performanceObserver.observe({entryTypes: ["measure"]});
for (let iteration = 0; iteration < ITERATIONS; iteration++) {
	console.log(iteration);
	let o = {};
	for (let property = Math.floor(Math.random() * (2**8 - 2**7)) + 2**7; property < PROPERTIES; property++) {
		o[property] = {v: Math.random()};
	}
	let d = 0;
	performance.mark("t0");
	for (let k in o) {
		d += o[k].v;
	}
	performance.mark("t1");
	performance.measure("for..in", "t0", "t1");
	performance.clearMarks();

	performance.mark("t0");
	for (let k of Object.keys(o)) {
		d += o[k].v;
	}
	performance.mark("t1");
	performance.measure("Object.keys", "t0", "t1");
	performance.clearMarks();

	performance.mark("t0");
	for (let v of Object.values(o)) {
		d += v.v; //Still can modify original objects (not non-reference types like numbers)
	}
	performance.mark("t1");
	performance.measure("Object.values", "t0", "t1");
	performance.clearMarks();

	performance.mark("t0");
	for (let [k, v] of Object.entries(o)) {
		d += v.v;
	}
	performance.mark("t1");
	performance.measure("Object.entries", "t0", "t1");
	performance.clearMarks();
	
	console.log(d);
}
let durations = {}
for (let performanceEntry of performanceObserver.takeRecords()) {
	if (!durations[performanceEntry.name]) {durations[performanceEntry.name] = 0;}
	durations[performanceEntry.name] += performanceEntry.duration;
}
console.log(durations);
}

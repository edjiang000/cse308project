# Congressional Redistricting Software
## Blue Jays Design Document

## Team Blue Jays
Edward Jiang
Nick Pirrello
Sean Yip
Zhouqi Gong
Tony O'Halleran

## States
Arizona, Pennsylvania, Maine

## Data
Maine boundary: Maine.gov
Arizona and Pennsylvania Boundary: Harvard
All voting data: Harvard
All demographic Data: US census

## Objective Function
Measures:
	Compactness (Polsby-Popper)
	Compactness (Schwartzberg)
	Political Equality (PVI)
	Political Equality (Efficiency Gap)
	Racial Equality
	Hispanic Equality
	Population Equality

## Dependencies
gson
python-geojson
Chart.js
leaflet
Spring
axios
jwt